#!/usr/bin/env python3

import os
import numpy as np
import argparse

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("f1",      help="first  maxpoint file")
    parser.add_argument("f2",      help="second maxpoint file")
    return parser.parse_args()

def print_delta_phi(f1, f2):
    assert os.path.isfile(f1), "Cannot find %s"%f1
    assert os.path.isfile(f2), "Cannot find %s"%f2
    
    V1 = np.loadtxt(f1)
    V2 = np.loadtxt(f2)
    
    ID  = V1[:,0] 
    REZ = np.unwrap(V2[:,3] - V1[:,3])
    for p in zip(ID,REZ):
        print("%i %g"%p);

if __name__ == "__main__":
    args = standalone_args()
    print_delta_phi(**vars(args))
