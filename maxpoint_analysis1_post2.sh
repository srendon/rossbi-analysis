#!/bin/bash
cd analysis/maxpoint1

maxpoint_analysis1_deltaphi.py max_gas_p_ndg.txt min_gas_rossby.txt > deltaphi_gas_p_ndg_gas_rossby.txt
maxpoint_analysis1_deltaphi.py max_gas_p_ndg.txt max_gas_rho_ndg.txt > deltaphi_gas_p_ndg_gas_rho_ndg.txt
plot_1d_txt.py deltaphi_gas_p_ndg_gas_rossby.txt deltaphi_gas_p_ndg_gas_rossby -legends "deltaphi gas_p_ndg,gas_rossby" -xlabel=idx -ylabel="delta_phi" -title="deltaphi gas_p_ndg,gas_rossby" -dpi=150 -ymin=-0.05 -ymax=0.05
plot_1d_txt.py deltaphi_gas_p_ndg_gas_rho_ndg.txt deltaphi_gas_p_ndg_gas_rho_ndg -legends "deltaphi gas_p_ndg,gas_rho_ndg" -xlabel=idx -ylabel="delta_phi" -title="deltaphi gas_p_ndg,gas_rho_ndg" -dpi=150 -ymin=-0.05 -ymax=0.05
