# Demonstration script for paraview version 5.9
#
# Creates a cylindrical grid with a Python ProgrammableSource.
# Works also in parallel by splitting the extents
#
# written by Jean M. Favre, Swiss National Supercomputing Centre
#
# tested Wed 09 Dec 2020 11:22:48 AM CET
# http://mathworld.wolfram.com/CylindricalCoordinates.html
#
# ffmpeg -framerate 25 -pattern_type sequence -start_number 1 -r 3 -i plot.%04d.png -s 1383x553 -crf 2 test.avi

from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

viewSG = GetRenderView()

ReqInfoScript = """
import numpy as np
import h5py
import glob, os 

def read_input_variables_file(input_dir=None):
    if input_dir==None:
        input_variables_file = open('./Input_variables.data')
    else:
        input_variables_file = open(input_dir + 'Input_variables.data')
    input_variables_str = input_variables_file.read()
    input_variables_file.close()
    return input_variables_str

reflective_bc       = False
path                = "/data/Paraview_tests/RWI_m6/high_res/"
input_variables_str = read_input_variables_file(path)

if 'NO_REFLECTIVE_BOUNDARY' not in input_variables_str:
    reflective_bc = True

list_data_files = glob.glob(path + "*data.h5")
for data_file in list_data_files:
    if 'restart' in data_file:
        list_data_files.remove(data_file)
    if 'high_res.data' in data_file:
        list_data_files.remove(data_file)
    if 'low_res.data' in data_file:
        list_data_files.remove(data_file)

N_files = len(list_data_files)

list_data_files = [os.path.basename(k) for k in list_data_files]
list_data_files = [int(k[:3]) for k in list_data_files]
s1              = int(min(list_data_files))


timesteps=range(s1, s1 + N_files)


executive = self.GetExecutive()
outInfo   = executive.GetOutputInformation(0)

outInfo.Remove(executive.TIME_STEPS())
for timestep in timesteps:
    outInfo.Append(executive.TIME_STEPS(), timestep)
outInfo.Remove(executive.TIME_RANGE())
outInfo.Append(executive.TIME_RANGE(), timesteps[0])
outInfo.Append(executive.TIME_RANGE(), timesteps[-1])

print('timestep_ini=',timesteps[0])
print('timestep_end=',timesteps[-1])

# A 3D cylindrical mesh
f     = h5py.File("{path}{time:03}.data.h5".format(path=path,time=s1),"r")
# radius, theta, z-elevation
nx=f["mesh"]["x"].shape[0]
ny=f["mesh"]["y"].shape[0]
nz=f["mesh"]["z"].shape[0]

# For closing the cylinder
ny = ny + 1

if reflective_bc == True:
    nz = 2*nz

outInfo.Set(executive.WHOLE_EXTENT(), 0, nx-1 , 0, ny-1 , 0, nz-1)
outInfo.Set(vtk.vtkAlgorithm.CAN_PRODUCE_SUB_EXTENT(), 1)
f.close()
"""

ReqDataScript = """
import numpy as np
import h5py
from vtk.numpy_interface import algorithms as algs
import vtk
import glob, os 

def double_dimension_zaxis(val, velocity_z=None):
    u = val
    if velocity_z==True:
        u = np.concatenate((-np.flip(u,0),u),axis=0)
    else:
        u = np.concatenate((np.flip(u,0),u),axis=0)
    return u

def read_input_variables_file(input_dir=None):
    if input_dir==None:
        input_variables_file = open('./Input_variables.data')
    else:
        input_variables_file = open(input_dir + 'Input_variables.data')
    input_variables_str = input_variables_file.read()
    input_variables_file.close()
    return input_variables_str

def transform_stationary(val,ny):
    u = val
    val = val.reshape(val.shape[0],1,val.shape[1])
    val = np.tile(val, [1,ny,1]) # We repeat the array ny times in 0 axis and 1 in 1 axis
    return val

def close_cylinder(val,nx,nz):
    # Allows to add a last 'sheet' in y direction in order to close the cylinder
    u = val[:,0,:]
    u = u.reshape((nz,1,nx))
    out_val = np.concatenate((val,u),axis=1)
    return out_val

def midplane_values(val):
    # Allows to take the values of the midplane and make an 
    # array of size (nz,ny,nx)
    nz,ny,nx = val.shape[0], val.shape[1], val.shape[2]
    k0       = 3 + int(0.5*nz)

    u = val[k0,:,:]
    u = u.reshape((1,ny,nx))
    # We reapeat the array nz times in z axis, 1 time in y axis and
    # 1 time in x axis
    out_val = np.tile(u,[nz,1,1])
    return out_val

reflective_bc       = False
path                = "/data/Paraview_tests/RWI_m6/high_res/"
input_variables_str = read_input_variables_file(path)


if 'NO_REFLECTIVE_BOUNDARY' not in input_variables_str:
    reflective_bc = True

list_data_files = glob.glob(path + "*data.h5")
for data_file in list_data_files:
    if 'restart' in data_file:
        list_data_files.remove(data_file)
    if 'high_res.data' in data_file:
        list_data_files.remove(data_file)
    if 'low_res.data' in data_file:
        list_data_files.remove(data_file)

N_files = len(list_data_files)

list_data_files = [os.path.basename(k) for k in list_data_files]
list_data_files = [int(k[:3]) for k in list_data_files]
s1              = int(min(list_data_files))

executive = self.GetExecutive()
outInfo   = executive.GetOutputInformation(0)

if outInfo.Has(executive.UPDATE_TIME_STEP()):
    time  = int(outInfo.Get(executive.UPDATE_TIME_STEP()))
else:
    print("no update time available")
    time  = 0

exts      = [executive.UPDATE_EXTENT().Get(outInfo, i) for i in range(6)]

output.SetExtent(exts)

print("{path}{time:03}.data.h5".format(path=path, time=time))
fstat = h5py.File("{path}Stationnary.h5".format(path=path),"r")
f     = h5py.File("{path}{time:03}.data.h5".format(path=path,time=s1),"r")
#f     = h5py.File("{path}{time:03}.data.h5".format(path=path,time=time),"r")


### Mesh dimensions ###
nx=f["mesh"]["x"].shape[0]
ny=f["mesh"]["y"].shape[0]
nz=f["mesh"]["z"].shape[0]
if 'NO_BOX' in input_variables_str:
    ny = ny + 1

### Mesh ###
Raxis     = f["mesh"]["x"][...]
Thetaaxis = f["mesh"]["y"][...]
Zaxis     = f["mesh"]["z"][...]
if 'NO_BOX' in input_variables_str:
    Thetaaxis = np.concatenate((Thetaaxis,np.array([Thetaaxis[0]])),axis=0)


#*************************************
#************** Gas ******************
#*************************************
### Euler Variables
p_gas      = f["gas"]["p"][()]
rho_gas    = f["gas"]["rho"][()]
strain_gas = f["gas"]["strain"][()]
vortx_gas  = f["gas"]["vortx"][()]
vorty_gas  = f["gas"]["vorty"][()]
vortz_gas  = f["gas"]["vortz"][()]
div_gas    = f["gas"]["div"][()]
vx_gas     = f["gas"]["vx"][()]
vy_gas     = f["gas"]["vy"][()]
vz_gas     = f["gas"]["vz"][()]



### Stationary values ###
p0_gas       = fstat["gas"]["p"][()]
rho0_gas     = fstat["gas"]["rho"][()]
p0_gas       = transform_stationary(p0_gas,ny)
rho0_gas     = transform_stationary(rho0_gas,ny)


#*************************************
#************ Particles **************
#*************************************
if 'ONEPHASE' not in input_variables_str:
    ### Euler Variables
    rho_part    = f["particles"]["rho"][()]
    strain_part = f["particles"]["strain"][()]
    vortx_part  = f["particles"]["vortx"][()]
    vorty_part  = f["particles"]["vorty"][()]
    vortz_part  = f["particles"]["vortz"][()]
    div_part    = f["particles"]["div"][()]
    vx_part     = f["particles"]["vx"][()]
    vy_part     = f["particles"]["vy"][()]
    vz_part     = f["particles"]["vz"][()]
    
    ### Stationary values ###
    rho0_part = fstat["particles"]["rho"][()]
    rho0_part = transform_stationary(rho0_part,ny)


#************************************#
#          Close cylinder            #
#************************************#
if 'NO_BOX' in input_variables_str:
    p_gas      = close_cylinder(p_gas,nx,nz)
    rho_gas    = close_cylinder(rho_gas,nx,nz)
    strain_gas = close_cylinder(strain_gas,nx,nz)
    vortx_gas  = close_cylinder(vortx_gas,nx,nz)
    vorty_gas  = close_cylinder(vorty_gas,nx,nz)
    vortz_gas  = close_cylinder(vortz_gas,nx,nz)
    div_gas    = close_cylinder(div_gas,nx,nz)
    vx_gas     = close_cylinder(vx_gas,nx,nz)
    vy_gas     = close_cylinder(vy_gas,nx,nz)
    vz_gas     = close_cylinder(vz_gas,nx,nz)

    if 'ONEPHASE' not in input_variables_str:
        rho_part    = close_cylinder(rho_part,nx,nz)
        strain_part = close_cylinder(strain_part,nx,nz) 
        vortx_part  = close_cylinder(vortx_part,nx,nz)
        vorty_part  = close_cylinder(vorty_part,nx,nz)
        vortz_part  = close_cylinder(vortz_part,nx,nz)
        div_part    = close_cylinder(div_part,nx,nz)
        vx_part     = close_cylinder(vx_part,nx,nz)
        vy_part     = close_cylinder(vy_part,nx,nz)
        vz_part     = close_cylinder(vz_part,nx,nz)


#************************************#
#   Reflective boundary conditions   #
#************************************#
if reflective_bc == True:
    Zaxis      = np.concatenate((-np.flip(Zaxis,0), Zaxis), axis=0)
    p_gas      = double_dimension_zaxis(p_gas)
    p0_gas     = double_dimension_zaxis(p0_gas)
    rho_gas    = double_dimension_zaxis(rho_gas)  
    rho0_gas   = double_dimension_zaxis(rho0_gas)  
    strain_gas = double_dimension_zaxis(strain_gas)
    vortx_gas  = double_dimension_zaxis(vortx_gas) 
    vorty_gas  = double_dimension_zaxis(vorty_gas) 
    vortz_gas  = double_dimension_zaxis(vortz_gas) 
    div_gas    = double_dimension_zaxis(div_gas)   
    vx_gas     = double_dimension_zaxis(vx_gas)    
    vy_gas     = double_dimension_zaxis(vy_gas)    
    vz_gas     = double_dimension_zaxis(vz_gas, velocity_z=True)    

    if 'ONEPHASE' not in input_variables_str:
        rho_part    = double_dimension_zaxis(rho_part)  
        rho0_part   = double_dimension_zaxis(rho0_part)  
        strain_part = double_dimension_zaxis(strain_part)
        vortx_part  = double_dimension_zaxis(vortx_part) 
        vorty_part  = double_dimension_zaxis(vorty_part) 
        vortz_part  = double_dimension_zaxis(vortz_part) 
        div_part    = double_dimension_zaxis(div_part)   
        vx_part     = double_dimension_zaxis(vx_part)    
        vy_part     = double_dimension_zaxis(vy_part)    
        vz_part     = double_dimension_zaxis(vz_part, velocity_z=True)    


#************************************#
#     Stationary midplane values     #
#************************************#
rho0_gas_mid = midplane_values(rho0_gas) 
p0_gas_mid   = midplane_values(p0_gas) 
#if 'ONEPHASE' not in input_variables_str:
#    rho0_part_mid = midplane_values(rho0_part, k0, nx, ny, nz) 


#************************************#
#       Create final meshgrid        #
#************************************#
z, t, r = np.meshgrid(Zaxis, Thetaaxis, Raxis, indexing="ij")
X       = r * np.cos(t)
Y       = r * np.sin(t)
Z       = z

coordinates   = algs.make_vector(X.ravel(),Y.ravel(),Z.ravel())
output.Points = coordinates


### Output PointData ###
output.PointData.append(p_gas.ravel(),                        "gas_p")
output.PointData.append(rho_gas.ravel(),                      "gas_rho")
output.PointData.append(p_gas.ravel()/p0_gas.ravel(),         "gas_p/gas_p0")
output.PointData.append(rho_gas.ravel()/rho0_gas.ravel(),     "gas_rho/gas_rho0")
output.PointData.append(p_gas.ravel()/p0_gas_mid.ravel(),     "gas_p/gas_p0_mid")
output.PointData.append(rho_gas.ravel()/rho0_gas_mid.ravel(), "gas_rho/gas_rho0_mid")
output.PointData.append(strain_gas.ravel(),                   "gas_strain")
output.PointData.append(vortx_gas.ravel(),                    "gas_vortx")
output.PointData.append(vorty_gas.ravel(),                    "gas_vorty")
output.PointData.append(vortz_gas.ravel(),                    "gas_vortz")
output.PointData.append(div_gas.ravel(),                      "gas_div")
output.PointData.append(vx_gas.ravel(),                       "gas_vx")
output.PointData.append(vy_gas.ravel(),                       "gas_vy")
output.PointData.append(vz_gas.ravel(),                       "gas_vz")
if 'ONEPHASE' not in input_variables_str:
    output.PointData.append(rho_part.ravel(),                      "part_rho")
    output.PointData.append(rho_part.ravel()/rho_gas.ravel(),      "part_rho/gas_rho")
    output.PointData.append(rho_part.ravel()/rho0_part.ravel(),    "part_rho/part_rho0")
    output.PointData.append(rho_part.ravel()/rho0_gas_mid.ravel(), "part_rho/gas_rho0_mid")
    output.PointData.append(strain_part.ravel(),                   "part_strain")
    output.PointData.append(vortx_part.ravel(),                    "part_vortx")
    output.PointData.append(vorty_part.ravel(),                    "part_vorty")
    output.PointData.append(vortz_part.ravel(),                    "part_vortz")
    output.PointData.append(div_part.ravel(),                      "part_div")
    output.PointData.append(vx_part.ravel(),                       "part_vx")
    output.PointData.append(vy_part.ravel(),                       "part_vy")
    output.PointData.append(vz_part.ravel(),                       "part_vz")

### Close files ###
f.close()
fstat.close()

output.GetInformation().Set(output.DATA_TIME_STEP(), time)
"""


# create a new 'Programmable Source'
programmableSource1                          = ProgrammableSource()
programmableSource1.OutputDataSetType        = 'vtkStructuredGrid'
programmableSource1.Script                   = ReqDataScript
programmableSource1.ScriptRequestInformation = ReqInfoScript
programmableSource1.PythonPath               = ''

rep2                = Show()
rep2.Representation = 'Surface'
ColorBy(rep2, ('POINTS', 'gas_p/gas_p0'))
Render()
viewSG.ResetCamera()
Render()
