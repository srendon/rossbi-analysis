#!/usr/bin/env python3
# transfer one snapshot (file_from) onto mesh of other snapshot (file_to)
# low_res.data.h5 is the file with the low resolution while high_res.data.h5 is the file with the
# high resolution. At then end high_res.data.h5 is renamed restart.data.h5


import numpy as np
import argparse
import h5py
import os
import scipy.interpolate

def interpolation_new(xfrom, yfrom, zfrom, xto, yto):
    # We create the interpolator
    I2 = scipy.interpolate.interp2d(xfrom, yfrom, zfrom, kind='cubic', copy=False)

    # We extrapolate the old mesh to the new mesh
    zto     = I2(xto, yto)

    return zto

def transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, dname):
    dfrom = h5from[dname][()]          # In python3
    zto   = interpolation_new(xfrom, yfrom, dfrom, xto, yto)
    h5to[dname][...] = zto
   

def replace_time_vars(h5from, h5to):
    for i in ["time","dt","dt_back"]:
        h5to['vars'].attrs[i] = h5from['vars'].attrs[i]


def transfer(file_from, file_to):
    h5from = h5py.File(file_from, "r")
    h5to   = h5py.File(file_to, "r+")
    
    # Replace time variables
    replace_time_vars(h5from, h5to)

    xfrom = h5from["/mesh/x"][()]      # In python3
    yfrom = h5from["/mesh/y"][()]

    xto   = h5to["/mesh/x"][()]        # In python3
    yto   = h5to["/mesh/y"][()]
    
    transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/p")
    transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/rho")
    transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/vx")
    transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/vy")
    try:
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/vort")
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/strain")
    except:
        print ("Warning ! There is no gas vort and strain in *.data.h5 files");


    try:
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/div")
    except:
        print ("Warning ! There is no gas div in *.data.h5 files");


    try:
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/div_vx")
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/gas/div_vy")
    except:
        print ("Warning ! There is no gas div_vx and div_vy in *.data.h5 files");


    if ("/particles" in h5from):
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/particles/rho")
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/particles/vx")
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/particles/vy")
        try:
            transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/particles/vort")
            transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/particles/strain")
        except:
            print ("Warning ! There is no particles vort and strain in *.data.h5 files");


        try:
            transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/particles/div")
        except:
            print ("Warning ! There is no particles div in *.data.h5 files");


        try:
            transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/particles/div_vx")
            transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/particles/div_vy")
        except:
            print ("Warning ! There is no particles div_vx and div_vy in *.data.h5 files");


    else:
        print ("Warning ! Cannot find particles");


    if ("/selfgravity" in h5from):
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/selfgravity/force_g_x")
        transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/selfgravity/force_g_y")
        try:
            transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/selfgravity/potential_g")
            print ("There is potential_g in *.data.h5 files");
        except:
            print ("Warning ! There is no potential_g in *.data.h5 files");

        try:
            transfer_dataset(h5from, h5to, xfrom, yfrom, xto, yto, "/selfgravity/potential")
            print ("There is potential in *.data.h5 files");
        except:
            print ("Warning ! There is no potential in *.data.h5 files");

    else:
        print ("Warning! Cannot find selfgravity");
    h5to.close()

    os.system("mv " + file_to + " restart.data.h5") 

h5from = "low_res.data.h5"
h5to   = "high_res.data.h5"

transfer(h5from, h5to)


# ------------------------ Old -------------------------

# in sorted array find index of member which is nearest to value
#def find_nearest_id(array, value):
#    idx = np.searchsorted(array, value, side="left")
#    if idx > 0 and (idx == len(array) or np.abs(value - array[idx-1]) < np.abs(value - array[idx])):
#        return idx - 1
#    else:
#        return idx
#
#def transfer_dataset(h5from, h5to, map_x, map_y, dname):
#    dfrom = h5from[dname][()]          # In python3
#    dto   = h5to[dname][()]
#    for ito, ifrom in enumerate(map_x):
#        for jto, jfrom in enumerate(map_y):
#            dto[jto, ito] = dfrom[jfrom, ifrom]
#    h5to[dname][...] = dto
#
#def transfer(file_from, file_to):
#    h5from = h5py.File(file_from, "r")
#    h5to   = h5py.File(file_to, "r+")
#    
#    # Replace time variables
#    replace_time_vars(h5from, h5to)
#
#    xfrom = h5from["/mesh/x"][()]      # In python3
#    yfrom = h5from["/mesh/y"][()]
#
#    xto   = h5to["/mesh/x"][()]        # In python3
#    yto   = h5to["/mesh/y"][()]
#    
#    map_x = np.zeros(len(xto), dtype='int64')
#    map_y = np.zeros(len(yto), dtype='int64')
#
#    for i in range(len(xto)):
#        map_x[i] = find_nearest_id(xfrom, xto[i])
#    for i in range(len(yto)):    
#        map_y[i] = find_nearest_id(yfrom, yto[i])
#    transfer_dataset(h5from, h5to, map_x, map_y, "/gas/p")
#    transfer_dataset(h5from, h5to, map_x, map_y, "/gas/rho")
#    transfer_dataset(h5from, h5to, map_x, map_y, "/gas/vx")
#    transfer_dataset(h5from, h5to, map_x, map_y, "/gas/vy")
#    if ("/particles" in h5from):
#        transfer_dataset(h5from, h5to, map_x, map_y, "/particles/rho")
#        transfer_dataset(h5from, h5to, map_x, map_y, "/particles/vx")
#        transfer_dataset(h5from, h5to, map_x, map_y, "/particles/vy")
#    else:
#        print ("Warning! Cannot find particles");
#    if ("/selfgravity" in h5from):
#        transfer_dataset(h5from, h5to, map_x, map_y, "/selfgravity/forcex")
#        transfer_dataset(h5from, h5to, map_x, map_y, "/selfgravity/forcey")
#        transfer_dataset(h5from, h5to, map_x, map_y, "/selfgravity/potential")
#    else:
#        print ("Warning! Cannot find selfgravity");
#    h5to.close()
#
#    os.system("mv " + file_to + " restart.data.h5") 


    

    
