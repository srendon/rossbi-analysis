#!/usr/bin/env python3
import numpy as np
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("input",        help="Input txt file")
    parser.add_argument("output",       help="output png/jpg file")
    parser.add_argument("-xmin",        help="Minimal value of x", type=float)
    parser.add_argument("-xmax",        help="Maximal value of x", type=float)
    parser.add_argument("-ymin",        help="Minimal value of y", type=float)
    parser.add_argument("-ymax",        help="Maximal value of y", type=float)    
    parser.add_argument("-xlabel",      help="xlabel")
    parser.add_argument("-ylabel",      help="ylabel")
    parser.add_argument("-lw",          help="linewitdh")
    parser.add_argument("-cols",        help="Columns to plot", default=[0, 1], type=int, nargs=2)
    parser.add_argument("-legend",      help="Legend", type=str)
    parser.add_argument("-title",       help="title")
    parser.add_argument("-fitrange",    help="make fit only for point within this range", default=[-np.inf,np.inf], type=float, nargs=2)
    return parser.parse_args()

def plot(input, output, xlabel=None, ylabel=None, xmin=None, ymin=None, xmax=None, ymax=None, lw=None, cols=[0, 1], 
legend=None, title=None, fitrange=None):
    plt.clf()
    data = np.loadtxt(input)
    X=data[:,cols[0]]
    Y=data[:,cols[1]]
    
    
    def gaus(x, bias, A, x0, sigma):                                               
        return bias + A * np.exp(-(x-x0)**2/(2*sigma**2))

    idx = np.logical_and(X > fitrange[0], X < fitrange[1]) 
    popt,pcov = curve_fit(gaus,X[idx],Y[idx], p0 = [1,1,0,1])
    for p in popt:
        print(p)
    
    plt.plot(X,Y, linewidth=lw)
    plt.plot(X,gaus(X,*popt), linewidth=lw)
        
    plt.xlim([xmin,xmax])
    plt.ylim([ymin,ymax])
    legends = [legend, "gausfit " + str(fitrange)]
    plt.legend(legends)
#    if (legend):
#        plt.legend(legend)
    if (xlabel):
        plt.xlabel(xlabel)
    if (ylabel):
        plt.ylabel(ylabel)
        
    if (title):
        plt.title(title)
    plt.tight_layout()
    plt.savefig(output)
    

if __name__ == "__main__":
    args = standalone_args()
    plot(**vars(args))
