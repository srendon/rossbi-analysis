#!/usr/bin/env python3

from scipy.optimize import curve_fit
import numpy as np
from scipy import special as sp
from matplotlib import pyplot as plt


# Import data
data = np.loadtxt('profile269.txt')
xdata = data[:,0]; ydata = data[:,1]


# Function profiles
def func(x,a,b,c,x0,n): 
  return a*np.abs(sp.jv(1,b*(x-x0))/(b*(x-x0)))**n + c

def func2(x,a,b,x0): 
  return a*np.exp(-b*np.abs(x-x0)**2) + c

# Curve fit
popt, pcov = curve_fit(func,xdata, ydata)
popt2, pcov2 = curve_fit(func2,xdata, ydata)

# Plot curves
plt.plot(xdata, func(xdata, *popt), label='Airy')
plt.plot(xdata, func2(xdata, *popt2),label='Gaussian')
plt.plot(xdata,ydata, label='Numerics')


plt.show()

