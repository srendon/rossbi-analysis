#!/usr/bin/env python3

import os
import itertools
import make_combined_plots
import argparse


def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-outdir",      help="outdir", default = "analysis/maxpoint1/")
    return parser.parse_args()

def plot(outdir):
    input_variables_file = open('Input_variables.data')
    input_variables_str = input_variables_file.read()
    input_variables_file.close()
    
    if 'ONEPHASE' in input_variables_str:
       input = [
       outdir + "/gas_rho_ndg_x/plot",
       outdir + "/gas_p_ndg_x/plot",
       outdir + "/gas_vx_x/plot",
       outdir + "/gas_vy_nmg_x/plot",
       outdir + "/gas_rossby_x/plot",
       outdir + "/gas_div_x/plot",
       outdir + "/gas_rho_ndg_y/plot",
       outdir + "/gas_p_ndg_y/plot",
       outdir + "/gas_vx_y/plot",
       outdir + "/gas_vy_nmg_y/plot",
       outdir + "/gas_rossby_y/plot",
       outdir + "/gas_div_y/plot"] 
  
       out_prefix = outdir + "/combined_xy/combined_xy"
       make_combined_plots.make(input, out_prefix, tile="6x2")

    if 'ONEPHASE' not in input_variables_str:
       input = [
       outdir + "/gas_rho_ndg_x/plot",
       outdir + "/part_rho_ndg_x/plot",
       outdir + "/log_part_rho_ndg_x/plot",
       outdir + "/gpc_rho_x/plot",
       outdir + "/gas_p_ndg_x/plot",
       outdir + "/gpc_vx_x/plot",
       outdir + "/gpc_vy_x/plot",
       outdir + "/gpc_rossby_x/plot",
       outdir + "/gpc_div_x/plot",
       outdir + "/gpc_strain_x/plot"]
       
       out_prefix = outdir + "/combined_x/combined_x"
       make_combined_plots.make(input, out_prefix)

       input = [
       outdir + "/gas_rho_ndg_y/plot",
       outdir + "/part_rho_ndg_y/plot",
       outdir + "/log_part_rho_ndg_y/plot",
       outdir + "/gpc_rho_y/plot",
       outdir + "/gas_p_ndg_y/plot",
       outdir + "/gpc_vx_y/plot",
       outdir + "/gpc_vy_y/plot",
       outdir + "/gpc_rossby_y/plot",
       outdir + "/gpc_div_y/plot",
       outdir + "/gpc_strain_y/plot"]

       out_prefix = outdir + "/combined_y/combined_y"
       make_combined_plots.make(input, out_prefix)


if __name__ == "__main__":
    args = standalone_args()
    plot(**vars(args))
