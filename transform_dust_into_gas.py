#!/usr/bin/env python3
# Transform all dust component into gas.
# sigma_f_gas = sigma_gas + sigma_dust
# P_f_gas     = P_gas*(1+sigma_dust/sigma_gas)**gamma
# V_f_gas     = V_gas + V_dust
# Merge one file having only gas (for example a steady vortex) with a file having the particle phase
# The XXX.data.h5 file of the only gas phase must be renamed gas.data.h5
# The 001.data.h5 file of the gas and particles phases must be renamed gas_and_part.data.h5
# The output is the restart.data.h5 file

import h5py
import numpy as np

h5_gas_dust                  = h5py.File("gas_and_part","r")
h5_combined                  = h5py.File("restart.data.h5","w")

group_gas_dust               = {}
attribute_gas_dust           = {}

attribute_gas_dust["params"] = ["nx","ny","r_0"]
attribute_gas_dust["vars"]   = ["time","dt","dt_back"]

group_gas_dust["particles"]  = ["rho","vx","vy"]
group_gas_dust["gas"]        = ["p","rho","vx","vy"]
group_gas_dust["mesh"]       = ["x","y"]

if ("/selfgravity" in h5_gas_dust):
    group_gas_dust["selfgravity"] = ["force_g_x","force_g_y","potential_g"]
    group_gas_dust["selfgravity"] = ["force_p_x","force_p_y","potential_p"]
if ('div' in list(h5_gas_dust['gas'].keys())):
    group_gas_dust["gas"].append('div')
    group_gas_dust["gas"].append('vort')
    group_gas_dust["gas"].append('strain')
if ('div_vx' in list(h5_gas_dust['gas'].keys())):
    group_gas_dust["gas"].append('div_vx')
    group_gas_dust["gas"].append('div_vy')
    group_gas_dust["gas"].append('vort')
    group_gas_dust["gas"].append('strain')


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# -----------------
#       Gas       -
# ----------------- 
rho_gas         = h5_gas_dust['gas']['rho'][()]
p_gas           = h5_gas_dust['gas']['p'][()]
vx_gas          = h5_gas_dust['gas']['vx'][()]
vy_gas          = h5_gas_dust['gas']['vy'][()]
div_vx_gas      = h5_gas_dust['gas']['div_vx'][()]
div_vy_gas      = h5_gas_dust['gas']['div_vy'][()]
vort_gas        = h5_gas_dust['gas']['vort'][()]
strain_gas      = h5_gas_dust['gas']['strain'][()]


# -----------------
#       Dust      -  
# -----------------
rho_dust        = h5_gas_dust['particles']['rho'][()]
vx_dust         = h5_gas_dust['particles']['vx'][()]
vy_dust         = h5_gas_dust['particles']['vy'][()]
div_vx_dust     = h5_gas_dust['particles']['div_vx'][()]
div_vy_dust     = h5_gas_dust['particles']['div_vy'][()]
vort_dust       = h5_gas_dust['particles']['vortz'][()]
strain_dust     = h5_gas_dust['particles']['strain'][()]


# -----------------
X_threshold     = 0.15
gamma           = 1.4
X               = rho_dust / rho_gas
alpha           = np.where(X>=X_threshold, X_threshold,     X)
beta            = np.where(X>=X_threshold, 1-X_threshold+X, 1)
# -----------------


# -----------------
#    New gas      -
# -----------------
# Ici on s'assure que la quantité de mouvement et l'énergie sont conservées
# ça permet de trouver la nouvelle vitesse du gaz et sa pression
rho_gas_new     = rho_gas*beta
vx_gas_new      = (1/beta)*vx_gas     + (X/beta)*(1-alpha/X)*vx_dust
vy_gas_new      = (1/beta)*vy_gas     + (X/beta)*(1-alpha/X)*vy_dust
div_vx_gas_new  = (1/beta)*div_vx_gas + (X/beta)*(1-alpha/X)*div_vx_dust
div_vy_gas_new  = (1/beta)*div_vy_gas + (X/beta)*(1-alpha/X)*div_vy_dust
vort_gas_new    = (1/beta)*vort_gas   + (X/beta)*(1-alpha/X)*vort_dust
strain_gas_new  = (1/beta)*strain_gas + (X/beta)*(1-alpha/X)*strain_dust
p_gas_new       = beta*p_gas+(gamma-1)/2.0*rho_gas*(vx_gas**2.0+vy_gas**2.0-vx_gas_new**2.0-vy_gas_new**2.0)


# -----------------
#    New dust     -
# -----------------
# Ici on s'assure que la quantité de mouvement et l'énergie sont conservées
# ça permet de trouver la nouvelle vitesse du gaz et sa pression
rho_dust_new     = rho_gas*alpha
vx_dust_new      = vx_dust
vy_dust_new      = vy_dust
div_vx_dust_new  = div_vx_dust
div_vy_dust_new  = div_vy_dust
vort_dust_new    = vort_dust
strain_dust_new  = strain_dust

# -----------------
#  Self-gravity   -
# -----------------
#Old version
#pot_new  = h5_gas_dust['selfgravity']['potential'][()]

#New version
sg_gas        = h5_gas_dust['selfgravity']['potential_g'][()]
sg_dust       = h5_gas_dust['selfgravity']['potential_p'][()]
pot_new       = sg_gas + sg_dust



# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i in attribute_gas_dust.keys():
    group=h5_combined.create_group(i)
    for j in attribute_gas_dust[i]:
        group.attrs[j]=h5_gas_dust[i].attrs[j]

for i in group_gas_dust.keys():
    if i=='gas':
        for j in group_gas_dust[i]:
            if j=='rho':
                h5_combined.create_dataset(i+"/"+j, data=rho_gas_new[()])
            elif j=='p':
                h5_combined.create_dataset(i+"/"+j, data=p_gas_new[()])
            elif j=='vx':
                h5_combined.create_dataset(i+"/"+j, data=vx_gas_new[()])
            elif j=='vy':
                h5_combined.create_dataset(i+"/"+j, data=vy_gas_new[()])
            elif j=='div_vx':
                h5_combined.create_dataset(i+"/"+j, data=div_vx_gas_new[()])
            elif j=='div_vy':
                h5_combined.create_dataset(i+"/"+j, data=div_vy_gas_new[()])
            elif j=='vort':
                h5_combined.create_dataset(i+"/"+j, data=vort_gas_new[()])
            elif j=='strain':
                h5_combined.create_dataset(i+"/"+j, data=strain_gas_new[()])
            else:
                h5_combined.create_dataset(i+"/"+j, data=h5_gas_dust[i][j][()])

    elif i=='particles':
        for j in group_gas_dust[i]:
            if j=='rho':
                h5_combined.create_dataset(i+"/"+j, data=rho_dust_new[()])
            elif j=='vx':
                h5_combined.create_dataset(i+"/"+j, data=vx_dust_new[()])
            elif j=='vy':
                h5_combined.create_dataset(i+"/"+j, data=vy_dust_new[()])
            elif j=='div_vx':
                h5_combined.create_dataset(i+"/"+j, data=div_vx_dust_new[()])
            elif j=='div_vy':
                h5_combined.create_dataset(i+"/"+j, data=div_vy_dust_new[()])
            elif j=='vort':
                h5_combined.create_dataset(i+"/"+j, data=vort_dust_new[()])
            elif j=='strain':
                h5_combined.create_dataset(i+"/"+j, data=strain_dust_new[()])
            else:
                h5_combined.create_dataset(i+"/"+j, data=h5_gas_dust[i][j][()])



    #elif i=='selfgravity':
    #    for j in group_gas_dust[i]:
    #        if j=='potential_g':
    #            h5_combined.create_dataset(i+"/"+j,data=pot_new[()])
    #        else:
    #            h5_combined.create_dataset(i+"/"+j,data=h5_gas_dust[i][j][()])

    else:
        for j in group_gas_dust[i]:
            h5_combined.create_dataset(i+"/"+j,data=h5_gas_dust[i][j][()])



h5_gas_dust.close()
h5_combined.close()



