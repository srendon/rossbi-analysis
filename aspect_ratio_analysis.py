#!/usr/bin/env python3
# we take maxpoint1/gas_vx_y/profile*.txt and calculate distance between min and max  (delta_y)
# we take maxpoint1/gas_vy_nmg_x/profile*.txt and calculate distance between min and max  (delta_x)
# we read radius of maximum of "gas_p   / gas_p0" from analysis/maxpoint1/max_gas_p_ndg.txt
#  (we've used this value as a reference one, for trace profiles in maxpoint_analysis1.py)


import os, argparse, sys, cv2
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
from plot_1d_txt import plot
from auxiliary_functions import parse_idx_range, create_file, create_dir
from auxiliary_functions import parallel_info, print_time_execution
from auxiliary_functions import generate_error_message, read_input_variables_file
from auxiliary_functions import concat_vh, split_list, find_parameter
import __main__ as main
from mc_meshdata import meshdata, init_meshdata4sim, calc_vol
from ellipse import LsqEllipse
from matplotlib.patches import Ellipse
from plot_2d_eval4sim import plot_2darray
from functools import partial


def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-input_dir",  help="Input directory", default='.')
    parser.add_argument("-idx_range",  help="Snapshot index (idx), index range (idx1-idx2) or all files (None)", default=None)
    parser.add_argument("-output_dir", help="output_dir", default = "analysis/aspect/")
    parser.add_argument("-polar",       help="Perform a plot in polar coordinates", default=False,  type=bool)
    return parser.parse_args()


def mount_images(input_dirs_list, out_name):
    # make list of files 
    l = input_dirs_list
   
    list_images=[]
    for i in range(0, len(l)):
        list_images.append(cv2.imread(l[i]))

    # White blank image if needed
    blank_image = 255*np.ones(shape=cv2.imread(l[0]).shape, dtype=np.uint8)

    # Number of images
    N = len(l)

    if 4<=N<=8 and N%2==1:
        N = N+1
        list_images.append(blank_image)
    elif 9<=N:
        if N%3==1:
            list_images.append(blank_image)
            list_images.append(blank_image)
            N = N+2
        elif N%3==2:
            N = N+1
            list_images.append(blank_image)

    # Create final image
    if N<=3:
        img1_s     = concat_vh([list_images])
    elif N<=8:
        l0, l1     = split_list(list_images, n=2)
        img1_s     = concat_vh([l0, l1])
    else:
        l0, l1, l2 = split_list(list_images, n=3)
        img1_s     = concat_vh([l0, l1, l2])


    cv2.imwrite(out_name, img1_s)
    cv2.destroyAllWindows()

def calc_surf(x,y,z):
    nx, ny = np.shape(z)
    # Array with all elementary surfaces
    s = calc_vol(x, y)
    s2 = np.tile(s, (ny,1))
    s2 = np.transpose(s2)

    # Surface calculation
    surf = np.where(z!=0, s2, 0)
    surf_tot = surf.sum()

    return surf_tot

def function_pass(idx, batch_values, percentage_values, 
                  dict_max_gas_p_ndg_list, dict_max_gas_rho_ndg_list, 
                  dict_min_gas_rossby_list, phase_type, input_dir, 
                  output_dir, polar, period_r0):
    mainfile = input_dir + '/' + "%.3i.data.h5"%(idx)
    mc       = init_meshdata4sim(input_dir + "/",idx)
    x        = mc.calc("mesh_x")
    y        = mc.calc("mesh_y")
    time     = float(mc.calc("vars_time"))

    #----------------------------------------
    # Here we center the plots with respect to pressure maximum
    val     = mc.calc("gas_p  / gas_p0")
    imax    = np.unravel_index(val.argmax(),val.shape)
    imax_y  = y[imax[1]]
    factor_box = 1.0
    X0, Y0     = np.meshgrid(y,x)

    shift_y     = imax_y
    y           = y-(shift_y-np.pi*factor_box)
    y           = y%(2*np.pi*factor_box)

    X, Y     = np.meshgrid(y,x)
    x_ref    = 1.0 # 1AU
    #----------------------------------------

    if phase_type=='part':
        phase_type_modified='particles'
    else:
        phase_type_modified=phase_type

    for batch in batch_values:

        name_txt_aspect = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/aspect.txt"

        if batch == "rho" or batch == "p":
            var = mc.calc("%s_%s / %s_%s0"%(phase_type_modified, batch, phase_type_modified, batch))
            var_extremum=np.amax(var)
            if batch == "rho":
                r_max, theta_max = dict_max_gas_p_ndg_list[1], dict_max_gas_p_ndg_list[2]
                if phase_type_modified=='gas':
                    name_txt_semi_major = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/semi_major_axis.txt"
                    name_txt_semi_minor = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/semi_minor_axis.txt"
                    name_txt_phi        = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/phi.txt"

            if batch == "rho":
                r_max, theta_max = dict_max_gas_rho_ndg_list[1], dict_max_gas_rho_ndg_list[2]
                if phase_type_modified=='particles':
                    name_txt_semi_major = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/semi_major_axis.txt"
                    name_txt_semi_minor = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/semi_minor_axis.txt"
                    name_txt_phi        = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/phi.txt"

            if batch == "p" and phase_type_modified=='gas':
                r_max, theta_max = dict_max_gas_p_ndg_list[1], dict_max_gas_p_ndg_list[2]

            var = np.where(np.abs((np.remainder(X0-imax_y+np.pi,2*np.pi)-np.pi))<=0.785398, var, 0)

        if batch == "rossby":
            var = mc.calc("%s_%s"%(phase_type_modified, batch)) 
            var = np.where(np.abs((np.remainder(X0-imax_y+np.pi,2*np.pi)-np.pi))<=0.785398, var, 0)
            #var = np.where(var <= -0.06, var, 0)

            var_extremum=np.amin(var)
            r_max, theta_max = dict_min_gas_rossby_list[1], dict_min_gas_rossby_list[2]
            if phase_type_modified=='gas':
                name_txt_semi_major = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/semi_major_axis.txt"
                name_txt_semi_minor = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/semi_minor_axis.txt"
                name_txt_phi        = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/phi.txt"

        if batch == "selfgravity_potential":
            var              = mc.calc("%s"%(batch)) 
            var_extremum     = np.amin(var)
            r_max, theta_max = dict_max_gas_p_ndg_list[1], dict_max_gas_p_ndg_list[2]

        list_aspect     = []
        list_semi_minor = []
        list_semi_major = []

        if batch=='rho':
            list_surface = []

        # Theta_max is shifted
        theta_max        = theta_max-(shift_y-np.pi*factor_box)
        theta_max        = theta_max%(2*np.pi*factor_box)

        for percentage in percentage_values:
            figure_name = output_dir + "{phase_type}/".format(phase_type=phase_type) + batch + "/{perc}/fig{idx:03d}.png".format(idx=idx, perc=percentage)  
            print(figure_name)

            if batch == "rho" or batch == "p":
                var_level = percentage
            if batch == "rossby":
                var_level = percentage
            if batch == "selfgravity_potential":
                var_level = (1.0-percentage)*0.0 + percentage*var_extremum

            # Only for computing the semi-major axis
            if batch == 'rho':
                p_cut   = np.where(var >= var_level, var, 0)
                surface = calc_surf(x, y, p_cut)
                list_surface.append(surface)
          
            cs = plt.contour(X, Y, var, levels=[var_level], linewidths=3.5) 


            #----------------------------------------------
            # We extract the contour lines from a matplotlib contour plot

            # Necessary because at the boundaries of the box the contours are split
            nb_contour_lines = np.shape(cs.collections[0].get_paths())[0]
            r_contour        = np.empty((1,1))
            theta_contour    = np.empty((1,1))

            for i in range(0, nb_contour_lines, 1):
                p = cs.collections[0].get_paths()[i]
                v = p.vertices
                if i==0:
                    r_contour     = list(v[:,1])
                    theta_contour = list(v[:,0])
                else:
                    r_contour     = r_contour + list(v[:,1])
                    theta_contour = theta_contour + list(v[:,0])
            plt.clf()

            r_contour     = np.array(r_contour)
            theta_contour = np.array(theta_contour)
            
            x_contour     = np.ones(np.shape(r_contour))
            y_contour     = np.ones(np.shape(r_contour))

            for i in range(0,len(r_contour)):
                x_contour[i] = r_contour[i]-r_max
                y_contour[i] = r_max*np.sin(theta_contour[i]-theta_max)
            # End extraction contour points coordinates
            #----------------------------------------------

            
            #----------------------------------------------
	    	# Fit the Ellipse with Least Squares fitting of ellipses (Lsq) library
            positions_contour = np.array(list(zip(x_contour, y_contour)))
            try:
                reg = LsqEllipse().fit(positions_contour)
                # If the contour doesn't match an ellipse en error can be raised
                center, width, height, phi = reg.as_parameters()
                width_vortex               = max(width, height)
                height_vortex              = min(width, height)
                aspect                     = width_vortex/height_vortex
            except:
                aspect        = np.nan                       
                center        = np.nan
                width_vortex  = np.nan
                height_vortex = np.nan
                width         = np.nan
                height        = np.nan
                phi           = np.nan

            if type(aspect) != np.float64 : aspect = np.nan
            if type(height_vortex) != np.float64 : height_vortex = np.nan
            if type(width_vortex) != np.float64 : width_vortex = np.nan

            list_aspect.append(aspect)
            list_semi_minor.append(height_vortex)
            list_semi_major.append(width_vortex)
            # End fit ellipse
            #----------------------------------------------

            #  Plot contour and fitted ellipses
            plt.close('all')

            # If the figure doesn't exist we perform the plot. Otherwise not
            exists_figure_out = os.path.isfile(figure_name)

            if exists_figure_out==False:
                ## If the ellipse was fitted Plot and save the contour plot + the fitted ellipse
                if np.isnan(aspect)==False:

                    if batch=="rho":            
                        title=r"$\sigma_{{contour}}/ \sigma_{{0,gas}}=${percentage}    {time:.2f} T$_0$".format(percentage=percentage, time=time/2/np.pi/period_r0)
                    if batch=="p":            
                        title=r"$p_{{contour}}/ p_{{0,gas}}=${percentage}    {time:.2f} T$_0$".format(percentage=percentage, time=time/2/np.pi/period_r0)
                    if batch=="rossby":            
                        title=r"$Ro_{{contour}}=${percentage}".format(percentage=percentage)
                    if batch=="selfgravity_potential":            
                        title=r"$\Phi^{{grav}}_{{contour}}=${percentage} $\Phi^{{grav}}_{{max}}$    {time:.2f} T$_0$".format(percentage=percentage, time=time/2/np.pi/period_r0)

                    plot_2darray(y, x, var, figure_name, title=title, ellipse=[center, width, height, phi, r_max, theta_max], contour_val=var_level, polar=polar) 


        # Save the aspect ratio
        txt_to_save = '{time:.2f}'.format(time=time/2/np.pi/period_r0)
        for i in range(0, len(list_aspect), 1):
            txt_to_save += ' {aspect:20.15f}'.format(aspect=list_aspect[i])

        os.system("echo '{txt_to_save}' >> {filename}".format(txt_to_save=txt_to_save, filename=name_txt_aspect))

        if (batch=='rho' and phase_type_modified=='particles') or ((batch=='rho' or batch=='rossby') and phase_type_modified=='gas'):
            # Semi-major axis
            txt_to_save = '{time:.2f}'.format(time=time/2/np.pi/period_r0)
            for i in range(0, len(list_surface), 1):
                #txt_to_save += ' {semi_major:20.15f}'.format(
                #               semi_major= np.sqrt(list_surface[i]*x_ref**2*list_aspect[i])/np.pi)
                txt_to_save += ' {semi_major:20.15f}'.format(
                               semi_major=list_semi_major[i] )
            os.system("echo '{txt_to_save}' >> {filename}".format(txt_to_save=txt_to_save, filename=name_txt_semi_major))

            # Semi-minor axis
            txt_to_save = '{time:.2f}'.format(time=time/2/np.pi/period_r0)
            for i in range(0, len(list_surface), 1):
                #txt_to_save += ' {semi_minor:20.15f}'.format(
                #               semi_minor= np.sqrt(list_surface[i]*x_ref**2*list_aspect[i])/np.pi/list_aspect[i])
                txt_to_save += ' {semi_minor:20.15f}'.format(
                               semi_minor= list_semi_minor[i])
            os.system("echo '{txt_to_save}' >> {filename}".format(txt_to_save=txt_to_save, filename=name_txt_semi_minor))

            # Inclination
            txt_to_save = '{time:.2f}'.format(time=time/2/np.pi/period_r0)
            for i in range(0, len(list_surface), 1):
                txt_to_save += ' {phi:20.15f}'.format(
                               phi       = phi) 
            os.system("echo '{txt_to_save}' >> {filename}".format(txt_to_save=txt_to_save, filename=name_txt_phi))

#def function_pass(idx, batch_values, percentage_values, 
#                  dict_max_gas_p_ndg_list, dict_max_gas_rho_ndg_list, 
#                  dict_min_gas_rossby_list, phase_type, input_dir, 
#                  output_dir, polar, period_r0):

def partial_intermediate(idx, batch_values, percentage_values, 
                         dict_max_gas_p_ndg_list, dict_max_gas_rho_ndg_list, 
                         dict_min_gas_rossby_list, phase_type, input_dir, 
                         output_dir, polar, period_r0):
    return function_pass(idx, batch_values, percentage_values, 
                         dict_max_gas_p_ndg_list[idx], dict_max_gas_rho_ndg_list[idx], 
                         dict_min_gas_rossby_list[idx], phase_type, input_dir, 
                         output_dir, polar, period_r0)


def calc_new(phase_type, batch_values, percentage_values, period_r0, input_dir,  output_dir, idx_range, polar):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)

    idx1, idx2 = parse_idx_range(idx_range, input_dir)

    batch_values_modified = batch_values

    if phase_type == 'part':
        batch_values_modified = ['rho'] 

    # Here we load the file containing the positions of the pressure, density and vorticy extremums 
    if phase_type != 'part':
        pressure_to_load = input_dir + "/analysis/maxpoint1/max_{phase_type}_p_ndg.txt".format(phase_type=phase_type)
        rossby_to_load   = input_dir + "/analysis/maxpoint1/min_{phase_type}_rossby.txt".format(phase_type=phase_type)
    rho_to_load = input_dir + "/analysis/maxpoint1/max_{phase_type}_rho_ndg.txt".format(phase_type=phase_type)

    # first line is not read since it's the columns titles
    if phase_type != 'part':
        max_gas_p_ndg = np.loadtxt(pressure_to_load, skiprows=1)
        min_gas_rossby = np.loadtxt(rossby_to_load, skiprows=1)
    max_gas_rho_ndg = np.loadtxt(rho_to_load, skiprows=1)

    # numpy has trouble to read one line file so we reshape
    if max_gas_rho_ndg.ndim == 1: 
        if phase_type != 'part':
            max_gas_p_ndg = max_gas_p_ndg.reshape(1, max_gas_p_ndg.size)
            min_gas_rossby = min_gas_rossby.reshape(1, min_gas_rossby.size)
        max_gas_rho_ndg = max_gas_rho_ndg.reshape(1, max_gas_rho_ndg.size)

    # file is sorted with respect to Idx 
    if phase_type != 'part':
        max_gas_p_ndg = max_gas_p_ndg[max_gas_p_ndg[:,0].argsort()]
        min_gas_rossby = min_gas_rossby[min_gas_rossby[:,0].argsort()]
    max_gas_rho_ndg = max_gas_rho_ndg[max_gas_rho_ndg[:,0].argsort()]

    dict_max_gas_p_ndg, dict_max_gas_rho_ndg, dict_min_gas_rossby, = {}, {}, {}

    for i in range(0, len(max_gas_rho_ndg[:, 0])):
        IDX = int(max_gas_rho_ndg[i, 0])
        if phase_type == 'part':
            dict_max_gas_p_ndg[IDX] = 0
            dict_min_gas_rossby[IDX] = 0 
        else:
            dict_max_gas_p_ndg[IDX]  = max_gas_p_ndg[i,1:]
            dict_min_gas_rossby[IDX] = min_gas_rossby[i,1:]
        dict_max_gas_rho_ndg[IDX] = max_gas_rho_ndg[i,1:]


    for batch in batch_values_modified:
        for percentage in percentage_values:
            create_dir(output_dir + "{phase_type}/".format(phase_type=phase_type)  + batch + "/{perc}".format(perc=percentage))

        list_name_txt = []
		# Filename for the 3 types of aspect : rho, p, rossby	
        name_txt_aspect = output_dir + "{phase_type}/".format(phase_type=phase_type)  + batch + "/aspect.txt"
        list_name_txt.append(name_txt_aspect)
        if (batch=='rho' and phase_type=='part') or ((batch=='p' or batch=='rossby') and phase_type=='gas'):
            name_txt_semi_maj_axis = output_dir + "{phase_type}/".format(phase_type=phase_type)  + batch + "/semi_major_axis.txt"
            list_name_txt.append(name_txt_semi_maj_axis)
        for name_txt in list_name_txt:
            if os.path.isfile(name_txt):
                os.system("rm {filename}".format(filename=name_txt))
            create_file(name_txt)
        #os.system("echo '  Idx   {perc0:4}   {perc1:4}   {perc2:4}   {perc3:4}   {perc4:4}   {perc5:4}' >> {name_txt}"
        #        .format(perc0=percentage_values[0], perc1=percentage_values[1], perc2=percentage_values[2], 
        #        perc3=percentage_values[3], perc4=percentage_values[4], perc5=percentage_values[5], 
        #        name_txt=name_txt))

    # PARALLEL
    ncpus = multiprocessing.cpu_count()
    pool  = multiprocessing.Pool(processes=ncpus)


    partial_function_pass = partial(partial_intermediate, 
                            batch_values=batch_values_modified, 
                            percentage_values=percentage_values,
                            dict_max_gas_p_ndg_list=dict_max_gas_p_ndg, 
                            dict_max_gas_rho_ndg_list=dict_max_gas_rho_ndg, 
                            dict_min_gas_rossby_list=dict_min_gas_rossby,
                            phase_type=phase_type, 
                            input_dir=input_dir, 
                            output_dir=output_dir, 
                            polar=polar, 
                            period_r0=period_r0)

    pool.map(partial_function_pass, range(idx1, idx2 + 1, 1))

    #function_pass(idx, batch_values_modified, percentage_values,
    #         dict_max_gas_p_ndg[idx], dict_max_gas_rho_ndg[idx], dict_min_gas_rossby[idx],
    #         phase_type, input_dir, output_dir, polar, period_r0) for idx in range(idx1, idx2 + 1, 1)

    pool.close()

    # Make a fast movie for each percentage
    ###for batch in batch_values:
    ###    for percentage in percentage_values:
    ###        input_dir_movie = output_dir + batch + "/{perc}/".format(perc=percentage) 
    ###        output_movie_name = input_dir_movie + "movie_aspect.avi"
    ###        #os.system("rm -rf fig*.png")
    ###        os.system("rm -rf " + input_dir_movie + "movie_aspect.avi")
    ###        file_names, frame_array = [], []
    ###        fps = 4

    ###        for i in range(idx1, idx2 + 1 , 1):
    ###            file_names.append(input_dir_movie + 'fig{:03d}.png'.format(i))

    ###        for filename in file_names:
    ###            img = cv2.imread(filename)
    ###            height, width, layers = img.shape
    ###            size = (width, height)
    ###            frame_array.append(img)
    ###        out = cv2.VideoWriter(output_movie_name, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
    ###        
    ###        for i in range(len(frame_array)):
    ###            out.write(frame_array[i])
    ###        out.release()
    ###        # End fast movie




if __name__ == "__main__":
    args = standalone_args()
    prog_name           = os.path.basename(main.__file__)
    start_time          = parallel_info(prog_name)
    input_variables_str = read_input_variables_file(args.input_dir)
    period_r0           = find_parameter(input_variables_str, "period_r0 (years)")

    ## Check the number of phases
    list_phase_type=['gas']
    #if 'ONEPHASE' not in input_variables_str:
    #    list_phase_type.append('part')

	## New version
    #percentage_values = [0.5,0.9] # We can set as many values as we want
    #percentage_values = [1.5, 1.60, -0.08, -0.05] # We can set as many values as we want
    #percentage_values = [1.9, 1.7, 1.6, 1.5, 1.4] # We can set as many values as we want
    #batch_values      = ["rho", "p"]
    percentage_values = [-0.08]
    batch_values      = ["rho","rossby"]
    cols_new          = range(0, len(percentage_values)+1)

    #if 'NO_GRAVITY ' not in input_variables_str:
    #    batch_values.append("selfgravity_potential") 

    print('***')
    print('Start : Compute ellipse parameters and save contour plots')
    print('***\n')
    for phase_type in list_phase_type:
        calc_new(phase_type, batch_values, percentage_values, period_r0, **vars(args) )
    print('\n')
    print('***')
    print('End   : Compute ellipse parameters and save contour plots')
    print('***\n')


     
    print('***')
    print('Start : Plot, save and combine ellipse parameters with respect to time')
    print('***\n')
    for phase_type in list_phase_type:
        l_aspect   = []
        l_combined = [] 

        batch_values_modified = batch_values

        marker=None

        if phase_type == 'part':
            batch_values_modified = ['rho'] 

        for batch in batch_values_modified: 
            name_file   = args.output_dir + "{phase_type}/".format(phase_type=phase_type)  + batch + "/aspect.txt"
            output_file = args.input_dir + "/analysis/aspect/" + "{phase_type}/".format(phase_type=phase_type)  + batch  + "/aspect.png"
            l_aspect.append(output_file)  # For combined plots

            if batch=='p':
                marker='+'
            elif batch=='rossby':
                marker='x'


            plot(input=name_file, output=output_file, ymin=4.5, ymax=12.5, cols=cols_new, marker=marker, legends=percentage_values , xlabel=r'Time (in T$_0$)', ylabel=r'$\chi$', 
                 title='Aspect ratio (computed thanks to {var})'.format(var=batch))

            if (batch=='rho' and phase_type=='part') or ((batch=='rho' or batch=='rossby') and phase_type=='gas'):
                # Semi-major axis
                name_file   = args.output_dir + "{phase_type}/".format(phase_type=phase_type)  + batch + "/semi_major_axis.txt"
                output_file = args.input_dir + "/analysis/aspect/" + "{phase_type}/".format(phase_type=phase_type)  + batch  + "/semi_major_axis.png"
                l_combined.append(output_file) # For combined plots
                plot(input=name_file, output=output_file, ymin=0.0, ymax=13.0, cols=cols_new, legends=percentage_values ,xlabel='idx', ylabel=r'a (in AU)', 
                     title='Semi major axis (computed thanks to {var})'.format(var=batch))

                # Semi-minor axis
                name_file   = args.output_dir + "{phase_type}/".format(phase_type=phase_type)  + batch + "/semi_minor_axis.txt"
                output_file = args.input_dir + "/analysis/aspect/" + "{phase_type}/".format(phase_type=phase_type)  + batch  + "/semi_minor_axis.png"
                l_combined.append(output_file) # For combined plots
                plot(input=name_file, output=output_file, ymin=0.0, ymax=13.0, cols=cols_new, legends=percentage_values ,xlabel='idx', ylabel=r'a (in AU)', 
                     title='Semi minor axis (computed thanks to {var})'.format(var=batch))

                # Phi 
                name_file   = args.output_dir + "{phase_type}/".format(phase_type=phase_type)  + batch + "/phi.txt"
                output_file = args.input_dir + "/analysis/aspect/" + "{phase_type}/".format(phase_type=phase_type)  + batch  + "/phi.png"
                l_combined.append(output_file) # For combined plots
                plot(input=name_file, output=output_file, ymin=-0.3, ymax=0.3, cols=cols_new, legends=percentage_values ,xlabel='idx', ylabel=r'$\phi$ (in rad)', 
                     title='Ellipse inclination : phi (computed thanks to {var})'.format(var=batch))


        # Combine all aspect plots (for all the batches and versions)
        out_combined    = args.input_dir + "/analysis/aspect/{phase_type}/combined.jpg".format(phase_type=phase_type)
        #os.system("montage -geometry +0+0 " + l_aspect + " " + out_combined) #Old with ImageMagick
        mount_images(l_aspect, out_combined)

        # Combine all semi major axis plots
        out_combined    = args.input_dir + "/analysis/aspect/{phase_type}/combined_semi_major.jpg".format(phase_type=phase_type)
        mount_images(l_combined, out_combined)
        #os.system("montage -geometry +0+0 " + l_combined + " " + out_combined) #Old with ImageMagick 

    print('\n')
    print('***')
    print('End   : Plot, save and combine ellipse parameters with respect to time')
    print('***\n')

    print_time_execution(start_time, prog_name)
    
	
