#!/bin/bash

plotall_2d_eval4sim.py . "gas_vort-particles_vort" analysis/movies/vort_diff_1e-6/plot -zmin=-1e-6 -zmax=1e-6
plotall_2d_eval4sim.py . "gas_div-particles_div"   analysis/movies/div_diff_1e-5/plot -zmin=-1e-5 -zmax=1e-5
