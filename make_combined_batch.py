#!/usr/bin/env python3

import os
import itertools
import argparse
import cv2
import numpy as np
from sys import stdout
from make_movie import make_fast_movie
from mc_meshdata import parse_idx_range
from auxiliary_functions import make_default_input
import multiprocessing
from functools import partial

def standalone_args():
    parser = argparse.ArgumentParser("This tool must be run in the folder where are located the *.data.h5 files")
    parser.add_argument("-input_dir",   help="input directory", default='./analysis/movies/')
    parser.add_argument("-out_prefix",  help="out prefix. It is mandatory that the name of the prefix should contain " + 
                                             "the string 'combined'", default='combined/')
    parser.add_argument("-idx_range", help="Snapshot index (idx), index range (idx1-idx2) or all files (None)", default=None)
    parser.add_argument("-tile",        help="tile option of montage. For instance : tile : '2x2'")
    parser.add_argument("-batches",     help="Batches defined by the user")
    return parser.parse_args()
   
def combined_batch(input_dir, batches=None, **kwargs):
    default_input = make_default_input(input_dir)
    if batches==None or batches=='':
        combined_batch=default_input
    else:
        combined_batch = []
        for p in batches:
            if p in default_input:
                combined_batch.append(p)

    for i in range(len(combined_batch)):
        combined_batch[i] = input_dir + combined_batch[i] + '/'

    # In order to avoid a "combinedCEPTION" xD
    for elt in combined_batch:
        if 'combined' in elt:
            combined_batch.remove(elt)

    return combined_batch
            
def concat_vh(list_2d): 
    return cv2.vconcat([cv2.hconcat(list_h) for list_h in list_2d])

def mount_images(i, input_dir, out_prefix, combined_batch_input, tile):
    postfix = "plot%.3i.jpg"%i;
    out = input_dir + out_prefix + postfix
    # make list of files 
    l = [s + postfix for s in combined_batch_input]
   
    def split_list(alist, n=1):
        n_split = np.array_split(alist, n)
        result = []
        for array in n_split:
            result.append(list(array))
        return result

    list_images=[]
    for i in range(0, len(l)):
        list_images.append(cv2.imread(l[i]))

    # White blank image if needed
    blank_image = 255*np.ones(shape=cv2.imread(l[0]).shape, dtype=np.uint8)

    # Number of images
    N = len(l)

    if 4<=N<=8 and N%2==1:
        N = N+1
        list_images.append(blank_image)
    elif 9<=N:
        if N%3==1:
            list_images.append(blank_image)
            list_images.append(blank_image)
            N = N+2
        elif N%3==2:
            N = N+1
            list_images.append(blank_image)

    # Create final image
    if N<=3:
        img1_s     = concat_vh([list_images])
    elif N<=8:
        l0, l1     = split_list(list_images, n=2)
        img1_s     = concat_vh([l0, l1])
    else:
        l0, l1, l2 = split_list(list_images, n=3)
        img1_s     = concat_vh([l0, l1, l2])


    cv2.imwrite(out, img1_s)

    cv2.destroyAllWindows()




def make_combined_plots(input_dir='.', out_prefix='combined/', idx_range=None, tile=None, batches=None, contour=None):
    combined_batch_input = combined_batch(input_dir + '/analysis/movies/', batches=batches )

    if contour != None and contour != '':
        out_prefix=out_prefix[:-1] + "_contour/"

    os.system("mkdir -p " + os.path.dirname( input_dir + '/analysis/movies/' + out_prefix ))

    idx1, idx2 = parse_idx_range(idx_range, input_dir)

    # PARALLEL
    # get number of cpus available to job
    try:
        ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
    except KeyError:
        ncpus = multiprocessing.cpu_count()

    pool = multiprocessing.Pool(processes=ncpus)

    partial_mount_images = partial(mount_images, input_dir=input_dir + '/analysis/movies/',
                                                 out_prefix=out_prefix, 
                                                 combined_batch_input=combined_batch_input, 
                                                 tile=tile)

    pool.map(partial_mount_images, range(idx1, idx2+1, 1))
    pool.close()

    # This is serial
    make_fast_movie(input_dir, input_dir + '/analysis/movies/' + out_prefix, idx_range)



if __name__ == "__main__":
    args = standalone_args()
    make_combined_plots(**vars(args))


