#!/usr/bin/env python3

from plot_2d_eval4sim import plot_batch
import numpy as np
import os
import argparse
from sys import stdout
from make_movie import make_all_movies
from make_combined_batch import make_combined_plots
from auxiliary_functions import read_input_variables_file 
from auxiliary_functions import check_contour, check_contour_combined
from auxiliary_functions import parallel_info, print_time_execution
from auxiliary_functions import read_batch_file, find_parameter  
import __main__ as main

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-input_dir",     help="Input directory", default='.')
    parser.add_argument("-idx_range",     help="Snapshot index (idx), index range (idx1-idx2) or all files (None)", default=None)
    parser.add_argument("-contour",       help="Make contour plots (True) or not (None)", default=None)
    parser.add_argument("-contour_val",   help="Make contour plots (True) or not (None)", default=None)
    parser.add_argument("-centered_plot", help="Center all plots with respect to gas pressure maximum (True) or not (None)", default=None)
    parser.add_argument("-polar",         help="Perform a plot in polar coordinates", default=False,  type=bool)
    return parser.parse_args()


def create_batch(input_dir, contour):
    dir_plots           = input_dir + "/analysis/movies/"
    input_variables_str = read_input_variables_file(input_dir)
    input_batch         = read_batch_file(input_dir)

    # Creates a batch list from batch.txt file
    batch = read_batch_file(input_dir)

    batch = check_contour(batch, contour)

    return batch



def run_plot_batch(idx_range=None, contour=None, centered_plot=None, contour_val=None, input_dir='./', output_dir=None, polar=None):
    ##############################################
    ### Make a plot for each batch  (Parallel) ###
    ##############################################
    batch = create_batch(input_dir, contour)
    input_variables_str = read_input_variables_file(input_dir)
    if 'NO_PLANET' not in input_variables_str:
        planet=True
    else:
        planet=None
    if 'NO_BOX' not in input_variables_str:
        box_size=find_parameter(input_variables_str, 'Box_size')
    else:
        box_size=None

    period_r0 = find_parameter(input_variables_str, "period_r0 (years)")


    print('***')
    print('Start : Plot and save each batch')
    print('***\n')
    plot_batch(input_dir, batch, idx_range, contour, planet, centered_plot, 
               contour_val, box_size, period_r0, polar)
    print('\n')
    print('***')
    print('End   : Plot and save each batch')
    print('***\n')

    ##############################################
    ### Make a movie for each batch (Parallel) ###
    ##############################################
    print('***')
    print('Start : Make a movie for each batch')
    print('***\n')
    batch_folders = list(zip(*batch))[1]
    make_all_movies(input_dir, batch=batch_folders, idx_range=idx_range) 
    print('\n')
    print('***')
    print('End   : Make a movie for each batch')
    print('***\n')

    ##############################################
    ### Combine various plots and make a movie ###
    ##############################################
    # By default all plots in analysis/movies are combined
    print('***')
    print('Start : Combine various plots and make a movie')
    print('***\n')
    make_combined_plots(input_dir, tile=None, idx_range=idx_range, 
                        contour=contour)
    print('\n')
    print('***')
    print('End   : Combine various plots and make a movie')
    print('***\n')
    
###    batch2 = [ "gas_rho_ndg", "part_rho_by_gas_rho"]
###    name_batch2 = "combined2/" 
###    name_batch2, batch2 = check_contour_combined(name_batch2, batch2, contour) 
###    print('***')
###    print('Start : Combine various plots and make a movie')
###    print('***\n')
###    make_combined_plots(input_dir, out_prefix = name_batch2, batches = batch2,
###                        tile='2x1', idx_range=idx_range, contour=contour)
###    #make_combined_plots(input_dir, out_prefix = name_batch2, batches = batch2, tile= None, idx_range=idx_range)
###    print('\n')
###    print('***')
###    print('End   : Combine various plots and make a movie')
###    print('***\n')

if __name__ == "__main__":
    args       = standalone_args()
    prog_name  = os.path.basename(main.__file__)
    start_time = parallel_info(prog_name)
    run_plot_batch(**vars(args))   
    print_time_execution(start_time, prog_name)

