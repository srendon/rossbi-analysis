#!/usr/bin/env python3
import numpy as np
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.interpolate
import h5py
import argparse
import mc_meshdata
import os

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("dir",          help="simulation directory")
    parser.add_argument("idx_range",    help="Snapshot index or index range idx1-idx2 (- all snapshots)")
    parser.add_argument("str4calc",     help="String for calculation (gas_rho - gas_rho0 for example)");
    parser.add_argument("output",       help="Output plot prefix")
    parser.add_argument("-xlabel",      help="xlabel", default="X (AU)")
    parser.add_argument("-ylabel",      help="ylabel", default="Y (AU)")
    parser.add_argument("-title",       help="title")
    parser.add_argument("-xmin",        help="Minimal value of x", type=float)
    parser.add_argument("-xmax",        help="Maximal value of x", type=float)
    parser.add_argument("-ymin",        help="Minimal value of y", type=float)
    parser.add_argument("-ymax",        help="Maximal value of y", type=float)
    parser.add_argument("-zmin",        help="Minimal value of z", type=float)
    parser.add_argument("-zmax",        help="Maximal value of z", type=float)
    parser.add_argument("-cmap",        help="Color map", default='jet');
    parser.add_argument("-nlin",        help="Size of meshgrid: nlin x nlin", default=1000,  type=int);
    
    return parser.parse_args()
    

def plot(dir, idx_range, str4calc, output, title = None, **kwargs):    
    if (title == None):
        title = str4calc        
        
    for idx in mc_meshdata.idx_range_it(dir, idx_range):
        d = mc_meshdata.init_meshdata4sim(dir, idx)
        r   = d.calc("x2d")
        phi = d.calc("y2d")
        
        x = r * np.cos(phi)
        y = r * np.sin(phi)
        
        z = d.calc(str4calc)        
        if ("-" in idx_range):
            title_idx = title + " " + str(idx)
            plot_2darray(y, x, z, output + str(idx), title= title_idx, **kwargs)                
        else:
            # special case for single idx
            plot_2darray(y, x, z, output, title= title, **kwargs)                
            
# simple batch
def plot_batch(dir, batch):
    strs, out_prefixs = zip(*batch)
    
    # create directories
    for p in out_prefixs:
        outdir = os.path.dirname(p)
        os.system("mkdir -p " + outdir)
        
    for idx, x, y, zs in mc_meshdata.eval_fullsim_it(dir, strs):
        for i in range(len(zs)):
            plot_2darray(y,x,zs[i], out_prefixs[i] + "%.3i.jpg"%idx, title = strs[i] + "     " + str(idx))
    
def plot_2darray(x,y,z, output, 
                 xmin=None, xmax=None, ymin=None, ymax=None, 
                 zmin=None, zmax=None, nlin=1000, xlabel="X (AU)", ylabel="Y (AU)", title=None, cmap='jet'):            
    #I2 = scipy.interpolate.NearestNDInterpolator((x, y), z)
    
    points = np.array(list(zip(x.reshape(-1),y.reshape(-1))))
    z      = z.reshape(-1)
    
    I2 = scipy.interpolate.NearestNDInterpolator(points,z)
    
    def set_max(a, array):
        return array.max() if a==None else a
    
    def set_min(a, array):
        return array.min() if a==None else a
        
    xmax = set_max(xmax, x);
    ymax = set_max(ymax, y);

    xmin = set_min(xmin, x);
    ymin = set_min(ymin, y);


    xi, yi = np.linspace(xmin, xmax, nlin), np.linspace(ymin, ymax, nlin)
    
    xv, yv = np.meshgrid(xi, yi)
    zi     = I2(xv, yv)

    zmax = set_max(zmax, zi);
    zmin = set_min(zmin, zi);

    plt.clf()
    plt.imshow(zi, vmin=zmin, vmax=zmax, origin='lower', extent=[xmin, xmax, ymin, ymax], aspect='auto')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    if (title != None):
        plt.title(title)

    if (cmap != None):
        plt.set_cmap(cmap)
    
    plt.colorbar();


    plt.savefig(output);

        
if __name__ == "__main__":                        
    args = standalone_args()
    plot(**vars(args))
    
    
