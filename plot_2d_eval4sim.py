#!/usr/bin/env python3
import numpy as np
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import colors
import scipy.interpolate
import h5py
import argparse
import mc_meshdata
import gc
import multiprocessing
import os
import copy
from sys import stdout
from auxiliary_functions import parse_idx_range
from functools import partial
# see : https://python.omics.wiki/multiprocessing_map/multiprocessing_partial_function_multiple_arguments
# see : https://stackoverflow.com/questions/15331726/how-does-functools-partial-do-what-it-does

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("dir",          help="simulation directory")
    parser.add_argument("idx_range",    help="Snapshot index or index range idx1-idx2 (- all snapshots)")
    parser.add_argument("str4calc",     help="String for calculation (gas_rho - gas_rho0 for example)");
    parser.add_argument("output",       help="Output plot prefix")
    parser.add_argument("-xlabel",      help="xlabel", default="theta (rad)")
    parser.add_argument("-ylabel",      help="ylabel", default="radius (AU)")
    parser.add_argument("-title",       help="title")
    parser.add_argument("-xmin",        help="Minimal value of x", type=float)
    parser.add_argument("-xmax",        help="Maximal value of x", type=float)
    parser.add_argument("-ymin",        help="Minimal value of y", type=float)
    parser.add_argument("-ymax",        help="Maximal value of y", type=float)
    parser.add_argument("-zmin",        help="Minimal value of z", type=float)
    parser.add_argument("-zmax",        help="Maximal value of z", type=float)
    parser.add_argument("-cmap",        help="Color map", default='jet');
    parser.add_argument("-nlin",        help="Size of meshgrid: nlin x nlin", default=2000,  type=int);
    parser.add_argument("-polar",       help="Perform a plot in polar coordinates", default=False,  type=bool);
    
    return parser.parse_args()
    

def plot(dir, idx_range, str4calc, output, title = None, **kwargs):    
    if (title == None):
        title = str4calc        
        
    for idx in mc_meshdata.idx_range_it(dir, idx_range):
        d = mc_meshdata.init_meshdata4sim(dir, idx)
        x = d.calc("mesh_x")
        y = d.calc("mesh_y")
        z = d.calc(str4calc)        
        if ("-" in idx_range):
            title_idx = title + " " + str(idx)
            plot_2darray(y, x, z, output + str(idx), title= title_idx, **kwargs)                
        else:
            # special case for single idx
            plot_2darray(y, x, z, output, title= title, **kwargs)                

            
def save_plot(idx, directory, out_prefix, strs, contour, planet, centered_plot, contour_val, box_size, period_r0, polar):
    planet_coord = None
    imax_y=None
    if centered_plot=='True':
        # Find the positions of the maximum of density
        d       = mc_meshdata.init_meshdata4sim(directory, idx)
        mesh_x  = d.calc("mesh_x")
        mesh_y  = d.calc("mesh_y")
        val     = d.calc("gas_p  / gas_p0")
        imax    = np.unravel_index(val.argmax(),val.shape)
        imax_y  = mesh_y[imax[1]]
        if planet==True:
            xp = d.calc("planet_x")
            yp = d.calc("planet_y")
            planet_coord=[xp,yp]
    else:
        if planet==True:
            d  = mc_meshdata.init_meshdata4sim(directory, idx)
            xp = d.calc("planet_x")
            yp = d.calc("planet_y")
            planet_coord=[xp,yp]

    # Do the plots and save them
    x, y , zs, time = mc_meshdata.idx_range_it(directory, strs, idx)
    for i in range(len(zs)):
        fout = out_prefix[i] + "plot%.3i.jpg"%idx
        print(fout)
        exists_fout = os.path.isfile(fout)
        if exists_fout==False: #Check if the file exists, if not the plot is done
            plot_2darray(y, x, zs[i], out_prefix[i] + "plot%.3i.jpg"%idx, 
                         title = strs[i] + "     " + "{:.2f}".format(time/2/np.pi/period_r0) + r" T$_0$", 
                         contour = contour, planet_coord=planet_coord, imax_y=imax_y, 
                         contour_val=contour_val, box_size=box_size, polar=polar)


# simple batch
def plot_batch(dir, batch, idx_range, contour, planet, centered_plot, contour_val, box_size, period_r0, polar):
    strs, out_prefix = zip(*batch)
    idx1, idx2 = parse_idx_range(idx_range, dir)   

    # create directories
    for p in out_prefix:
        outdir = os.path.dirname(p)
        os.system("mkdir -p " + outdir)


    # Parallel loop
    # get number of cpus available to job
    try:
        ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
    except KeyError:
        ncpus = multiprocessing.cpu_count()

    pool              = multiprocessing.Pool(processes=ncpus)

    partial_save_plot = partial(save_plot, directory=dir, out_prefix=out_prefix, 
                                strs=strs, contour=contour, planet=planet, 
                                centered_plot=centered_plot, contour_val=contour_val, 
                                box_size=box_size, period_r0=period_r0, polar=polar)

    pool.map(partial_save_plot, range(idx1, idx2 + 1 , 1))
    pool.close()


def plot_2darray(x,y,z, output, 
                 xmin=None, xmax=None, ymin=40, ymax=60, 
                 zmin=None, zmax=None, nlin=4000, xlabel=r"$\theta$ (rad)", 
                 ylabel="radius (AU)", title=None, cmap='seismic',
                 contour=None, planet_coord=None, imax_y=None, ellipse=None, 
                 contour_val=None, box_size=None, polar=False, threshold=None):            
    # xmin and ymin should be in degrees
    #polar=False

    # For centering the box in case the azimuth range is [0,2*pi*box_size]
    if box_size == None:
        factor_box = 1.0
    else:
        factor_box = box_size

    if imax_y != None:
        # For centering the plots
        shift_x = imax_y
        x       = x-(shift_x-np.pi*factor_box)
        x       = x%(2*np.pi*factor_box)

    I2 = scipy.interpolate.interp2d(x, y, z, kind='linear', copy=False)

    
    def set_max(a, array):
        return np.nanmax(array) if a==None else a
    
    def set_min(a, array):
        return np.nanmin(array) if a==None else a
    
    xmax = set_max(xmax, x);
    ymax = set_max(ymax, y);

    xmin = set_min(xmin, x);
    ymin = set_min(ymin, y);

    xi, yi = np.linspace(xmin, xmax, nlin), np.linspace(ymin, ymax, nlin)
    zi     = I2(xi, yi)

    X, Y = np.meshgrid(xi,yi)


    if threshold!=None:
        threshold_type  = str(threshold[0])
        threshold_value = float(threshold[1])
        if threshold_type=='lower':    
            zi = np.where(zi<=threshold_value, np.nan, zi)
        elif threshold_type=='greater':
            zi = np.where(zi>=threshold_value, np.nan, zi)

    #if np.isnan(zi).any()==False:
    zmin = float(set_min(zmin, zi));
    zmax = float(set_max(zmax, zi));


    if any(x in title for x in ["rossby","vort","div","gas_vy-gas_vy0","gas_vx-gas_vx0","gas_gradh"]):
        zmax = max(np.abs(zmin),zmax)
        #zmax=0.60
        zmin = -zmax
   
##    if any(x in title for x in ["gas_rho / gas_rho0"]):
##        zmax = 4.5 
##        #zmax=0.60
##        zmin = 0.7

    #if any(x in title for x in ["log10(particles_rho / gas_rho0)"]):
    #    zmax = 200 
    #    #zmax=0.60
    #    zmin = 0.0002

    plt.clf()

    # Creation of the figure and ax object
    if polar==None or polar==False or polar=='False':
        fig, ax      = plt.subplots()
    else:
        fig, ax      = plt.subplots(subplot_kw={'projection': 'polar'})

    # Colormap options
    if (cmap != None):
        #cmap = copy.copy(matplotlib.cm.get_cmap('seismic')) # New matplotlib versions don't allow cmap modification
        plt.set_cmap(cmap)

        # It is not allowed to modify the state of a globally registered colormap
        # Therefore we need to copy it first.
        current_cmap = copy.copy(matplotlib.cm.get_cmap()) 
        current_cmap.set_bad(color='grey')
  
    # Contour options
    if contour == None or contour == '':
        #plt.imshow(X,Y,zi, origin='lower', extent=[xmin, xmax, ymin, ymax], aspect='auto')
        #if any(x in title for x in ["log10(particles_rho / gas_rho0)"]):
        #    cs=ax.pcolormesh(X, Y, zi,  shading='auto', norm=matplotlib.colors.LogNorm(vmin=0.002, vmax=200) ) # It's when imshow doesn't work. Apparently is a problem coming from python 3.7
        #else:
        #    cs=ax.pcolormesh(X, Y, zi, vmin=zmin, vmax=zmax, shading='auto' ) # It's when imshow doesn't work. Apparently is a problem coming from python 3.7
        cs=ax.pcolormesh(X, Y, zi, vmin=zmin, vmax=zmax, shading='auto' ) # It's when imshow doesn't work. Apparently is a problem coming from python 3.7
        fig.colorbar(cs, orientation='vertical', pad=0.1)
    else:
        #ax.contour(X, Y, zi, levels = 80, vmin=zmin, vmax=zmax, , linewidths=0.5)
        #list_contour=[-0.13, -0.1275,-0.126, -0.125,-0.12,-0.09,-0.08, -0.03,-0.015]
        list_contour = [0.9*zmin, 0.8*zmin, 0.7*zmin, 0.6*zmin, 0.5*zmin, 0.4*zmin, 0.3*zmin, 0.2*zmin, 0.1*zmin]
        #list_contour = [0.9*zmin, 0.80*zmin, 0.7*zmin, 0.5*zmin, 0.3*zmin, 0.2*zmin, 0.1*zmin]
        #cs=ax.pcolormesh(X, Y, zi, vmin=zmin, vmax=zmax, shading='auto' ) # It's when imshow doesn't work. Apparently is a problem coming from python 3.7
        #fig.colorbar(cs, orientation='vertical', pad=0.1)
        #list_contour = [0.99*zmin, 0.9*zmin, 0.8*zmin, 0.7*zmin, 0.6*zmin, 0.5*zmin]
        ax.contour(X, Y, zi, levels = list_contour, vmin=zmin, vmax=zmax, linewidths=1.0)
        #ax.clabel(cs, inline=True, fontsize=10)

    if contour_val!=None and contour_val!='':
        ax.contour(X, Y, zi, levels=[float(contour_val)],vmin=zmin, vmax=zmax, linestyles='dashdot', colors='k')


    # xticks and yticks options
    #plt.xticks([xmin, (xmin+xmax)/2.0, xmax],[r"%.2f"%xmin,r"%.2f$\pi$"%((xmin+xmax)/2.0/np.pi),r"%.2f $\times 2\pi$"%(xmax/(2.0*np.pi))])
    if polar==None or polar==False or polar=='False':
        #plt.xticks([xmin, (xmin+xmax)/2.0, xmax],[r"%.0f"%xmin,r"$\pi$",r"$2\pi$"])
        if factor_box==1.0:
            #plt.xticks([xmin, (xmin+xmax)/2.0, xmax],[r"${3\pi}/{4}$",r"$\pi$",r"${5\pi}/{4}$"], fontsize=10)
            plt.xticks([xmin, (xmin+xmax)/2.0, xmax],[r"$0$",r"$\pi$",r"$2\pi$"])
        else:
            plt.xticks([xmin, (xmin+xmax)/2.0, xmax],[r"%.2f"%xmin,r"%.2f$\pi$"%((xmin+xmax)/2.0/np.pi),r"%.2f $\times 2\pi$"%(xmax/(2.0*np.pi))])

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
    else:
        ax.set_theta_zero_location('N', offset=180) # Place the vortex in the North region
        if contour_val!=None or contour_val!='':
            #ax.set_rorigin(1.0*ymin)
            ax.set_rorigin(0.85*ymin)
            #ax.set_rorigin(0.95*ymin)
        else:
            ax.set_rorigin(0.9*ymin)
            #ax.set_rorigin(0.90*ymin)
            #ax.set_rorigin(1.0*ymin)
        #ax.set_rmin(43.0)
        #ax.set_rmax(52.0)
        #ax.set_rmin(47.5)
        #ax.set_rmax(53)
        #ax.set_thetamin(135)
        #ax.set_thetamax(225)
        ax.set_rmin(ymin)
        ax.set_rmax(ymax)

        ax.grid(True, linewidth=0.5, linestyle='dashed', alpha=0.6)
        ax.set_rlabel_position(45)
        ax.yaxis.get_major_locator().base.set_params(nbins=5)

        rlabels = ax.get_ymajorticklabels()
        if 'log' in title:
            for label in rlabels:
                label.set_color('black')
        elif ('rho' in title) or ('gas_p' in title) or ('sigma' in title) or ('p_{{0,gas}}' in title):
            for label in rlabels:
                label.set_color('white')



    title=set_title(title)
    if (title != None):
        ax.set_title(title, y=1.08)


    # Planet options
    if planet_coord!=None:
        x_p, y_p = planet_coord[1], planet_coord[0]
        if imax_y != None:
            # For centering the plots
            shift_x = imax_y
            x_p     = x_p-(shift_x-np.pi*factor_box)
            x_p     = x_p%(2*np.pi*factor_box)
        plt.plot(x_p, y_p,'ro')

    # Fit ellipse options
    if ellipse!=None:
        center, width, height, phi, r_max, theta_max = ellipse
        if type(phi) != np.complex128:
            # Conversion of the cartesian grid ellipse parameters
            # in cylindrical grid ellipse parameters
            y_center = center[0]+r_max
            x_center = np.arcsin(center[1]/r_max)+theta_max
            x_center = x_center%(2.0*np.pi)
   
            # I remind that axis are inverted here
            width, height  = np.arcsin(height/r_max), width

            # Ellipse creation
            theta = np.deg2rad(np.arange(0.0, 360.0, 1.0))
            x_ellipse = width * np.cos(theta)
            y_ellipse = height * np.sin(theta)
      
            #Rotation matrix
            rtheta = np.radians(phi)
            R = np.array([
                [np.cos(rtheta), -np.sin(rtheta)],
                [np.sin(rtheta),  np.cos(rtheta)],
                ])

            x_ellipse, y_ellipse = np.dot(R, [x_ellipse, y_ellipse])
            x_ellipse += x_center
            y_ellipse += y_center

            x_ellipse = x_ellipse%(2.0*np.pi)

            # Here we just insert a nan value in order to break the line at
            # the windows boundaries
            pos = np.where(np.abs(np.diff(x_ellipse)) >= 0.5)[0]
            x_ellipse[pos] = np.nan
            y_ellipse[pos] = np.nan

            x_ellipse[pos+1] = np.nan
            y_ellipse[pos+1] = np.nan

            if imax_y != None:
                # For centering the plots
                shift_x = imax_y
                x_ellipse = x_ellipse-(shift_x-np.pi)
                x_ellipse = x_ellipse%(2*np.pi)

            ax.plot(x_ellipse, y_ellipse, linewidth=3.5, zorder=1, color='green', linestyle='solid')


    plt.savefig(output, transparent=True, dpi=900);
    plt.cla()
    plt.clf()
    plt.close("all")
    gc.collect()

def set_title(title):
  title_out=title
  if "particles_rho*gas_rho0/gas_rho/particles_rho0" in title:
      old="particles_rho*gas_rho0/gas_rho/particles_rho0"
      new=r"$\left(\frac{\sigma_{p}}{\sigma_{gas}}\right) \left/ \left(\frac{\sigma_{p}}{\sigma_{gas}}\right)_0 \right. $"
      title_out = title.replace(old, new)
  if "gas_enthalpy" in title:
      old="gas_enthalpy"
      new=r"$\mathcal{H}(r,\theta)-\mathcal{H}_0(r)$"
      title_out = title.replace(old, new)
  if "gas_enthalpy_plus_kinetic" in title:
      old="gas_enthalpy_plus_kinetic"
      new=r"$\mathcal{H}(r,\theta)-\mathcal{H}_0(r)+\frac{v_{gas}^2}{2}$"
      title_out = title.replace(old, new)
  if "gas_tot_energy" in title:
      old="gas_tot_energy"
      new=r"$\mathcal{H}(r,\theta)-\mathcal{H}_0(r)+\frac{v_{gas}^2}{2}+\Phi_{selfgrav}(r,\theta)$"
      title_out = title.replace(old, new)
  if "selfgravity_potential" in title:
      old="selfgravity_potential"
      new=r"$\Phi_{selfgrav}(r,\theta)$"
      title_out = title.replace(old, new)
  if "gas_rossby" in title:
      old="gas_rossby"
      new=r"$Ro=\frac{\omega_{gas}(r,\theta)}{2 \Omega(r)}$"
      title_out = title.replace(old, new)
  if "gas_kinetic_energy" in title:
      old="gas_kinetic_energy"
      new=r"$\frac{v_{gas}^2}{2}$"
      title_out = title.replace(old, new)
  if "gas_rho_nmg" in title:
      old="gas_rho_nmg"
      new=r"$\rho_{gas}(r,\theta)-\rho_{gas,0}(r)$"
      title_out = title.replace(old, new)
  if "gas_rho / gas_rho0" in title:
      old="gas_rho / gas_rho0"
      new=r"$\sigma_{gas}(r,\theta) \quad / \quad \sigma_{0,gas}(r)$"
      title_out = title.replace(old, new)
  if "gas_p_nmg" in title:
      old="gas_p_nmg"
      new=r"$\p_{gas}(r,\theta)-p_{gas,0}(r)$"
      title_out = title.replace(old, new)
  if "gas_p  / gas_p0" in title:
      old="gas_p  / gas_p0"
      new=r"$\frac{p_{gas}(r,\theta)}{p_{gas,0}(r)}$"
      title_out = title.replace(old, new)
  if "gas_vort" in title:
      old="gas_vort"
      new=r"$\omega_{gas}=\left( \nabla \times \vec{v}_{gas} \right) \cdot \vec{e}_z $"
      title_out = title.replace(old, new)
  if "gas_kinetic_x" in title:
      old="gas_kinetic_x"
      new=r"$ (u_{x}-u_{x,0})^2/2 $"
      title_out = title.replace(old, new)
  if "gas_kinetic_y" in title:
      old="gas_kinetic_y"
      new=r"$ (u_{y}-u_{y,0})^2/2 $"
      title_out = title.replace(old, new)
  if "gas_div_normalised" in title:
      old="gas_div_normalised"
      new=r"$ \frac{\nabla \cdot \vec{v}}{2 \Omega(r)} $"
      title_out = title.replace(old, new)
  if "gas_strain_normalised" in title:
      old="gas_strain_normalised"
      new=r"$ \frac{strain}{2 \Omega(r)} $"
      title_out = title.replace(old, new)
  if "gas_diff_vx_x" in title:
      old="gas_diff_vx_x"
      new=r"$ \frac{\partial (u_r-u_{0,r})}{\partial r} $ for gas"
      title_out = title.replace(old, new)
  if "gas_diff_vx_y" in title:
      old="gas_diff_vx_y"
      new=r"$ \frac{1}{r} \frac{\partial (u_r-u_{0,r})}{\partial \theta}$ for gas"
      title_out = title.replace(old, new)
  if "gas_diff_vy_x" in title:
      old="gas_diff_vy_x"
      new=r"$ \frac{\partial (v_\theta-v_{\theta,0})}{\partial r}$ for gas"
      title_out = title.replace(old, new)
  if "gas_diff_vy_y" in title:
      old="gas_diff_vy_y"
      new=r"$ \frac{1}{r} \frac{\partial (v_\theta-v_{\theta,0})}{\partial \theta}$ for gas"
      title_out = title.replace(old, new)
  if "log10(particles_rho / gas_rho)" in title:
      old="log10(particles_rho / gas_rho)"
      new=r"$\log_{10}\left(\sigma_{part} / \sigma_{gas}\right)$"
      title_out = title.replace(old, new)
  if "log10(particles_rho / gas_rho0)" in title:
      old="log10(particles_rho / gas_rho0)"
      new=r"$\log_{10}\left(\sigma_{part} / \sigma_{0,gas}\right)$"
      title_out = title.replace(old, new)
  if "selfgravity_forcex" in title:
      old="selfgravity_forcex"
      new=r"$f_{selfgrav, r}$"
      title_out = title.replace(old, new)
  if "selfgravity_forcey" in title:
      old="selfgravity_forcey"
      new=r"$f_{selfgrav, \theta}$"
      title_out = title.replace(old, new)
  if "sign(gas_rossby)*log10(abs(gas_rossby)+1)" in title:
      old="sign(gas_rossby)*log10(abs(gas_rossby)+1)"
      new=r"$symlog(Ro)$"
      title_out = title.replace(old, new)
  return title_out


if __name__ == "__main__":                        
    args = standalone_args()
    plot(**vars(args))
    
    
