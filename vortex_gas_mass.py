#!/usr/bin/env python3
from mc_meshdata import meshdata, init_meshdata4sim
import os
import itertools
import numpy as np
import math
import plotall_1d_txt
import make_combined_plots
import argparse
from mc_meshdata import parse_idx_range
import mc_meshdata
import plot_2d_eval4sim

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-idx_range",          help="Snapshot index or index range idx1-idx2 (- all snapshots)")
    parser.add_argument("-phase_type",         help="Phase type : gas or particles (by default gas)", default="gas", type=str)
    parser.add_argument("-rho_min",            help="Density minimal value allowing to compute the vortex mass (by default 1.2)", default=1.2, type=float)
    parser.add_argument("-plot_rho_snapshots", help="Plot the filtered density (by default False)", default="False", type=str)
    return parser.parse_args()

def check_create_dir(dir, file_name):
    if (not os.path.isdir(dir)):
        os.system("mkdir -p " + dir)
        os.system("touch" + file_name)

def return_idx_range(idx_range=None):
    if idx_range==None:
        idx1=1
        idx2=200000
    else:
        idx1, idx2 = parse_idx_range(idx_range);
    return idx1, idx2

def read_input_variables():
    input_variables_file = open('Input_variables.data')
    input_variables_list = input_variables_file.readlines()
    input_variables_file.close()
    return input_variables_list

def find_parameter(file_list):
    particles=False
    for i in file_list:
        if 'selfref' in i:
            selfref_line=i
        if 'PARTICLES' in i:
            particles=True
    selfref=float(selfref_line.split()[1])
    return selfref, particles


def calc_mass_surf(x,y,z):
    nx, ny = np.shape(z)

    # Array with all elementary surfaces
    s = mc_meshdata.calc_vol(x, y)
    s2 = np.tile(s, (ny,1))
    s2 = np.transpose(s2)

    # Mass calculation
    local_mass = z*s2
    total_mass = local_mass.sum()

    # Surface calculation
    surf = np.where(z!=0, s2, 0)
    surf_tot = surf.sum()

    return surf_tot, total_mass

class run:

    def __init__(self):
        self.outdir = "analysis/mass_and_surf/";
        self.file = "analysis/mass_and_surf/mass_and_surf.txt"
        check_create_dir(self.outdir, self.file)
        self.input_variables_list = read_input_variables()
        self.f=open(self.file, "ab+")
        np.savetxt(self.f, np.array([('Idx', 'Surface (in AU**2)   ', '    Vortex Mass (in M_J) ')]),delimiter='   ',fmt=['%3s','%20s','%20s']) 
        self.Msun_by_MJ=1047.3486421491427

    # Filter rho with a lower density value
    # The density of the background is substracted
    def filter_rho_ndg_and_save(self, idx, phase_type, rho_min, mc, plot_rho_snapshots):
        selfref, particles = find_parameter(self.input_variables_list)
        Mvortex_by_MJ = selfref*self.Msun_by_MJ
        x_ref=1.0
        x = mc.calc("mesh_x")
        y = mc.calc("mesh_y")
        rho = mc.calc("%s_rho / %s_rho0"%(phase_type,phase_type))
        rho0 = mc.calc("%s_rho0"%(phase_type))
        rho_cut = np.where(rho >= rho_min, rho-1.0, 0)
        rho_cut = rho_cut*rho0
        surface_vortex, mass_vortex = calc_mass_surf(x,y,rho_cut)
        np.savetxt(self.f, np.array([(int(idx), x_ref**2*surface_vortex, Mvortex_by_MJ*mass_vortex)]),delimiter='   ',fmt=['%d' , '%.20f', '%.20f']) 
        if plot_rho_snapshots=="True":
            plot_output=self.outdir + 'rho_cut' + '%.3i.jpg'%(idx)
            plot_2d_eval4sim.plot_2darray(y,x,rho_cut, plot_output,title=r'Filtered density : $\sigma \geq %s\sigma_0$'%(rho_min))

    def pass1(self, idx, phase_type, rho_min, mc, plot_rho_snapshots):
        self.filter_rho_ndg_and_save(idx, phase_type, rho_min, mc, plot_rho_snapshots)

    def ending(self):
        self.f.close()

if __name__ == "__main__":
    args = standalone_args()
    idx1, idx2 = return_idx_range(args.idx_range)

    sfile = "Stationnary.h5";
    assert os.path.isfile(sfile), "Stationnary.h5 doesn't exists"
    a1 = run()
    for idx in itertools.count(idx1):
        if (idx > idx2):
            break;
        mainfile ="%.3i.data.h5"%(idx);
        if (not  os.path.isfile(mainfile)):
            break
        mc = init_meshdata4sim("./",idx)
        a1.pass1(idx, args.phase_type, args.rho_min, mc, args.plot_rho_snapshots)
    a1.ending()



