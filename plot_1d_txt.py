#!/usr/bin/env python3
import numpy as np
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("input",        help="Input txt files", nargs='+')
    parser.add_argument("output",       help="output png/jpg file")
    parser.add_argument("-xmin",        help="Minimal value of x", type=float)
    parser.add_argument("-xmax",        help="Maximal value of x", type=float)
    parser.add_argument("-ymin",        help="Minimal value of y", type=float)
    parser.add_argument("-ymax",        help="Maximal value of y", type=float)    
    parser.add_argument("-xlabel",      help="xlabel")
    parser.add_argument("-ylabel",      help="ylabel")
    parser.add_argument("-lw",          help="linewitdh")
    parser.add_argument("-marker",      help="marker (for example : '-' , '--', '*', '^', 'o', 's')")
    parser.add_argument("-cols",        help="Columns to plot", default=[0, 1], type=int, nargs=2)
    parser.add_argument("-legends",     help="Legends", type=str, nargs='*')
    parser.add_argument("-title",       help="title")
    parser.add_argument("-dpi",         help="set dpi", type=int)
    parser.add_argument("-ylogscale",   help="set logscale if True")
    return parser.parse_args()

def plot(input, output, xlabel=None, ylabel=None, xmin=None, ymin=None, xmax=None, ymax=None, lw=None, cols=[0, 1], 
legends=None, title=None, dpi=None, ylogscale=None, marker=None):
    
    if (isinstance(input, str)):
        input = [input]
    plt.clf()
    for f in input:
        data = np.loadtxt(f,comments=['I','#','*'])

        # If there are not only nans in the array the plot is performed
        if np.isnan(data).all()==False: 
            if data.ndim == 1:
                data=data.reshape(1, data.size)
            old_X=data[:,cols[0]]
            if len(cols)==2:
                old_Y=data[:,cols[1]]
                # Allows to sort X and Y (because in some files data is not written in order because of parallelism)
                X, Y = zip(*sorted(zip(old_X, old_Y)))
                plt.plot(X,Y, linewidth=lw, marker=marker)
            else:
                for i in range(1,len(cols)):
                    old_Y=data[:,cols[i]]
                    # nanmin and nanmax takes the min and max of an array ignoring the nan values
                    if np.nanmin(old_Y) <= ymin : 
                        ymin=np.nanmin(old_Y)
                    if ymax <= np.nanmax(old_Y) : 
                        ymax=np.nanmax(old_Y) + 0.5
                    # Allows to sort X and Y (because in some files data is not written in order because of parallelism)
                    X, Y = zip(*sorted(zip(old_X, old_Y)))
                    plt.plot(X,Y, linewidth=lw, marker=marker)
        
    plt.xlim([xmin,xmax])
    plt.ylim([ymin,ymax])
    plt.grid()
    if (legends):
        plt.legend(legends)
    if (xlabel):
        plt.xlabel(xlabel)
    if (ylabel):
        plt.ylabel(ylabel)
        
    if (title):
        plt.title(title)
    plt.tight_layout()
   
    if ylogscale==True:
        plt.yscale('log')

    if (dpi == None):
        plt.savefig(output, transparent=True)    
    else:
        plt.savefig(output,dpi=dpi, Transparent=True)

    plt.close('all')

if __name__ == "__main__":
    args = standalone_args()
    plot(**vars(args))
