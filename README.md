# RoSSBi Analysis

The RoSSBiAnalysis code is a set of Python3.6 parallelized functions allowing to plot and study the 2D outputs of the _RoSSbi3D_ code.

## Proper reference 

When the RoSSBiAnalysis code, or parts of it, are used for a scientific work, publication or
communications the following reference should be cited: XXX

## User's guide

Required libraries, installation indications and the user's guide can be found in the _RoSSBi analysis_ tool [wiki](wiki).  

## License

RoSSBiAnalysis is governed by the CeCILL2  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info/index.en.html" and/or LICENSE file.

## Copyright

                       --  RoSSBiAnalysis --

            Copyright ©Steven RENDON RESTREPO 2022-2023
               http://projets.lam.fr/projects/rossbi

                   steven.rendon-restrepo_AT_lam_DOT_fr
                     Groupe Systemes Planetaires (GSP)
              Laboratoire d'Astrophysique de Marseille
                         (CNRS U.M.R 7326)
             Pôle de lEtoile Site de Château-Goert
                    38, rue Frédéric Joliot-Cur
                      13388 Marseille cedex 13
                              FRANCE

## New features

1/ All tasks are parallelized
2/ All postprocessing calculations are done in Python instead of using Fortran routines 
3/ Migration to Python3 done
4/ Automatization of many tasks. The tool detects by itself if the simulation was done with 
   PARTICLES and GRAVITY
5/ Code check if plots already exist before creating them. It allows to save time
6/ New tool allowing to compute gas perturbation and particles mass
7/ An analysis0.sh script which is coupled to the ROSSBI code and allows to obtain the first
   plots on the fly

## Authors

Developed by                : Sergey RODIONOV
Mantainer                   : Steven RENDON RESTREPO
Parallelisation             : Steven RENDON RESTREPO


