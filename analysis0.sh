#!/bin/bash

range='001-001'

###############################################################################
###  Creates stationnary data and computes div, strain and curl  (Parallel) ###
###############################################################################
make_stationnaryh5.py

runall_calc_vort_div_2d_h5_f.py -stationnary=Stationnary.h5  -idx_range=${range}

####################################################################
###        Plot the batch and make combined plots (Parallel)     ###
####################################################################
plot_batch.py          -idx_range=${range} 


