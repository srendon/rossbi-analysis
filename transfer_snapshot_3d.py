#!/usr/bin/env python3
# transfer one snapshot (file_from) onto mesh of other snapshot (file_to)
# low_res.data.h5 is the file with the low resolution while high_res.data.h5 is the file with the
# high resolution. At then end high_res.data.h5 is renamed restart.data.h5

import numpy as np
import argparse
import h5py
import os
import scipy.interpolate
from scipy.interpolate import RegularGridInterpolator

def interpolation_new(xfrom, yfrom, zfrom, xto, yto, zto, data_from):
    # We create the interpolator
    I3 = RegularGridInterpolator((zfrom, yfrom, xfrom), data_from, method='linear',bounds_error=False,fill_value=None)

    # I3 only takes as arguments an array of points
    points      = np.meshgrid(zto, yto, xto, indexing="ij")
    flat_points = np.array([m.flatten() for m in points])
    flat_points = flat_points.T

    # We extrapolate the old mesh to the new mesh
    # Array of only one dimension with nz_to*ny_to*nx_to
    # elements
    data_to = I3(flat_points)
    # Reshape of the array
    data_to = data_to.reshape(*points[0].shape)

    return data_to

def transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, data_name):
    data_from        = h5from[data_name][()]          # In python3
    data_to          = interpolation_new(xfrom, yfrom, zfrom, xto, yto, zto, data_from)
    h5to[data_name][...] = data_to
   

def replace_time_vars(h5from, h5to):
    for i in ["time","dt","dt_back"]:
        h5to['vars'].attrs[i] = h5from['vars'].attrs[i]


def transfer(file_from, file_to):
    h5from = h5py.File(file_from, "r")
    h5to   = h5py.File(file_to, "r+")
    
    # Replace time variables
    replace_time_vars(h5from, h5to)

    xfrom = h5from["/mesh/x"][()]      # In python3
    yfrom = h5from["/mesh/y"][()]
    zfrom = h5from["/mesh/z"][()]

    xto   = h5to["/mesh/x"][()]        # In python3
    yto   = h5to["/mesh/y"][()]
    zto   = h5to["/mesh/z"][()]
    
    transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/p")
    transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/rho")
    transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/vx")
    transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/vy")
    transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/vz")
    try:
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/vortx")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/vorty")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/vortz")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/div")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/gas/strain")
    except:
        print ("Warning ! There is no gas vort, div and strain in *.data.h5 files");

    if ("/particles" in h5from):
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto,  "/particles/rho")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto,  "/particles/vx")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto,  "/particles/vy")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto,  "/particles/vz")
        try:
            transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/particles/vortx")
            transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/particles/vorty")
            transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/particles/vortz")
            transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/particles/div")
            transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/particles/strain")
        except:
            print ("Warning ! There is no particles vort, div and strain in *.data.h5 files");
    else:                                                      
        print ("Warning! Cannot find particles");

    if ("/selfgravity" in h5from):
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/selfgravity/forcex")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/selfgravity/forcey")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/selfgravity/forcez")
        transfer_dataset(h5from, h5to, xfrom, yfrom, zfrom, xto, yto, zto, "/selfgravity/potential")
    else:
        print ("Warning! Cannot find selfgravity");
    h5to.close()

    os.system("mv " + file_to + " restart.data.h5") 

h5from = "low_res.data.h5"
h5to   = "high_res.data.h5"

transfer(h5from, h5to)

    

    
