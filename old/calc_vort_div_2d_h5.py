import h5py
import numpy as np
import sys 
import os.path

def run_vort_div_strain(input_file, group_name, output_file, stationnary=None):
    
    # Open data.h5 file if exists
    h5_read = h5py.File(input_file,"r")
   
    # Check if group_name belongs to the hdf5 file
    # It's important mainly for particles
    if not group_name in h5_read:
      return

    # Importing mesh shape
    nx = h5_read["params"].attrs["nx"][0]
    ny = h5_read["params"].attrs["ny"][0]
    
    
    # Importing mesh
    X = h5_read["mesh/x"]
    Y = h5_read["mesh/y"]
    
    # Velocity field
    U = h5_read[group_name + "/vx"]
    V = h5_read[group_name + "/vy"]
   
    # Density
    RHO = h5_read[group_name + "/rho"]
    P   = h5_read[group_name + "/p"]

    # Copy of the velocity field since it is cumbersome
    # to modify a dataset (The file would be modified)
    x = X[:]
    y = Y[:]
    
    u = U[:]
    v = V[:]

    rho = RHO[:]
    p   = P[:]

    # Remove the velocity stationnary components
    if (stationnary):
      h5_read_stationnary = h5py.File(stationnary,"r")
      u0   = h5_read_stationnary[group_name + "/vx"]
      v0   = h5_read_stationnary[group_name + "/vy"]
      rho0 = h5_read_stationnary[group_name + "/rho"]
      p0   = h5_read_stationnary[group_name + "/p"]
    
      #rho = rho #/rho0[:]
      p_norm   = p/p0[:]
      h0  = p0/np.power(rho0,1.4)
      h0  = np.tile(h0, [np.shape(p)[0],1])

      # Index maximum pressure
      imax    = np.unravel_index(p_norm.argmax(),p_norm.shape)

      #u   = u-u0[:]
      #v   = v-v0[:]
      u   = u-u0[imax[1]]
      v   = v-v0[imax[1]]

      h5_read_stationnary.close()


    h   = p/(rho**1.4)
    
    # Transpose u and v : indeed np.shape(v)=(ny,nx)
    u    = u.transpose()
    v    = v.transpose()
    rho  = rho.transpose()
    p    = p.transpose()
    h    = h.transpose()

    # Create the vorticity, divergence and strain arrays
  ###  vort          = np.zeros((nx,ny))
  ###  div           = np.zeros((nx,ny))
  ###  strain        = np.zeros((nx,ny))
  ###  v_dot_grad_vx = np.zeros((nx,ny))
  ###  v_dot_grad_vy = np.zeros((nx,ny))
  ###  div_vx        = np.zeros((nx,ny))
  ###  div_vy        = np.zeros((nx,ny))
    hx           = np.zeros((nx,ny))
    hy           = np.zeros((nx,ny))
    
    for i in range(1,nx-1):
      for j in range(0,ny):
        jp1 = j + 1
        jm1 = j - 1
        
        # Special cases at the boundaries in j
        if j==0:
          jm1 = ny-1
        if j==ny-1:
          jp1 = 0
    
        deltay = y[jp1] - y[jm1]
    
        if j==0 or j==ny-1:
          deltay = deltay + 2*np.pi

        hx[i,j] = (h[i+1,j] - h[i-1,j])/(x[i+1]-x[i-1])
        hy[i,j] = (h[i,jp1]-h[i,jm1])/(deltay)/x[i]
  ###  
  ###      vort[i,j] = (v[i+1,j] - v[i-1,j])/(x[i+1]-x[i-1]) + v[i,j]/x[i] - (u[i,jp1]-u[i,jm1])/(deltay)/x[i]
  ###  
  ###      strain[i,j] = (v[i+1,j] - v[i-1,j])/(x[i+1]-x[i-1]) - v[i,j]/x[i] + (u[i,jp1]-u[i,jm1])/(deltay)/x[i]
  ###  
  ###      div[i,j] = (u[i+1,j] - u[i-1,j])/(x[i+1]-x[i-1]) + u[i,j]/x[i] + (v[i,jp1]-v[i,jm1])/(deltay)/x[i]

  ###      v_dot_grad_vx[i,j] = (u[i,j]*(u[i+1,j]-u[i-1,j])/(x[i+1]-x[i-1])+v[i,j]*(v[i+1,j]-v[i-1,j]))/(x[i+1]-x[i-1])
  ###      v_dot_grad_vx[i,j] = v_dot_grad_vx[i,j] - (v[i,j]+x[i]*(v[i+1,j]-v[i-1,j])/(x[i+1]-x[i-1])-(u[i,jp1]-u[i,jm1])/(deltay))*v[i,j]/x[i]
  ###  
  ###      v_dot_grad_vy[i,j] = (u[i,j]*(u[i,jp1]-u[i,jm1])/(deltay)/x[i]+v[i,j]*(v[i,jp1]-v[i,jm1])/(deltay))/x[i]
  ###      v_dot_grad_vy[i,j] = v_dot_grad_vy[i,j] + (v[i,j]+x[i]*(v[i+1,j]-v[i-1,j])/(x[i+1]-x[i-1])-(u[i,jp1]-u[i,jm1])/(deltay))*u[i,j]/x[i]

  ###      div_vx[i,j] = (u[i+1,j] - u[i-1,j])/(x[i+1]-x[i-1]) + u[i,j]/x[i] 
  ###      div_vy[i,j] = (v[i,jp1]-v[i,jm1])/(deltay)/x[i]

    
  ###  vort          = vort.transpose()
  ###  div           = div.transpose()
  ###  strain        = strain.transpose()
  ###  v_dot_grad_vx = v_dot_grad_vx.transpose()
  ###  v_dot_grad_vy = v_dot_grad_vy.transpose()
  ###  div_vx = div_vx.transpose()
  ###  div_vy = div_vy.transpose()
    u             = u.transpose()
    v             = v.transpose()
    h             = h.transpose()
    hx            = hx.transpose()
    hy            = hy.transpose()

    # Create vort_div_main_group.h5 file
    h5_write = h5py.File(output_file,"w")
 
    # Create the data sets
    for i in ['params','vars','mesh']:
      group_path = h5_read['/' + i].parent.name
      group_id = h5_write.require_group(group_path)
      h5_read.copy('/' + i, group_id)

  ###  h5_write.create_dataset(group_name + "/vort",data=vort)
  ###  h5_write.create_dataset(group_name + "/div",data=div)
  ###  h5_write.create_dataset(group_name + "/strain",data=strain)
  ###  h5_write.create_dataset(group_name + "/v_dot_grad_vx",data=v_dot_grad_vx) 
  ###  h5_write.create_dataset(group_name + "/v_dot_grad_vy",data=v_dot_grad_vy) 
  ###  h5_write.create_dataset(group_name + "/div_vx",data=div_vx) 
  ###  h5_write.create_dataset(group_name + "/div_vy",data=div_vy) 
    h5_write.create_dataset(group_name + "/u",  data=u)
    h5_write.create_dataset(group_name + "/v",  data=v)
    h5_write.create_dataset(group_name + "/h",  data=h)
    h5_write.create_dataset(group_name + "/h0", data=h0)
    h5_write.create_dataset(group_name + "/hx", data=hx)
    h5_write.create_dataset(group_name + "/hy", data=hy)
    
    
    h5_read.close()
    h5_write.close()


#if __name__ == "__main__":    
#    args = standalone_args()
#    run_vort_div_strain(**vars(args))  



