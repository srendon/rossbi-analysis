#!/usr/bin/env python3

import sys
import os
import argparse
import itertools, h5py
import numpy as np
import multiprocessing
from calc_vort_div_2d_h5 import run_vort_div_strain
from auxiliary_functions import parallel_info, print_time_execution
from auxiliary_functions import read_input_variables_file, parse_idx_range
from termcolor import colored
from functools import partial
import __main__ as main

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-input",        help="Input  (dir + posfix)", default='data.h5')
    parser.add_argument("-output_dir",   help="Output dir",            default='analysis/vort_div')
    parser.add_argument("-stationnary",  help="Stationnary.h5, if specified we remove stationnary component")
    parser.add_argument("-idx_range",    help="Snapshot index (idx), index range (idx1-idx2) or all files (None)", default=None)
    parser.add_argument("-check_exists", help="Check if vort, div and strain exists in *.data.h5 files", default=True)
    return parser.parse_args()
    

#auxilary function
def fname(input, i):
    input_dir     = os.path.dirname(input);
    input_postfix = os.path.basename(input);
    return os.path.join(input_dir, "%.3i."%i + input_postfix)    


def function_write_vort(i, onephase, input, output_dir, str_stat):

    #if (i > idx2):
    #    break;
    f = fname(input,i)
    if (not os.path.isfile(f)):
        return            
    fout_g = output_dir + "/%.3i.vort_div_gas.h5"%i
    fout_p = output_dir + "/%.3i.vort_div_particles.h5"%i
    
    exists_g = os.path.isfile(fout_g)
    exists_p = os.path.isfile(fout_p)

     
    if onephase==True:
       if exists_g==False:   #We check if the vort_div_gas.h5 file already exists
          print (f) 
          run_vort_div_strain(f,"gas",fout_g,str_stat)

    if onephase==False:
       if (exists_g and exists_p)==False: #We check if the vort_div_gas.h5 and vort_div_particles.h5 file already exist
          print (f) 
          run_vort_div_strain(f,"gas",fout_g,str_stat)
          run_vort_div_strain(f,"particles",fout_p,str_stat)


#main run function
def run(check_exists, input=None, output_dir=None, stationnary=None, idx_range=None, number_cpu=None):

    input_variables_str = read_input_variables_file(os.path.dirname(input))

    # create output directory if we need it
    os.system("mkdir -p " + output_dir)            
   
    onephase=False
    if 'ONEPHASE' in input_variables_str:
        onephase=True

    str_stat = "";
    if (stationnary):
        str_stat = stationnary
 
    input_dir = os.path.dirname(input)
    idx1, idx2 = parse_idx_range(idx_range, input_dir)

    # Parallel loop
    try:
        ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
    except KeyError:
        ncpus = multiprocessing.cpu_count()

    pool = multiprocessing.Pool(processes=ncpus)

    # Create function of one variable
    partial_write_vort=partial(function_write_vort, onephase=onephase, 
                               input=input, output_dir=output_dir, str_stat=str_stat)

    pool.map(partial_write_vort, range(idx1, idx2 + 1 , 1))

    # Parallel loop
    #ray.get([function_write_vort.remote(i, onephase, input, output_dir, str_stat) for i in range(idx1, idx2+1, 1)])

    pool.close()

def check_vort_div_strain_exist_new_version(input=None, output_dir=None, 
                                            stationnary=None, idx_range=None, 
                                            number_cpu=None, check_exists=True):

    input_dir = os.path.dirname(input)
    idx1, idx2 = parse_idx_range(idx_range, input_dir)

    f       = fname(input,idx1)    
    h5_read = h5py.File(f,"r")
    exist   = False
    if ('gas/vort' in h5_read) and ('gas/div' in h5_read) and ('gas/strain' in h5_read) and (check_exists == True):
        exist = True

    if exist == True:
        print(colored("Vorticity, divergence and strain were already computed in", 'green'))
        print(colored("the Rossby code. There is no need to compute these quantities again.", 'green'))

    h5_read.close()

    return exist

if __name__ == "__main__":    
    args = standalone_args()

    prog_name  = os.path.basename(main.__file__)
    start_time = parallel_info(prog_name)
    exist      = check_vort_div_strain_exist_new_version(**vars(args))
    if exist == False:
        run(**vars(args))
    print_time_execution(start_time, prog_name)
