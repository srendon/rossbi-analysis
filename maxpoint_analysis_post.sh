#!/bin/bash

# Each call to plot_1d_txt.py plots in a same graph all the inputs corresponding
# to the maximum (resp minimum ) of each physical quantity with respect to the
# time step

cd analysis/maxpoint1
if grep -q "ONEPHASE" ../../Input_variables.data; then
  # Allows to plot in same figure the maximums of different quantitites with respect to each time step
  plot_1d_txt.py max_gas_rho_ndg.txt max_gas_rho_nmg.txt max_gas_p_ndg.txt max_gas_p_nmg.txt plot_max_x.png -cols 0 2 -legends "gas_rho/gas_rho0" "gas_rho - gas_rho0" "gas_p / gas_p0" "gas_p - gas_p0" -xlabel=idx -ylabel=R -title="max comparison" -dpi=150
  plot_1d_txt.py max_gas_p_ndg.txt max_gas_rho_ndg.txt  plot_max_x_v2.png -cols 0 2 -legends "gas_p / gas_p0" "gas_rho/gas_rho0" -xlabel=idx -ylabel=R -title="max comparison" -dpi=150
  plot_1d_txt.py max_gas_p_ndg.txt max_gas_p_nmg.txt min_gas_vort.txt min_gas_rossby.txt plot_max_x_wrossby.png -cols 0 2 -legends "gas_p / gas_p0" "gas_p - gas_p0" "gas_vort" "gas_rossby" -xlabel=idx -ylabel=R -title="max comparison" -dpi=150
  plot_1d_txt.py min_gas_rossby.txt plot_min_gas_rossby.png -cols 0 4 -legends "min(gas_rossby)" -xlabel=idx -ylabel="-min(gas_rossby)" -title="-min(gas_rossby)" -dpi=150

else

  plot_1d_txt.py max_gas_rho_ndg.txt max_gas_rho_nmg.txt max_gas_p_ndg.txt max_gas_p_nmg.txt plot_max_x.png -cols 0 2 -legends "gas_rho/gas_rho0" "gas_rho - gas_rho0" "gas_p / gas_p0" "gas_p - gas_p0" -xlabel=idx -ylabel=R -title="max comparison" -dpi=150
  #plot_1d_txt.py max_gas_p_ndg.txt   max_part_rho_ndg.txt plot_max_x_v2.png -cols 0 2 -legends "gas_p / gas_p0" "particles_rho / gas_rho0" -xlabel=idx -ylabel=R -title="max comparison" -dpi=150
  #plot_1d_txt.py max_gas_rho_ndg.txt max_part_rho_ndg.txt plot_max_x_v3.png -cols 0 2 -legends "gas_rho/gas_rho0" "particles_rho / gas_rho0"  -xlabel=idx -ylabel=R -title="max comparison" -dpi=150
  #plot_1d_txt.py max_gas_p_ndg.txt max_gas_p_nmg.txt min_gas_vort.txt min_gas_rossby.txt plot_max_x_wrossby.png -cols 0 2 -legends "gas_p / gas_p0" "gas_p - gas_p0" "gas_vort" "gas_rossby" -xlabel=idx -ylabel=R -title="max comparison" -dpi=150
  #plot_1d_txt.py min_gas_rossby.txt plot_min_gas_rossby.png -cols 0 4 -legends "min(gas_rossby)" -xlabel=idx -ylabel="-min(gas_rossby)" -title="-min(gas_rossby)" -dpi=150

fi

