#!/usr/bin/env python3

import sys
import os
import __main__ as main
import ray
from termcolor import colored
import multiprocessing


#main run function
def info_nodes():
    ray.init(address=os.environ["ip_head"])
    print(colored('**********************************','green'))
    print(colored('Node configuration in Ray cluster:','green'))
    print(colored('**********************************','green'))
    for i in range(0,len(ray.nodes())):
        print('\n')
        print(ray.nodes()[i])
    ray.shutdown()
    print('\n')

if __name__ == "__main__":    
    info_nodes()

