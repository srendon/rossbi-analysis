#!/usr/bin/env python3
#special function to calculate cell volume
import numpy as np

def calc_vol(mesh_x, mesh_y):
    # test is it linear or logariphic
    
    ratio  = mesh_x[1] / mesh_x[0]
    ratio2 = mesh_x[2] / mesh_x[1]
    
    ny = len(mesh_y)
    #dy = 2 * np.pi / ny #old
    dy = (mesh_y[ny-1]-mesh_y[0])/ny # More general, particularly for the BOX option
    
    if (np.abs(ratio - ratio2) < 1e-14):
        #logariphical scale
        sratio   = np.sqrt(ratio)
        rin      = mesh_x[0] / sratio
        cell_vol = 0.5 * mesh_x**2 * (sratio + 1.0 / sratio ) * (sratio - 1.0 / sratio) * dy        
    else:
        # linear scale
        dx = mesh_x[1] - mesh_x[0]
        assert np.abs(dx - (mesh_x[2] - mesh_x[1]) ) < 1e-14 , "Error | mc_meshdata_calc_vol | scale error (not linear and not logariphic)"  
        cell_vol = mesh_x * dx * dy         
    return cell_vol
    
    
