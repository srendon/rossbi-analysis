#!/usr/bin/env python

import os
import itertools
import argparse

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("input",       help="Input directores to combined", nargs='+')
    parser.add_argument("out_prefix",  help="out prefix")
    parser.add_argument("tile",        help="tile option of montage")
    parser.add_argument("pdia",        help="particules diameter")
    return parser.parse_args()

def make(input, out_prefix, tile = None):
    print (input, out_prefix)
    os.system("mkdir -p " + os.path.dirname(out_prefix))
    for i in itertools.count(1):
        postfix = "%.3i.jpg"%i;
        ftest   = input[0] + postfix
        if (not os.path.isfile(ftest)):
            break;
        out = out_prefix + postfix;
        # make list of files 
        l = " ".join(["-label " + s[0:5]+ "_cm" + " "  +   s + postfix for s in input])
        print (l, out)
        if (tile):
            os.system("montage -geometry +0+0 " + "-tile " + tile + " " + l + " " + out )
        else:
            os.system("montage -geometry +0+0 " + l + " " + out)
    os.system("make_fast_movie.sh  " + out_prefix); 
                                                                                                     
                                                                                                                   
if __name__ == "__main__":
    args = standalone_args()
    make(**vars(args))
