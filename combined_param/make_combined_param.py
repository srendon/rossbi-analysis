#!/usr/bin/env python

import os
import itertools
import make_combined_pdia_plots

pdia=["2000","14000","17000","20000","32000"]
calc_type=["gas_rho_nmg","gas_rho_ndg","gas_p_nmg","gas_p_ndg","part_rho_nmp","log_part_rho_ndg","gas_strain","part_strain","gas_div","part_div","vort_diff","div_diff","gas_rossby","part_rossby","rossby_diff"]

input_string1="/analysis/movies/"
input_string2="/plot"

for j in calc_type:
  input = [s + input_string1 + j + input_string2 for s in pdia]
  out_prefix =  "combined_plots/" + j  + "/plot"; 
  make_combined_pdia_plots.make(input, out_prefix)




