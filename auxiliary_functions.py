#!/usr/bin/env python3

import os
import sys
import glob
import time
import __main__ as main
from termcolor import colored
import numpy as np
from numpy.linalg import eig, inv
import cv2

def generate_error_message(function_name, script_name):
    ErrorMessage = "Error encountered in function {function_name} from script {script_name}".format(
                function_name=function_name, script_name=script_name)
    return ErrorMessage


def read_input_variables_file(input_dir=None):
    if input_dir==None:
        input_variables_file = open('./Input_variables.data')
    else:
        input_variables_file = open(input_dir + '/Input_variables.data')
    input_variables_str = input_variables_file.read()
    input_variables_file.close()
    return input_variables_str


def read_batch_file(input_dir=None):
    if input_dir==None:
        file_np = np.loadtxt('./batch.txt', str, comments='#', usecols = (0,1,2))
    else:
        file_np = np.loadtxt(input_dir + '/batch.txt', str, comments='#', usecols = (0,1,2))

    batch_list = []

    length_batch = np.shape(file_np)[0]
    for i in range(0, length_batch, 1):
        if file_np[i][0] == 'Y':
            batch_list.append([file_np[i][1], input_dir+'/analysis/movies/'+file_np[i][2]])

    return batch_list


def read_max_profile_batch(input_dir=None):
    if input_dir==None:
        file_np = np.loadtxt('./max_profile_batch.txt', str, comments='#', usecols = (0,1,2))
    else:
        file_np = np.loadtxt(input_dir + '/max_profile_batch.txt', str, comments='#', usecols = (0,1,2))

    max_profile_batch_list = []

    length_batch = np.shape(file_np)[0]

    for i in range(0, length_batch, 1):
        if file_np[i][0] == 'Y':
            max_profile_batch_list.append((file_np[i][1], file_np[i][2]))

    return max_profile_batch_list



def read_profile_batch(input_dir=None):
    if input_dir==None:
        file_np = np.loadtxt('./batch_maxpoint.txt', str, comments='#', usecols = (0,1,2))
    else:
        file_np = np.loadtxt(input_dir + '/batch_maxpoint.txt', str, comments='#', usecols = (0,1,2))

    profile_batch = {}

    length_batch = np.shape(file_np)[0]

    for i in range(0, length_batch, 1):
        if file_np[i][0] == 'Y':
            profile_batch[file_np[i][1]] = file_np[i][2]

    return profile_batch



def find_parameter(file_list, string_to_find):
    for i in file_list.split('\n'):
        if string_to_find in i:
            string_line=i
    parameter=float(string_line.split()[-1])
    return parameter


def create_dir(*dir):
    for elt in dir:
        if (not os.path.isdir(elt)):
            os.system("mkdir -p " + elt)


def create_file(filename):
    os.system("touch {filename}".format(filename=filename))


def parse_idx_range(idx_range, input_dir='.'):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)

    # List all *data.h5 files except restart.data.h5
    list_data_files = glob.glob(input_dir + '/' + '*data.h5')

    if list_data_files == []:
        print(colored("There is no *.data.h5 files in your current folder.", 'red'))
        print(colored("Check if you are running this tool in the correct simulation folder.\n",'red'))
        sys.exit(ErrorMessage) 

    for data_file in list_data_files:
        if 'restart' in data_file:
            list_data_files.remove(data_file)
    
    # Keep only the digits
    list_data_files = [os.path.basename(k) for k in list_data_files]   # Suppress the path, keep only the file name
    list_data_files = [int(k.split(".")[0]) for k in list_data_files]  # Keep only the digit
    
    s1, s2 = min(list_data_files), max(list_data_files)

    # Check if there is a missing file between s1.data.h5 and s2.data.h5
    x = sorted(range(s1,s2+1))
    list_missing_files=[]
    for i in x:
        if i not in list_data_files:
            list_missing_files.append(i) 

    if len(list_missing_files)!=0:
        print(colored("You are missing files in your current simulation folder.", 'red'))
        print(colored("The missing files are : \n",'red'))
        for missing_file_idx in list_missing_files:
            print(colored("{idx:03d}.data.h5".format(idx=missing_file_idx),'red'))
        print(colored("\nPlease correct this issue and start again.",'red'))
        sys.exit(ErrorMessage) 

    if idx_range == None or idx_range=='':
        return s1, s2

    else:
        if ("-" not in idx_range):
            idx = int(idx_range)
            if idx in list_data_files:
                return idx, idx
            else:
                print(colored("There is no {idx:03d}.data.h5 file in current folder.".format(idx=idx),'red'))
                print(colored("Check if this file exists or if you are running this \
                               tool in the correct simulation folder.\n",'red'))
                sys.exit(ErrorMessage) 
        else:
            idx1, idx2 = idx_range.split("-")
            idx1, idx2 = int(idx1), int(idx2)
            if (s1<=idx1) and (idx2<=s2):
                return idx1, idx2
            else:
                print(colored("The provided -idx_range={idx_range} is not between the minimum ({minv:03d}) \
                               and maximum ({maxv:03d}) values \n".format(idx_range=idx_range, minv=s1, maxv=s2),'red'))
                print(colored("Please check your idx_range format or your idx_range values.\n",'red'))
                sys.exit(ErrorMessage) 


# For mounting images
def split_list(alist, n=1):
    n_split = np.array_split(alist, n)
    result = []
    for array in n_split:
        result.append(list(array))
    return result


# For mounting images
def concat_vh(list_2d): 
    return cv2.vconcat([cv2.hconcat(list_h) for list_h in list_2d])


def check_contour(batch, contour):
    batch_to_modify = batch
    if contour != None and contour != '':
        for i in range(len(batch_to_modify)):
            batch_to_modify[i][1] = batch_to_modify[i][1][:-1] + '_contour' + '/'
    for i in range(len(batch_to_modify)):
        batch_to_modify[i] = tuple(batch_to_modify[i])
    return batch_to_modify


def check_contour_combined(name_batch, batch, contour):
    batch_to_modify = batch
    name_to_modify = name_batch
    if contour != None and contour != '':
        for i in range(len(batch_to_modify)):
            batch_to_modify[i]=batch_to_modify[i]+"_contour"
        name_to_modify = name_to_modify[:-1] + "_contour/"
    return name_to_modify, batch_to_modify


def make_default_input(input_dir):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__) 

    default_input = os.listdir(input_dir)
    if default_input == []:
        print("The {folder} folder is empty. Check the folder from where you \n".format(folder=input_dir))
        print("are launching the calculation.")
        sys.exit(ErrorMessage) 

    return default_input


def parallel_info(program_name, sequential=False, color='green'):
    print(colored('*********************************************************',color))
    print(colored("Running script : {program_name}".format(program_name=program_name),color))
    print(colored('*********************************************************\n',color))
    return time.time()      


def print_time_execution(start_time, program_name, color='green'):
    end_time=time.time()
    print('\n')
    print(colored('*********************************************************',color))
    print(colored("Time execution of {program_name} : {exec_time:.6f} s".format(program_name=program_name, exec_time=end_time-start_time),color))
    print(colored('*********************************************************\n',color))
    print('\n')



