#!/usr/bin/env python3
import os
import sys
import numpy as np
import h5py
import __main__ as main
from auxiliary_functions import generate_error_message

ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)

if not os.path.exists('Stationnary.data'):
    sys.exit("Cannot find Stationnary.data" + "\n" + ErrorMessage)

if os.path.exists('Stationnary.h5'):
    os.system("rm Stationnary.h5") 

allstat = np.loadtxt("Stationnary.data");
if (allstat.shape[1] != 4 and allstat.shape[1] != 7):
    sys.exit("Wrong format in Stationnary.data" + "\n" + ErrorMessage)

h5f = h5py.File("Stationnary.h5", 'w')

# we should remove 2 guard cells in the begining and 2 in the end
# nstop 
ns = allstat.shape[0] - 2


h5f.create_dataset('/gas/vx',  data=allstat[2:ns,0])
h5f.create_dataset('/gas/vy',  data=allstat[2:ns,1])
h5f.create_dataset('/gas/p',   data=allstat[2:ns,2])
h5f.create_dataset('/gas/rho', data=allstat[2:ns,3])

if (allstat.shape[1] == 7):
    h5f.create_dataset('/particles/vx',  data=allstat[2:ns,4])
    h5f.create_dataset('/particles/vy',  data=allstat[2:ns,5])
    h5f.create_dataset('/particles/rho', data=allstat[2:ns,6])

h5f.close()

