#!/usr/bin/env python3
import numpy as np
import argparse
import plot_2d_eval4sim
import mc_meshdata
import os

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("dir",          help="simulation directory")
    parser.add_argument("str4calc",     help="String for calculation (gas_rho - gas_rho0 for example)");
    parser.add_argument("output",       help="Output prefix")
    parser.add_argument("-xlabel",      help="xlabel", default="theta (rad)")
    parser.add_argument("-ylabel",      help="ylabel", default="radius (AU)")
    parser.add_argument("-xmin",        help="Minimal value of x", type=float)
    parser.add_argument("-xmax",        help="Maximal value of x", type=float)
    parser.add_argument("-ymin",        help="Minimal value of y", type=float)
    parser.add_argument("-ymax",        help="Maximal value of y", type=float)
    parser.add_argument("-zmin",        help="Minimal value of z", type=float)
    parser.add_argument("-zmax",        help="Maximal value of z", type=float)
    parser.add_argument("-cmap",        help="Color map", default='jet');
    parser.add_argument("-nlin",        help="Size of meshgrid: nlin x nlin", default=2000,  type=int);
    
    return parser.parse_args()
    

# simple batch
def plot(dir, str4calc, output, **kwargs):        
    outdir = os.path.dirname(output)
    os.system("mkdir -p " + outdir)
    for idx, x, y, zs in mc_meshdata.eval_fullsim_it(dir, [str4calc]):
        plot_2d_eval4sim.plot_2darray(y,x,zs[0], output + "%.3i.jpg"%idx, title = str4calc + "     " + str(idx), **kwargs)
    os.system("make_fast_movie.sh  " + output);
        
if __name__ == "__main__":                        
    args = standalone_args()
    plot(**vars(args))
    
    
