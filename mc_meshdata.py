#!/usr/bin/env python3
import h5py
import numpy as np
import re
import itertools
import os
import __main__ as main
from numpy import *
from mc_meshdata_calc_vol import calc_vol
from sys import stdout
import sys
import glob
from auxiliary_functions import parse_idx_range
from auxiliary_functions import generate_error_message

# make list of hdf5 groups (remove first level!)
def list_of_groups_h5(h5):
    rez = []
    h5.visit(lambda x: rez.append(x) if ("/" in x) else None)
    return rez

class meshdata:
    
    #self.h5_main - main input file
    #self.h5_stationarry - stationnary.h5
    #self.h5_add         - list of additional h5 files (vort_div_gas.h5/vort_div_particles)
    
    # mainfile - Main input file
    # sfile    - optional stationnary.h5 file  
    # addfiles - optional additional files (vorticity for example)
    def __init__(self, mainfile, sfile=None, addfiles=None):
        
        self.h5_main = h5py.File(mainfile, "r")        
        self.nx = self.h5_main["params"].attrs["nx"][0]
        self.ny = self.h5_main["params"].attrs["ny"][0]
        
        self.list_datasets  = list_of_groups_h5(self.h5_main)
        self.list_datasets  = self.list_datasets + ["x2d","y2d", "r_0", "vars_time", "gas_rossby", \
            "particles_rossby", "cell_vol", "cell_vol2d","particles_rho*gas_rho0/gas_rho/particles_rho0", \
            "gas_kinetic", "gas_tot_energy", "gas_enthalpy", \
            "gas_enthalpy_plus_kinetic","gas_kinetic_x","gas_kinetic_y", "gas_enthalpy_plus_kinetic2", \
            "gas_vx1", "gas_vy1","gas_vy1_by_vx1","XMAX", "YMAX", "planet_x", "planet_y", "gas_div_normalised", \
            "gas_strain_normalised", "gas_diff_vx_x", "gas_diff_vx_y", "gas_diff_vy_x", "gas_diff_vy_y", "time", \
            "log10(particles_rho / gas_rho0)", "gas_div", "gas_div_vx_normalised", "gas_div_vy_normalised", \
            "gas_gradh_dot_v_normalised",  "gas_cs", "gas_scale_height", "gas_mean_free_path", "st", "st0"]
        self.h5_stationary = None
        self.list_datasets0 = []
        if (sfile):
            self.h5_stationnary = h5py.File(sfile, "r") 
            self.list_datasets0 = list_of_groups_h5(self.h5_stationnary)
        
        self.h5_add = []
        if (addfiles):
            for f in addfiles:
                h5 = h5py.File(f, "r")
                self.h5_add.append(h5)
                self.list_datasets += list_of_groups_h5(h5)
            


    # read dataset
    def read_dataset(self, dataset):
        if (dataset == "x2d"):
            x = self.h5_main["/mesh/x"][()] #For python3
            return np.tile(x,[self.ny,1]).transpose()
        if (dataset == "y2d"):
            y = self.h5_main["/mesh/y"][()] #For python3
            return np.tile(y,[self.nx,1])
        if (dataset == "planet_x"):
            return self.h5_main["planet"].attrs["x_planet"][0]
        if (dataset == "planet_y"):
            return self.h5_main["planet"].attrs["y_planet"][0]
        if (dataset == "r_0"):
            return self.h5_main["params"].attrs["r_0"][0]
        if (dataset == "vars_time"):
            return self.h5_main["vars"].attrs["time"][0]
        if (dataset == "gas_rossby"):
            return self.calc("gas_vort/(2*gas_vy0/x2d)")
        if (dataset == "gas_div"):
            return self.calc("gas_div_vx+gas_div_vy")
        if (dataset == "gas_div_normalised"):
            return self.calc("gas_div/(2*gas_vy0/x2d)")
        if (dataset == "gas_div_vx_normalised"):
            return self.calc("gas_div_vx/(2*gas_vy0/x2d)")
        if (dataset == "gas_div_vy_normalised"):
            return self.calc("gas_div_vy/(2*gas_vy0/x2d)")
        if (dataset == "gas_strain_normalised"):
            return self.calc("gas_strain/(2*gas_vy0/x2d)")
        if (dataset == "gas_gradh_dot_v_normalised"):
            return self.calc("(gas_hx*(gas_vx-gas_vx0)+gas_hy*(gas_vy-gas_vy0))/(gas_vy0/x2d)/gas_h0")
        if (dataset == "gas_diff_vx_x"):
            return self.calc("gas_div_vx-(gas_vx-gas_vx0)/x2d")
        if (dataset == "gas_diff_vy_y"):
            return self.calc("gas_div_vy")
        if (dataset == "gas_diff_vx_y"):
            return self.calc("(gas_vy-gas_vy0)/x2d+0.5*(gas_strain-gas_vort)")
        if (dataset == "gas_diff_vy_x"):
            return self.calc("0.5*(gas_vort+gas_strain)")
        if (dataset == "particles_rossby"):
            return self.calc("particles_vortz/(2*particles_vy0/x2d)")
        if (dataset == "log10(particles_rho*gas_rho0/gas_rho/particles_rho0)"):
            return self.calc("log(particles_rho*gas_rho0/(particles_rho0*gas_rho))")
        if (dataset == "log10(particles_rho / gas_rho0)"):
            return self.calc("log10(particles_rho/gas_rho0)")
        if (dataset == "gas_kinetic"):
            return self.calc("((gas_vx-gas_vx0)**2 + (gas_vy-gas_vy0)**2)/gas_vy0**2")
        if (dataset == "gas_enthalpy"):
            return self.calc("(gas_p/gas_rho)-(gas_p0/gas_rho0)")
        if (dataset == "gas_enthalpy_plus_kinetic"):
            return self.calc("gas_enthalpy+gas_kinetic")
        if (dataset == "gas_tot_energy"):
            return self.calc("gas_kinetic + gas_enthalpy + selfgravity_potential")
        if (dataset == "gas_kinetic_x"):
            return self.calc("(gas_vx-gas_vx0)**2/2.0")
        if (dataset == "gas_kinetic_y"):
            return self.calc("(gas_vy-gas_vy0)**2/2.0")
        if (dataset == "cell_vol"):
            return calc_vol(self.calc("mesh_x"), self.calc("mesh_y"));
        if (dataset == "cell_vol2d"):
            cell_vol = self.calc("cell_vol");
            return np.tile(cell_vol,[self.ny,1]).transpose()
        if (dataset == "gas_cs"):
            return self.calc("sqrt(1.4*gas_p/gas_rho)")
        if (dataset == "gas_scale_height"):
            return self.calc("sqrt(2/1.4)*gas_cs/(gas_vy0/x2d)")
        if (dataset == "gas_mean_free_path"):
            return self.calc("gas_scale_height/gas_rho")
        if (dataset == "st"):
            return self.calc("0.9213070503e-04/gas_rho")
        if (dataset == "st0"):
            return self.calc("0.9213070503e-04/gas_rho0")
        if (dataset == "gas_vx1"):
            y = self.calc("y2d-YMAX")
            y = np.where(y!=0,y,0.001)
            return self.calc("(gas_vx-gas_vx0)")/y
        if (dataset == "gas_vy1"):
            x = self.calc("x2d-XMAX")
            x = np.where(x!=0,x,0.001)
            return self.calc("(gas_vy-gas_vy0)")/x
        if (dataset == "XMAX"):
            imax = self.trace_max("gas_p   / gas_p0", "max_gas_p_ndg")
            x = self.h5_main["/mesh/x"][()] #For python3
            return x[imax[0]]
        if (dataset == "YMAX"):
            imax = self.trace_max("gas_p   / gas_p0", "max_gas_p_ndg")
            y = self.h5_main["/mesh/y"][()] #For python3
            return y[imax[1]]
        if (dataset == "gas_vy1_by_vx1"):
            np.seterr(divide='ignore')
            return self.calc("(gas_vy1/gas_vx1)")
        if (dataset in self.h5_main):
            return np.transpose(self.h5_main[dataset][()]); #For python3
        
        for h5 in self.h5_add:
            if (dataset in h5):
                return np.transpose(h5[dataset][()]);  #For python3

        return None
    
    def trace_max(self, str_val, output):
        val = self.calc(str_val)
        imax = np.unravel_index(val.argmax(),val.shape)
        return imax

    # read stationnary dataset
    def read_dataset_0(self, dataset):
        s    = self.h5_stationnary[dataset][()] #For python3
        return np.tile(s,[self.ny,1]).transpose()     

    
    def calc(self, str):
        for ds in self.list_datasets:
            # replace / by _ 
            ds_ = re.sub("/","_", ds)
            str = re.sub("(?<!\w)%s(?!\w)"%(ds_), "self.read_dataset(\"%s\")"%ds, str)

        for ds in self.list_datasets0:
            # replace / by _ 
            ds_ = re.sub("/","_", ds) + "0"
            str = re.sub("(?<!\w)%s(?!\w)"%(ds_), "self.read_dataset_0(\"%s\")"%ds, str)

        # eval allows to eval directly an expression
        return eval(str)

# -------------------------------------------------------
# Additional functions for eval (they can be defined here)
# ------------------------------------------------------- 
def AatmaxB(A, B):
    imax = np.unravel_index(B.argmax(),B.shape)
    return A[imax];

# -------------------------------------------------------
# Some auxiliary function
# -------------------------------------------------------

def init_meshdata4sim(dir, idx):
    mainfile = "%s/%.3i.data.h5"%(dir, idx);
    sfile    = "%s/Stationnary.h5"%(dir);

    addfiles = []
    h5_read = h5py.File(mainfile,"r")
    # If vort, div and strain are not in the groups we add the computed vort, div and strain
    # files to the meshdata 
    if not (('gas/vort' in h5_read) and ('gas/div' in h5_read) and ('gas/strain' in h5_read)):
        vd_gas   = "%s/analysis/vort_div/%.3i.vort_div_gas.h5"%(dir,idx);
        vd_part  = "%s/analysis/vort_div/%.3i.vort_div_particles.h5"%(dir,idx);
        if (os.path.exists(vd_gas)):
            addfiles.append(vd_gas)
        if (os.path.exists(vd_part)):
            addfiles.append(vd_part)

    h5_read.close()
    d = meshdata(mainfile, sfile, addfiles);
    return d

# use meshdata for simulations
#def eval4sim(dir, idx, str):
#    idx = int(idx)
#    d = init_meshdata4sim(dir, idx)    
#    return (d.calc("mesh_x"), d.calc("mesh_y"), d.calc(str))

# calculate list of strings for full simulation
def eval_fullsim_it(dir, strs):
    for idx in itertools.count(1):
        mainfile ="%s/%.3i.data.h5"%(dir, idx);
        if (not  os.path.exists(mainfile)):
            break
        d = init_meshdata4sim(dir, idx)        
        yield (idx, d.calc("mesh_x"), d.calc("mesh_y"), list(map(d.calc, strs)))


def idx_range_it(dir, strs, idx):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)

    mainfile ="%s/%.3i.data.h5"%(dir, idx)

    if not os.path.isfile(mainfile):
        print("There is no {file_name} file in current folder.\n".format(file_name=mainfile))
        sys.exit(ErrorMessage)

    d = init_meshdata4sim(dir, idx)

    time = h5py.File(mainfile,'r')['vars'].attrs["time"][0] 

    return  d.calc("mesh_x"), d.calc("mesh_y"), list(map(d.calc, strs)), time


        
#if __name__ == "__main__":
#    (rez,x,y) = eval4sim("/home/seger/work/workspace/planets_code/ROSSBI_project/tests/dynamic_allocation/mib_np24/", 53, "gas_rho + 1.0 / particles_rho0 + gas_vort");
#    print (rez.shape, x.shape, y.shape)
