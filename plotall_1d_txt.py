#!/usr/bin/env python3
# plot all 

import numpy as np
import plot_1d_txt
import argparse
import itertools
import os
import sys
import multiprocessing
import __main__ as main
from make_movie import make_fast_movie
from joblib import Parallel, delayed
from auxiliary_functions import parse_idx_range
from auxiliary_functions import generate_error_message
from functools import partial

def standalone_args():
    parser = argparse.ArgumentParser()    
    parser.add_argument("in_prefixes",  help="We plot all in_prefix*.txt files", nargs='+')
    parser.add_argument("out_prefix",   help="Outputs will be out_prefix*.jpg")
    parser.add_argument("-xlabel",      help="xlabel")
    parser.add_argument("-ylabel",      help="ylabel")
    parser.add_argument("-title",       help="title", default="")
    parser.add_argument("-legends",     help="Legends", type=str, nargs='*')
    return parser.parse_args()

def plot_parallel(idx, in_prefixes, out_prefix, title, xlabel, ylabel, X_min, X_max, Y_min, Y_max, legends):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)

    files = ["%s%.3i.txt"%(p,idx) for p in in_prefixes]
    if (not all(map(os.path.isfile, files))) == True:
        print("Some files in the following list are missing :\n")
        print("{files_list}".format(files_list=files))
        sys.exit(ErrorMessage)
    input = ["%s%.3i.txt"%(p,idx) for p in in_prefixes]
    out = "%s%.3i.jpg"%(out_prefix,idx)
    plot_1d_txt.plot(input, out, title=title + "    " + str(idx), xlabel=xlabel, ylabel=ylabel, xmin = X_min, xmax = X_max, ymin=Y_min, ymax=Y_max, legends=legends)        

def plot(input_dir, in_prefixes, out_prefix, idx_range, title="", xlabel="", ylabel="", legends = None):
    idx1, idx2 = parse_idx_range(idx_range, input_dir) 

    # get number of cpus available to job
    try:
        ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
    except KeyError:
        ncpus = multiprocessing.cpu_count()

    pool = multiprocessing.Pool(processes=ncpus)

    if (isinstance(in_prefixes, str)):
        in_prefixes = [in_prefixes]
    X_min = 0
    X_max = 0
    Y_min = 0
    Y_max = 0
    for i in itertools.count(idx1):       
        files = ["%s%.3i.txt"%(p,i) for p in in_prefixes]
        if (not all(map(os.path.isfile, files))):
            break
        for f in files:
            data = np.loadtxt(f)        
            X = data[:,0]
            Y = data[:,1]
            if Y.min()<=Y_min : Y_min = Y.min()
            if X.min()<=X_min : X_min = X.min()
            if Y.max()>=Y_max : Y_max = Y.max()
            if X.max()>=X_max : X_max = X.max()



    # PARALLEL
    partial_plot_parallel=partial(plot_parallel, in_prefixes=in_prefixes, 
                                                 out_prefix=out_prefix,
                                                 title=title, 
                                                 xlabel=xlabel, ylabel=ylabel,
                                                 X_min=X_min, X_max=X_max, 
                                                 Y_min=Y_min, Y_max=Y_max, 
                                                 legends=legends)

    pool.map(partial_plot_parallel, range(idx1, idx2 + 1, 1))
    make_fast_movie(input_dir, out_prefix[:-4], idx_range=idx_range)


if __name__ == "__main__":
    args = standalone_args()
    plot(**vars(args))
