### No spaces are allowed inside any column
### Only Gas ###
N      gas_vel_minus_part_vel    (gas_vx-particles_vx)**2+(gas_vy-particles_vy)**2       
N      gas_rho_nmg               gas_rho-gas_rho0                                        
N      gas_p_nmg                 gas_p-gas_p0                                            
N      gas_log_rho               log10(gas_rho/gas_rho0)                                 
N      gas_log_p                 log10(gas_p/gas_p0)                                     
Y      gas_rho_ndg               gas_rho/gas_rho0                                        
Y      gas_p_ndg                 gas_p/gas_p0                                            
N      gas_vort                  gas_vort                                                
Y      gas_div_normalised        gas_div_normalised                                      
Y      gas_rossby                gas_rossby                                              
N      gas_sound_speed           gas_cs                                                  
N      gas_scale_height          gas_scale_height                                        
N      gas_mean_free_path        gas_mean_free_path                                      
N      stokes_number             st                                                      
N      stokes_number0            st0                                                     
Y      gas_strain_normalised     gas_strain_normalised                                   
N      gas_diff_vx_x             gas_diff_vx_x                                           
N      gas_diff_vx_y             gas_diff_vx_y                                           
N      gas_diff_vy_x             gas_diff_vy_x                                           
N      gas_diff_vy_y             gas_diff_vy_y                                           
Y      gas_vy_less_gas_vy0       gas_vy-gas_vy0                                          
N      gas_vx_less_gas_vx0       gas_vx-gas_vx0                                          
N      grad_h_v_normalised       gas_gradh_dot_v_normalised                              
N      gas_kinetic               gas_kinetic                                             
N      gas_enthalpy              gas_enthalpy                                            
N      gas_H_plus_kinetic        gas_enthalpy_plus_kinetic                               
N      gas_vx                    gas_vx-gas_vx0                                          
N      gas_vy                    gas_vy-gas_vy0                                          
N      gas_div_vx                gas_div_vx_normalised                                   
N      gas_div_vy                gas_div_vy_normalised                                   
         
     
### Dust and gas ###
N      part_rho_ndg              particles_rho/particles_rho0                        
N      part_rossby               particles_rossby                                    
N      rossby_diff               gas_rossby-particles_rossby                         
N      ratio_part_rho_gas_rho    log10(particles_rho*gas_rho0/gas_rho/particles_rho0)
N      part_rho_nmp              particles_rho-particles_rho0                        
N      log_part_rho_by_gas_rho0  log10(particles_rho/gas_rho0)                       
N      log_part_rho_by_gas_rho   log10(particles_rho/gas_rho)                        
N      part_rho_by_gas_rho       particles_rho/gas_rho0                              
N      part_rossby               particles_rossby                                    
N      rossby_diff               gas_rossby-particles_rossby                         
N      ratio_part_rho_gas_rho    particles_rho*gas_rho0/gas_rho/particles_rho0       
N      part_rho_nmp              particles_rho-particles_rho0                        
N      log_part_rho_ndg          log10(particles_rho/particles_rho0)                 
N      part_vort                 particles_vort                                      
N      part_strain               particles_strain                                    
N      part_div                  particles_div                                       
N      vort_diff                 gas_vort-particles_vort                             
N      div_diff                  gas_div-particles_div                               
     
     
### Gas SG ###
N      dir_plots+"force_g_x      selfgravity_force_g_x                                    
N      dir_plots+"force_g_y      selfgravity_force_g_y                                    
N      module_SG_force_g         sqrt(selfgravity_force_g_x**2+selfgravity_force_g_y**2)  
                          
                          
### Dust SG ###
N      dir_plots+"force_p_x      selfgravity_force_p_x                                    
N      dir_plots+"force_p_y      selfgravity_force_p_y                                    
N      module_SG_force_p         sqrt(selfgravity_force_p_x**2+selfgravity_force_p_y**2)  
    


