#!/usr/bin/env python3
from mc_meshdata import meshdata, init_meshdata4sim
import os, sys
import itertools
import numpy as np
import math
import plotall_1d_txt
import argparse
import multiprocessing
import __main__ as main
from auxiliary_functions import parse_idx_range, read_input_variables_file
from auxiliary_functions import create_dir, parse_idx_range 
from auxiliary_functions import parallel_info, print_time_execution
from auxiliary_functions import generate_error_message
from auxiliary_functions import read_profile_batch, read_max_profile_batch
from functools import partial


# 1) Allows to trace the coordinates of the maximum of a given physical quantity (i.e gas_nmg) thanks to
#    "trace_max". Files are saved in maxpoint1 folder
# 2) Allows to plot profiles throug x=x_max and y=y_max thanks to run.

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-input_dir",  help="Input directory", default='.')
    parser.add_argument("-output_dir", help="Main folder where should be saved the max data", default='analysis/maxpoint1/')
    parser.add_argument("-idx_range",  help="Snapshot index (idx), index range (idx1-idx2) or all files (None)", default=None)
    return parser.parse_args()


def create_max_output(filename_out):
    if os.path.isfile(filename_out):
        os.system("rm {filename}".format(filename=filename_out))
    os.system("echo 'Idx              t                 mesh_x                  mesh_y               value' >> {filename}".format(filename=filename_out))


def create_profile_batch(input_dir, output_dir, idx):
    input_variables_str = read_input_variables_file(input_dir)

    
    profile_batch     = read_profile_batch(input_dir)
    max_profile_batch = read_max_profile_batch(input_dir)


    for p in max_profile_batch:
        filename_max = output_dir + '/' + p[1] + '.txt'
        create_max_output(filename_max)

    pairs_batch = [ 
    ("gas_vy_nmg" , "part_vy_nmp" , "gpc_vy"),
    ("gas_rho_ndg", "part_rho_ndg", "gpc_rho"),
    ("gas_vx"     , "part_vx"     , "gpc_vx"),
    ("gas_div"    ,  "part_div"   , "gpc_div"),
    ("gas_rossby" , "part_rossby" , "gpc_rossby"),
    ("gas_strain" , "part_strain" , "gpc_strain")
    ]

    return profile_batch, max_profile_batch, pairs_batch           


def trace_max_profiles(idx, input_dir, output_dir, mc, max_profile_batch):                           
    input_variables_str = read_input_variables_file(input_dir)

    for p in max_profile_batch:
        if p[1] == "max_gas_p_ndg":
            ref_imax = trace_max(idx, output_dir, mc, p[0], p[1])
        else:
            trace_max(idx, output_dir, mc, p[0], p[1])

    return ref_imax


# Allows to identify the coordinates of the point having the maximum value
def trace_max(idx, output_dir, mc, str_val, output):
    filename_out = output_dir + '/' + output + '.txt'
    val = mc.calc(str_val)
    mesh_x  = mc.calc("mesh_x")
    mesh_y  = mc.calc("mesh_y")
    t       = mc.calc("vars_time");
    imax = np.unravel_index(val.argmax(),val.shape)
    os.system("echo '{idx:03d}  {t:20.15f}  {imax_x:20.15f}  {imax_y:20.15f}  {max_val:20.15f}' >> {filename}".format(idx=idx,
               t=t, imax_x=mesh_x[imax[0]], imax_y=mesh_y[imax[1]], 
               max_val=val[imax], filename=filename_out))

    return imax

 
def make_profiles(output_dir, point_idx, idx, mc, str_val, tag):
    # point_idx are the positions of the maximum
    mesh_x    = mc.calc("mesh_x")
    mesh_y    = mc.calc("mesh_y")
    val       = mc.calc(str_val)
    x0 = mesh_x[point_idx[0]]
    y0 = mesh_y[point_idx[1]]
    # profile along x axis with fixed y = point_y    
    val_fix_y = val[:, point_idx[1]]
    name_xtxt = output_dir + "/%s_x/profile%.3i.txt"%(tag,idx)
    print(name_xtxt)
    create_dir(os.path.dirname(name_xtxt))
    np.savetxt(name_xtxt, np.transpose([mesh_x - x0, val_fix_y]))
    
    #profile along y axis with fixed x = point_x
    val_fix_x = val[point_idx[0], :]
    
    # convert to +-pi interval and sort it
    len_y_mesh = mesh_y[-1]-mesh_y[0]
    fixed_y   = mesh_y - y0
    #fixed_y   = (fixed_y + math.pi) % (math.pi * 2.0) - math.pi
    fixed_y   = (fixed_y + len_y_mesh/2.0) % (len_y_mesh) - len_y_mesh/2.0
    sorti     = np.argsort(fixed_y)
    fixed_y   = fixed_y[sorti]
    val_fix_x = val_fix_x[sorti]    
    name_ytxt = output_dir + "/%s_y/profile%.3i.txt"%(tag,idx)        
    create_dir(os.path.dirname(name_ytxt))
    np.savetxt(name_ytxt, np.transpose([fixed_y, val_fix_x]))
 

def plotall(tag, str_val, idx_range, input_dir, output_dir):
    idx1, idx2 = parse_idx_range(idx_range, input_dir)
    if (not os.path.exists("{output_dir}/{tag}_x/profile{idx:03d}.txt".format(output_dir=output_dir, 
                                                                              tag=tag, idx=idx1))):
        print ("Cannot print profiles for: " + tag);
        return
    plotall_1d_txt.plot(input_dir, "%s/%s_x/profile"%(output_dir, tag), "%s/%s_x/plot"%(output_dir, tag), 
                        idx_range, title=str_val, 
                        xlabel=r"$r - r_0$ (in AU)",            ylabel=str_val)
    plotall_1d_txt.plot(input_dir, "%s/%s_y/profile"%(output_dir, tag), "%s/%s_y/plot"%(output_dir, tag), 
                        idx_range, title=str_val, 
                        xlabel=r"$\theta - \theta_0$ (in rad)", ylabel=str_val)


def plotall_pairs(tag1, tag2, output, idx_range, input_dir, output_dir):
    idx1, idx2 = parse_idx_range(idx_range, input_dir)
    if (not os.path.exists("{output_dir}/{tag}_x/profile{idx:03d}.txt".format(output_dir=output_dir, 
                                                                              tag=tag1, idx=idx1)) 
        or not os.path.exists("{output_dir}/{tag}_x/profile{idx:03d}.txt".format(output_dir=output_dir, 
                                                                                 tag=tag2, idx=idx1))):
        print ("Cannot print pair of profiles for: " + tag1 + " " + tag2);
        return 
    
    title = "\"" + tag1 + "\"  \"" +  tag2 + "\""
    ylabel = tag1 + " " + tag2
   
    create_dir(output_dir + output + "_x");
    create_dir(output_dir + output + "_y");
   
    plotall_1d_txt.plot(input_dir, ["%s/%s_x/profile"%(output_dir, tag1), "%s/%s_x/profile"%(output_dir, tag2)],
                        "%s/%s_x/plot"%(output_dir, output),
                        idx_range, title=title, xlabel=r"$r- r_0$ (in AU)",   
                        ylabel=ylabel, legends = [tag1, tag2])

    plotall_1d_txt.plot(input_dir, ["%s/%s_y/profile"%(output_dir, tag1), "%s/%s_y/profile"%(output_dir, tag2)],
                        "%s/%s_y/plot"%(output_dir, output),        
                        idx_range, title=title, xlabel=r"$\theta - \theta_0$", 
                        ylabel=ylabel, legends = [tag1, tag2])

def initialize_maxpoint(input_dir, idx_range, output_dir):
    idx1, idx2 = parse_idx_range(idx_range, input_dir)

    sfile = "Stationnary.h5";
    assert os.path.isfile(sfile), "Stationnary.h5 doesn't exists"

    create_dir(output_dir)
    profile_batch, max_profile_batch, pairs_batch = create_profile_batch(input_dir, output_dir, idx1)
   
    return idx1, idx2, profile_batch, max_profile_batch, pairs_batch

def run(idx, profile_batch, max_profile_batch, output_dir, idx_range, input_dir):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)

    mainfile = input_dir + '/' + "%.3i.data.h5"%(idx)

    if (not  os.path.isfile(mainfile)):
        print("There is no {idx:03d}.data.h5 file in current folder.\n".format(idx=idx))
        print("Check if this file exists or if you are running this tool in the correct simulation folder.\n")
        sys.exit(ErrorMessage)

    mc = init_meshdata4sim(input_dir + "/", idx)        
    ref_imax = trace_max_profiles(idx, input_dir, output_dir, mc, max_profile_batch)    
    for tag, str_val in profile_batch.items():            
        try:
            make_profiles(output_dir, ref_imax, idx, mc, str_val, tag) 
        except NameError:
            print("fail to make profile: " + str_val)


    
def ending(profile_batch, pairs_batch, idx_range, output_dir, input_dir):
    input_variables_str = read_input_variables_file(input_dir)
    #if 'ONEPHASE' not in input_variables_str:
    #    for p in pairs_batch:
    #        plotall_pairs(p[0], p[1], p[2], idx_range, input_dir, output_dir) 

    #plotall is parallel
    for tag, str_val in profile_batch.items():            
        plotall(tag, str_val, idx_range, input_dir, output_dir)

if __name__ == "__main__":
    args = standalone_args()
    prog_name = os.path.basename(main.__file__)
    start_time = parallel_info(prog_name) 
   
    idx1, idx2, profile_batch, max_profile_batch, pairs_batch = initialize_maxpoint(**vars(args))
  
    # get number of cpus available to job
    try:
        ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
    except KeyError:
        ncpus = multiprocessing.cpu_count()

    pool = multiprocessing.Pool(processes=ncpus)


    print('***')
    print('Start : Create Maxpoint profiles txt files')
    print('***\n')
    # PARALLEL
    partial_run = partial(run, profile_batch=profile_batch, max_profile_batch=max_profile_batch, **vars(args))
    pool.map(partial_run, range(idx1, idx2 + 1, 1))
    pool.close()
    print('\n')
    print('***')
    print('End   : Create Maxpoint profiles txt files')
    print('***\n')
    
    # PARALLEL 
    print('***')
    print('Start : Plot, save and make movies of maxpoint profiles')
    print('***\n')
    ending(profile_batch, pairs_batch, **vars(args))
    print('\n')
    print('***')
    print('End   : Plot, save and make movies of maxpoint profiles')
    print('***\n')


    print_time_execution(start_time, prog_name)

