#!/usr/bin/env python3

import cv2
import numpy as np
import os
import argparse
import glob
import sys
import multiprocessing
import  argparse
import os.path
import __main__ as main
from sys import stdout
from auxiliary_functions import parse_idx_range, make_default_input
from auxiliary_functions import generate_error_message
from functools import partial

def standalone_args():
    parser = argparse.ArgumentParser(description="This tool must be run in the folder where are located the *.data.h5 files")
    parser.add_argument("-input_dir", help="Path to the folder where are the movie folders", default='analysis/movies/')
    parser.add_argument("-batch",     help="Tuple containing the paths to the folders where are located the plots.\n" + 
                                           "If no tuples are provided the code will do the movies of all folders in 'analysis/movies'", 
                                           default=None)
    parser.add_argument("-idx_range", help="Snapshot index (idx), index range (idx1-idx2) or all files (None)", default=None)
    return parser.parse_args()

def make_fast_movie(input_dir, batch, idx_range=None):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)
 
    fps = 3*4 # Frames per second
    batch_dir = batch
    output_name = batch_dir + 'movie.avi'
    idx1, idx2 = parse_idx_range(idx_range, input_dir)
    file_names, frame_array= [], []

    if os.path.exists(output_name) == True:
       stdout.write('File movie.avi already exists. This file will be \n')
       stdout.write('erased and a new one will be created. \n')
       os.remove(output_name)


    # Only files in idx_range
    for i in range(idx1, idx2 + 1 , 1):
        file_names.append(batch_dir + 'plot{:03d}.jpg'.format(i))


    # Sorting the files just in case
    #file_names.sort(key = lambda x: x[5:-4])    

    for filename in file_names:
        # Check if plot images exist
        ###if os.path.isfile(filename) == False:
        ###    print("File {filename} doesn't exist.\n".format(filename=filename))
        ###    print("Please check if you have run the plot_batch tool or if you")
        ###    print("are running this tool from the correct folder.\n")
        ###    sys.exit(ErrorMessage)           
        if os.path.isfile(filename) == False:
            print("File {filename} is missing.".format(filename=filename))
        else:
            #reading each file
            img = cv2.imread(filename)
            height, width, layers = img.shape
            size = (width, height)
    
            #inserting the frames into an image array
            frame_array.append(img)
       
    out = cv2.VideoWriter(output_name, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
    
    for i in range(len(frame_array)):
        # writing to a image array
        out.write(frame_array[i])
    out.release()
    

# Il est nécessaire que cette fonction soit globale (pas à l'intérieur
# de make_all_movies) sinon on a une erreur où multiprocessing
# ne peut pas accéder à partial_intermediate
def partial_intermediate(batch, input_dir, idx_range):
    return make_fast_movie(input_dir, batch, idx_range)


     
def make_all_movies(input_dir='./', batch=None, idx_range=None):
    # If batch is not provided the movies of all folders in analysis/movies will be done
    if batch == None or batch == '':
        batch = make_default_input(input_dir + 'analysis/movies/')
        for i in range(len(batch)):
            batch[i]=input_dir + '/' + batch[i] + 'analysis/movies/'
        batch = tuple(batch)

    # PARALLEL
    # get number of cpus available to job
    try:
        ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
    except KeyError:
        ncpus = multiprocessing.cpu_count()

    pool = multiprocessing.Pool(processes=ncpus)

    partial_make_fast_movie = partial(partial_intermediate, input_dir=input_dir, idx_range=idx_range )

    pool.map(partial_make_fast_movie, batch )
    pool.close()

 
if __name__ == "__main__":
    args = standalone_args()
    make_all_movies(**vars(args))




