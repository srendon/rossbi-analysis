#!/usr/bin/env python3
# Merge one file having only gas (for example a steady vortex) with a file having the particle phase
# The XXX.data.h5 file of the only gas phase must be renamed gas.data.h5
# The 001.data.h5 file of the gas and particles phases must be renamed gas_and_part.data.h5
# The output is the restart.data.h5 file

import h5py
import numpy as np

h5_gas      = h5py.File("gas.data.h5","r")
h5_gas_part = h5py.File("gas_and_part.data.h5","r")
h5_combined = h5py.File("restart.data.h5","w")

group_gas, group_gas_part = {},{}
attribute_gas             = {}

attribute_gas["params"]      = ["nx","ny","nz","r_0"]
attribute_gas["vars"]        = ["time","dt","dt_back"]

group_gas_part["particles"]  = ["rho","vx","vy","vz", "vortx","vorty","vortz","div","strain"]
group_gas["gas"]             = ["p","rho","vx","vy","vz","vortx","vorty","vortz","div","strain"]
group_gas["mesh"]            = ["x","y","z"]

if ("/selfgravity" in h5_gas):
    group_gas["selfgravity"] = ["forcex","forcey","forcez","potential"]
if ('div_vx' in list(h5_gas['gas'].keys())):
    group_gas["gas"].append('div_vx')
    group_gas["gas"].append('div_vy')
    group_gas_part["particles"].append('div_vx')
    group_gas_part["particles"].append('div_vy')



for i in attribute_gas.keys():
    group=h5_combined.create_group(i)
    for j in attribute_gas[i]:
        group.attrs[j]=h5_gas[i].attrs[j]

for i in group_gas.keys():
    for j in group_gas[i]:
        h5_combined.create_dataset(i+"/"+j,data=h5_gas[i][j][()])

for i in group_gas_part.keys():
    for j in group_gas_part[i]:
        h5_combined.create_dataset(i+"/"+j,data=h5_gas_part[i][j][()])

h5_gas.close()
h5_gas_part.close()
h5_combined.close()



