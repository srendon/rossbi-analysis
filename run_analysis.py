#!/bin/env python3

#// ================================================================================
#// Copyright Steven RENDON RESTREPO - 2022-2023                                  
#// e-mail:   steven.rendon-restrepo@lam.fr                                      
#// address:  Groupe Systemes planetaires (GSP)              
#//           Laboratoire d'Astrophysique de Marseille                          
#//           Pole de l'Etoile, site de Chateau-Gombert                         
#//           38, rue Frederic Joliot-Curie                                     
#//           13388 Marseille cedex 13 France                                   
#//           CNRS U.M.R 7326                                                   
#// ================================================================================
#// See the complete license in LICENSE and/or "http://www.cecill.info".        
#// ================================================================================

import os
import sys
import argparse
import plot_batch
import __main__ as main
from auxiliary_functions import *
from plot_batch import run_plot_batch

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-idx_range",  help="Snapshot index (idx), index range (idx1-idx2) \
                                             or all files (None)", default=None)

    return parser.parse_args()



if __name__ == '__main__':
    args       = standalone_args()
    cdir       = os.getcwd()
    prog_name  = os.path.basename(main.__file__)
    start_time = parallel_info(prog_name, color='green')


    ####################################################################
    ###        Plot the batch and make combined plots (Parallel)     ###
    ####################################################################
    #run_plot_batch(input_dir=cdir,       idx_range=args.idx_range, 
    #               centered_plot='True', polar='True') #-contour='False' #-contour_val=1.6


    ####################################################################
    ###                    Maxpoint analysis (Parallel)              ###
    ####################################################################
    os.system("maxpoint_analysis.py -input_dir={} -idx_range={} -output_dir={}".format(cdir,     \
                                                    args.idx_range, cdir+'/analysis/maxpoint1'))

    ### Creates combined plots with the profile graphs. Plots are saved in folder maxpoint1/combinedXX
    #os.system("make_combined_profiles.py")
    
    ### Plots in a same figure the value the maximum of many physical quantities with respect to time 
    #os.system("maxpoint_analysis_post.sh")
    
    #os.system("maxpoint_analysis1_post2.sh")
    
    # Supplementary analysis when particles option is switched on
    #  run_add_analysis_set1.sh
    
    ####################################################################
    ### Computes the aspect ratio of the vortex and creates the plot ###
    ####################################################################
    #aspect_dir    = cdir + "/analysis/aspect/"
    #distances_dir = cdir + "/analysis/distances/"
    #os.system("aspect_ratio_analysis.py -input_dir={} -output_dir={} -idx_range={} \
    #                                             -polar='True'".format(cdir, aspect_dir, args.idx_range))
    #os.system("distance_star_analysis.py -input_dir={} -output_dir={} \ 
    #                                                   -idx_range={}".format(cdir, distances_dir, args.idx_range))

    ####################################################################
    ###  Computes the distance between the maximum of gas pressure   ###
    ###          and  the maximum of particles density               ###
    ####################################################################
    #dist_vort_part_dir = cdir + "/analysis/distances/gas_particle/"
    #os.system("distance_gas_vortex_particles.py -input_dir={} -output_dir={}".format(cdir, dist_vort_part_dir))
    
    ###################################################################################
    ### Computes the vortex and particles mass in regions of highest concentration  ###
    ###################################################################################
    # This module does the density cut in the whole mesh i.e it is not suited for the study
    # of mass evolution for St >> 1 because particles are also trapped in coorbitale regions.
    # We should add an option for focusing in a particular region of space
    #mass_rossby_analysis.py -input_dir="${cdir}" -output_dir="${cdir}/analysis/mass_and_surf" -idx_range=${range} -percentage_cut_g=1.25
    #-percentage_cut_rho_spir=1.25 -percentage_cut_p=200 -percentage_rossby_cut=-0.08 -rho_snapshots="True" 
    #-toomre_snapshots="False"  -hill="False" -rossby_snapshots="True" #-polar='True'

    print_time_execution(start_time, prog_name, color='green')





