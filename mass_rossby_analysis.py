#!/usr/bin/env python3
from mc_meshdata import meshdata, init_meshdata4sim
import os, itertools, math, argparse
import numpy as np
import matplotlib.pyplot as plt
import mc_meshdata
import plotall_1d_txt
import plot_1d_txt
import multiprocessing
import __main__ as main
from mc_meshdata import parse_idx_range
from plot_2d_eval4sim import plot_2darray
from auxiliary_functions import parse_idx_range, read_input_variables_file, find_parameter
from auxiliary_functions import create_dir, parallel_info, print_time_execution
from functools import partial

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-input_dir",              help="Input directory", default='.')
    parser.add_argument("-output_dir",             help="Output_dir", default = "./analysis/mass_and_surf")
    parser.add_argument("-idx_range",              help="Snapshot index (idx), index range (idx1-idx2) or all files (None)", default=None)
    parser.add_argument("-percentage_cut_g",       help="Density minimal value (for gas) allowing to compute the vortex mass (by default 1.6)", default=1.6, type=float)
    parser.add_argument("-percentage_cut_rho_spir",help="Density minimal value (for gas) allowing to compute the vortex mass (by default 1.4)", default=1.3, type=float)
    parser.add_argument("-percentage_cut_p",       help="Density minimal value (for particles) allowing to compute the vortex mass (by default 1.2)", default=100, type=float)
    parser.add_argument("-percentage_rossby_cut",  help="Percentage of the Rossby minimum at which we do the cut off", default=-0.08, type=float)
    parser.add_argument("-percentage_toomre_cut",  help="Percentage of the Toomre minimum at which we do the cut off", default=0.6, type=float)
    parser.add_argument("-rho_snapshots",          help="Plot the filtered density", default=None)
    parser.add_argument("-toomre_snapshots",       help="Plot the Toomre Parameter", default=None)
    parser.add_argument("-hill",                   help="Plot the Hill radius in the filtered density and in Toomre parameter plots", default=None)
    parser.add_argument("-rossby_snapshots",       help="Plot the filtered rossby", default=None)
    parser.add_argument("-polar",                  help="Perform a plot in polar coordinates", default=False,  type=bool)
    return parser.parse_args()


def calc_mass_surf(x,y,z):
    nx, ny = np.shape(z)

    # Array with all elementary surfaces
    s  = mc_meshdata.calc_vol(x, y)
    s2 = np.tile(s, (ny,1))
    s2 = np.transpose(s2)

    # Mass calculation
    local_mass = z*s2
    total_mass = local_mass.sum()

    # Surface calculation
    surf     = np.where(z!=0, s2, 0)
    surf_tot = surf.sum()

    return surf_tot, total_mass

def trace_max(mc, str_val, output):
    val = mc.calc(str_val)
    imax = np.unravel_index(val.argmax(),val.shape)
    return imax


# Filter rho with a lower density value
# The density of the background is substracted
def save_mass_surface(idx, file_mass, phase_type, percentage_cut, percentage_cut_rho_spir, mc, x_ref, M_by_MJ, time, resolution_r, imax_y):
    x                = mc.calc("mesh_x")
    y                = mc.calc("mesh_y")
    X, Y             = np.meshgrid(y,x) #X=theta, Y=r
    

    rho              = mc.calc("%s_rho / %s_rho0"%(phase_type,phase_type))
    rho_min          = percentage_cut 
    rho0             = mc.calc("%s_rho0"%(phase_type))
    #rho_cut = np.where(rho >= rho_min, rho-1, 0)    # Should we keep the background density or not ?
    # In order to make the cut two conditions must be fulfilled:
    # 1) The density should be higher than rho_min
    # 2) The maximum angular separation with the vortex maximum must be lower than 45°=0.78 rad.
    #    This permits to not cat secondary vortices when they are present in the simulation
    #rho_cut          = np.where(np.mod(np.abs(X-imax_y),2*np.pi)<=0.785398, rho, 0)
    rho_cut          = np.where(np.abs((np.remainder(X-imax_y+np.pi,2*np.pi)-np.pi))<=0.785398, rho, 0)
    rho_cut          = np.where(rho >= rho_min, rho_cut, 0)
    rho_cut          = rho_cut*rho0
    surface, mass    = calc_mass_surf(x, y, rho_cut)

    rho_uncertainty  = np.where(rho >= rho_min-percentage_cut*2*resolution_r/Y, rho, 0)             # 2>|b_sigma|
    rho_uncertainty  = np.where(rho <= rho_min+percentage_cut*2*resolution_r/Y, rho_uncertainty, 0)
    rho_uncertainty  = rho_uncertainty*rho0 
    surface_uncertainty, mass_uncertainty    = calc_mass_surf(x, y, rho_uncertainty)

    #rho_uncertainty  = rho_uncertainty/rho0

    os.system("echo '{time:.2f}   {surface_val:20}   {mass_val:20}     {mass_un:20}' >> {file_path}".format(time=time, surface_val=x_ref**2*surface, mass_val=M_by_MJ*mass, mass_un=M_by_MJ*mass_uncertainty, file_path=file_mass))

    rho_cut=rho_cut/rho0

    #return x, y, mass, rho_uncertainty 
    return x, y, mass, rho_cut, rho



# Filter rossby with a lower rossby value
def save_rossby_surface(idx, file_rossby, phase_type, percentage_rossby_cut, mc, x_ref, time, imax_y):
    x = mc.calc("mesh_x")
    y = mc.calc("mesh_y")

    X, Y                     = np.meshgrid(y,x) #X=theta, Y=r

    rho_min                  = 2.2

    rossby                   = mc.calc("%s_rossby"%(phase_type))
    rho                      = mc.calc("%s_rho / %s_rho0"%(phase_type,phase_type))

    rossby_cut               = np.where(np.abs((np.remainder(X-imax_y+np.pi,2*np.pi)-np.pi))<=0.785398, rossby, 0)
    rossby_cut               = np.where(rho >= rho_min, rossby_cut, 0)
    #rossby_cut               = np.where(rossby_cut <= 0, rossby_cut, 0)    
    #rossby_cut               = np.where(rossby_cut <= percentage_rossby_cut, rossby_cut, 0)    
    surface, integral_rossby = calc_mass_surf(x, y, rossby_cut)
    mean_rossby              = integral_rossby/surface

    os.system("echo '{time:.2f}   {surface_val:20}  {rossby_val:20}' >> {file_path}".format(time=time, surface_val=x_ref**2*surface, rossby_val=mean_rossby, file_path=file_rossby))

    return x, y, rossby_cut

# Filter Toomre with a lower Toomre value
def save_toomre_surface(idx, file_toomre, phase_type, percentage_toomre_cut, mc, x_ref, selfref, percentage_sg, time):
    x = mc.calc("mesh_x")
    y = mc.calc("mesh_y")

    c_s     = mc.calc("sqrt(1.4*gas_p/gas_rho)")
    c_s0    = mc.calc("sqrt(1.4*gas_p0/gas_rho0)")

    toomre  = (1+2*mc.calc("gas_rossby"))*(c_s /mc.calc("gas_vy") )*(1/selfref)*(1/(np.pi*mc.calc("x2d**2*gas_rho")))
    toomre0 = (c_s0/mc.calc("gas_vy0"))*(1/selfref)*(1/(np.pi*mc.calc("x2d**2*gas_rho0")))

    diff_toomre = toomre-toomre0

    min_toomre_value         = np.amin(diff_toomre)
    toomre_cut               = np.where(diff_toomre <= min_toomre_value*percentage_toomre_cut, toomre, 0)    
    toomre_cut               = toomre_cut/percentage_sg 
    surface, integral_toomre = calc_mass_surf(x, y, toomre_cut)
    mean_toomre              = integral_toomre/surface

    os.system("echo '{time:.2f}   {surface_val:20}  {toomre_val:20}' >> {file_path}".format(time=time, surface_val=x_ref**2*surface, toomre_val=mean_toomre, file_path=file_toomre))

    return x, y, toomre_cut 

def save_hill(idx, file_hill, mc, x, y, mass, x_ref, selfref, time):
    imax = trace_max(mc, "gas_p   / gas_p0", "max_gas_p_ndg")
    hill_posx   = x[imax[0]]
    hill_posy   = y[imax[1]]
    hill_radius =  hill_posx*(mass*selfref/3.0)**(1/3.0)

    os.system("echo '{time:.2f}   {value:20}' >> {file_path}".format(time=time, value=hill_radius*x_ref, file_path=file_hill))


def plot_snapshot_and_save(idx, val_dir, percentage_cut, val, x, y, imax, time, polar, type_plot, spiral=None):
    plot_output=val_dir + '/%.3i.jpg'%(idx)

    if type_plot=='density' and (spiral==False or spiral==None):
        title=r'Filtered density : $\sigma / \sigma_0 \geq {percentage_cut}$        {time:.2f} T$_0$ '.format(percentage_cut=percentage_cut, time=time)
    elif type_plot=='density':
        title=r'Filtered density : $\sigma / \sigma_0 \leq {percentage_cut}$        {time:.2f} T$_0$ '.format(percentage_cut=percentage_cut, time=time)
    elif type_plot=='rossby':
        title=r'Filtered rossby : Ro $\leq {percentage_cut}$                        {time:.2f} T$_0$ '.format(percentage_cut=percentage_cut, time=time)
    elif type_plot=='toomre':
        title=r'Filtered Toomre parameter : Q $\geq$ Q$_{{0}}$ + {percentage_cut} Q$_{{min}}$      {time:.2f} T$_0$ '.format(percentage_cut=percentage_cut, time=time)

    if (spiral==False or spiral==None) and type_plot!='rossby':
        plot_2darray(y, x, val, plot_output, xlabel=r'$\theta$ (in rad)', ylabel= 'radius (in AU)',
                     title=title, imax_y=imax, 
                     polar=polar, threshold=['lower',percentage_cut] )
    elif type_plot=='rossby':
        plot_2darray(y, x, val, plot_output, xlabel=r'$\theta$ (in rad)', ylabel= 'radius (in AU)',
                     title=title, imax_y=imax, ymin=None, ymax=None, 
                     polar=polar, threshold=None)
    else:
        plot_2darray(y, x, val, plot_output, xlabel=r'$\theta$ (in rad)', ylabel= 'radius (in AU)',
                     title=title, imax_y=imax, ymin=None, ymax=None, 
                     polar=polar, threshold=['greater',percentage_cut])



def default_directories_and_files(output_dir, phase_type):
    # Directories
    outdir           = output_dir + "/%s"%(phase_type);
    toomre_dir       = outdir + '/toomre'
    density_dir      = outdir + '/density'
    density_spir_dir = outdir + '/density_spir'
    rossby_dir       = outdir + '/rossby'

    # Files
    file_mass   = outdir + "/mass.txt"
    file_hill   = outdir + "/hill.txt"
    file_rossby = outdir + "/rossby.txt"
    file_toomre = outdir + "/toomre.txt"
  
    directories = outdir, toomre_dir, density_dir, density_spir_dir, rossby_dir
    files       = file_mass, file_hill, file_rossby, file_toomre

    return directories, files


def create_files_mass_analysis(filename):
    for elt in list(filename):
        os.system("rm -f %s"%elt)
        if (not os.path.isdir(elt)):
            os.system("touch " + elt)
            if elt[-8:]=='mass.txt':
                os.system("echo 'Idx   Surface (in AU**2)              Mass (in M_J)     Mass uncertainty (in M_J)' >> %s"%elt)
            if elt[-8:]=='hill.txt':
                os.system("echo 'Idx   Hill radius (in AU)' >> %s"%elt)
            if elt[-8:]=='ssby.txt':
                os.system("echo 'Idx   Surface (in AU**2)          Minimum Rossby   ' >> %s"%elt)
            if elt[-8:]=='omre.txt':
                os.system("echo 'Idx   Surface (in AU**2)          Minimum Toomre   ' >> %s"%elt)

def function_pass(idx, directories, files_path, selfref, percentage_sg, phase_type, period_r0, resolution_r, 
                  percentage_cut_g, percentage_cut_rho_spir, percentage_cut_p, percentage_rossby_cut, percentage_toomre_cut, 
                  rho_snapshots, toomre_snapshots, hill, rossby_snapshots, polar, 
                  idx_range=None, input_dir=None, output_dir=None):

    outdir, toomre_dir, density_dir, density_spir_dir, rossby_dir    = directories 
    file_mass, file_hill, file_rossby, file_toomre                   = files_path

    Msun_by_MJ   = 1047.3486421491427
    M_by_MJ      = selfref*Msun_by_MJ
    x_ref        = 1.0

    if phase_type=='gas':
        percentage_cut = percentage_cut_g
    else:
        percentage_cut = percentage_cut_p

    mainfile              = input_dir + "/%.3i.data.h5"%(idx);
    mc                    = init_meshdata4sim(input_dir + "/",idx)


    # Finding pressure maximum
    val     = mc.calc("gas_p  / gas_p0")
    x       = mc.calc("mesh_x")
    y       = mc.calc("mesh_y")
    imax    = np.unravel_index(val.argmax(),val.shape)
    imax_x  = x[imax[0]]
    imax_y  = y[imax[1]]
    time    = float(mc.calc("vars_time"))
    time    = time/2/np.pi/period_r0

    x0, y0, mass, rho_cut, rho = save_mass_surface(idx, file_mass, phase_type, percentage_cut, percentage_cut_rho_spir, 
                                          mc, x_ref, M_by_MJ, time, resolution_r, imax_y)

    print(mainfile, 'hill')
    if hill != None and hill != '':
    #    print(mainfile, 'hill')
        save_hill(idx, file_hill, mc, x0, y0, mass, x_ref, selfref, time)
    if rho_snapshots != None and rho_snapshots != '':
    #    print(mainfile, 'rho')
        plot_snapshot_and_save(idx, density_dir, percentage_cut, rho_cut, 
                               x0, y0, imax_y, time, polar, 'density', spiral=False)
        #plot_snapshot_and_save(idx, density_dir, 1.2, rho, x0, y0, imax_y, time, polar, 'density', spiral=False)
        plot_snapshot_and_save(idx, density_spir_dir,percentage_cut_rho_spir, rho, 
                               x0, y0, imax_y, time, polar, 'density', spiral=True)
    if rossby_snapshots != None and rossby_snapshots != '':
    #    print(mainfile, 'rossby')
        x1, y1, rossby_cut = save_rossby_surface(idx, file_rossby, phase_type, 
                                                 percentage_rossby_cut, mc, x_ref, time, imax_y)
        plot_snapshot_and_save(idx, rossby_dir, percentage_rossby_cut, rossby_cut,
                               x1, y1, imax_y, time, polar, 'rossby')
    if toomre_snapshots != None and toomre_snapshots != '' and phase_type=="gas":
    #    print(mainfile, 'toomre')
        x1, y1, toomre_cut = save_toomre_surface(idx, file_toomre, phase_type, 
                                                 percentage_toomre_cut, mc, x_ref, selfref, percentage_sg, time)
        plot_snapshot_and_save(idx, toomre_dir, percentage_toomre_cut, toomre_cut, 
                               x1, y1, imax_y, time, polar, 'toomre')


def mass_rossby_initialization(phase_type, input_variables_str, idx_range, 
                               percentage_cut_g, percentage_cut_rho_spir, percentage_cut_p, 
                               percentage_rossby_cut, percentage_toomre_cut, 
                               rho_snapshots=False, toomre_snapshots=False, hill=False, 
                               rossby_snapshots= False, polar=None,
                               input_dir=None, output_dir=None):

    idx1, idx2              = parse_idx_range(idx_range, input_dir)
    directories, files_path = default_directories_and_files(output_dir, phase_type)
    create_dir(*directories)
    create_files_mass_analysis(files_path)
   
    selfref              = find_parameter(input_variables_str, "selfref")
    resolution_r         = find_parameter(input_variables_str, "x resolution (in AU)") 

    if phase_type == 'gas':
        try:
            percentage_sg = find_parameter(input_variables_str, "percentage_self_gravity_gas")/100.0
        except:
            percentage_sg = 0.0
    else:
        try:
            percentage_sg        = find_parameter(input_variables_str, "percentage_self_gravity_part")/100.0
        except:
            percentage_sg = 0.0

    if percentage_sg == 0.0:
        percentage_sg = 1.000

    return idx1, idx2, directories, files_path, selfref, percentage_sg, resolution_r 


def ending(directories, files_path, percentage_cut_g=1.6, 
           percentage_cut_rho_spir=1.3, 
           percentage_cut_p=1.2, percentage_rossby_cut=-0.08, 
           percentage_toomre_cut=0.6, rho_snapshots=True, 
           toomre_snapshots=None, hill=None, rossby_snapshots=None, 
           idx_range=None, input_dir=None, output_dir=None, polar=None):
    outdir = directories[0]
    file_mass, file_hill, file_rossby, file_toomre = files_path 
    plot_1d_txt.plot(file_mass,   outdir + '/surf_evolution.jpg',        xlabel=r'Time (in T$_0$)', ylabel=r'surface (in AU$^2$)', title='Surface evolution', cols=[0,1])
    plot_1d_txt.plot(file_mass,   outdir + '/mass_evolution.jpg',        xlabel='time (in T$_0$)',  ylabel=r'mass (in $M_J$)',     title='Mass evolution', cols=[0,2])
    plot_1d_txt.plot(file_rossby, outdir + '/mean_rossby_evolution.jpg', xlabel='Time ( in T$_0$)', ylabel=r'Ro',                  title='Mean Rossby number evolution at the vortex center', cols=[0,2])
    plot_1d_txt.plot(file_toomre, outdir + '/mean_toomre_evolution.jpg', xlabel='Time ( in T$_0$)', ylabel=r'Q',                   title='Mean Toomre number evolution at the vortex center', cols=[0,2])
    if hill != None and hill != '':
        plot_1d_txt.plot(file_hill, outdir + '/hill_evolution.jpg', xlabel='T/T$_0$', ylabel=r'Hill radius (in AU)', title='Hill radius evolution')




if __name__ == "__main__":
    args                 = standalone_args()
    prog_name            = os.path.basename(main.__file__)
    start_time           = parallel_info(prog_name)    

    input_variables_str  = read_input_variables_file(args.input_dir)
    period_r0            = find_parameter(input_variables_str, "period_r0 (years)")


    # Parallel loop
    # get number of cpus available to job
    try:
        ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
    except KeyError:
        ncpus = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes=ncpus)

    #list_phase_type=['gas']
    #list_phase_type=['gas']
    #if 'ONEPHASE' not in input_variables_str:
    #    list_phase_type.append('particles')

    list_phase_type=['particles']

    for phase_type in list_phase_type:
        print('***')
        print('Start : Create folders and files paths for {phase_type}'.format(phase_type=phase_type))
        print('***\n')
        idx1, idx2, directories, files_path, selfref, percentage_sg, resolution_r = mass_rossby_initialization(phase_type, input_variables_str, **vars(args))
        print('\n')
        print('***')
        print('End   : Create folders and files paths for {phase_type}'.format(phase_type=phase_type))
        print('***\n')

        # Parallel loop
        print('***')
        print('Start : Compute and save plots for {phase_type}'.format(phase_type=phase_type))
        print('***\n')
        partial_function_pass = partial(function_pass, 
                                        directories=directories, 
                                        files_path=files_path, 
                                        selfref=selfref, 
                                        percentage_sg=percentage_sg, 
                                        phase_type=phase_type, 
                                        period_r0=period_r0, 
                                        resolution_r=resolution_r, 
                                        **vars(args))
        pool.map(partial_function_pass, list(range(idx1, idx2+1, 1)))
        pool.close()
        print('\n')
        print('***')
        print('End   : Compute and save plots for {phase_type}'.format(phase_type=phase_type))
        print('***\n')

        # Plot the mass, toomre with respect to time
        print('***')
        print('Start : Plot the mass, toomre and rossby at the vortex core with respect to time for {phase_type}'.format(phase_type=phase_type))
        print('***\n')
        ending(directories, files_path, **vars(args))
        print('\n')
        print('***')
        print('End   : Plot the mass, toomre and rossby at the vortex core with respect to time for {phase_type}'.format(phase_type=phase_type))
        print('***\n')


    print_time_execution(start_time, prog_name)


