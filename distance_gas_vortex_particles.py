#!/usr/bin/env python3

import numpy as np
import argparse
import os
from plot_1d_txt import *
import sys
import __main__ as main
from auxiliary_functions import generate_error_message, read_input_variables_file
from auxiliary_functions import parallel_info, print_time_execution

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-input_dir",  help="Input directory", default='.')
    parser.add_argument("-output_dir", help="output_dir", default = "./analysis/distances/gas_particle/")
    return parser.parse_args()

def check_create_dir(dir):
    if (not os.path.isdir(dir)):
        os.system("mkdir -p " + dir)

def calc(input_dir, output_dir):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)

    # Load max gas pressure
    if os.path.exists(input_dir + "/analysis/maxpoint1/max_gas_p_ndg.txt"):
        max_gas_p_ndg       = np.loadtxt(input_dir + "/analysis/maxpoint1/max_gas_p_ndg.txt", skiprows=1)
    else:
        print("Fail to load gas_p/gas_p0 maxpoint.\n")
        print("Check if file max_gas_p_ndg.txt was created\n")
        sys.exit(ErrorMessage)

    # Load min gas rossby
    if os.path.exists(input_dir + "/analysis/maxpoint1/min_gas_rossby.txt"):
        min_gas_rossby      = np.loadtxt(input_dir + "/analysis/maxpoint1/min_gas_rossby.txt", skiprows=1)
    else:
        print("Fail to load gas_rossby minpoint.\n")
        print("Check if file min_gas_rossby.txt was created\n")
        sys.exit(ErrorMessage)

    # Load max particles density
    if os.path.exists(input_dir + "/analysis/maxpoint1/max_part_rho_ndg.txt"):
        max_part_rho_ndg = np.loadtxt(input_dir + "/analysis/maxpoint1/max_part_rho_ndg.txt", skiprows=1)
    else:
        print("Fail to load particles_rho/gas_rho0 maxpoint.\n")
        print("Check if you have run simulations with particles")
        print("and that file max_part_rho_ndg.txt was created\n")
        sys.exit(ErrorMessage)

    
    check_create_dir(output_dir)
    name_txt_p           = output_dir + "max_p_gas_and_max_part_rho.txt"
    name_txt_rossby      = output_dir + "max_gas_rossby_and_max_part_rho.txt"

    
    file_to_write_p      = open(name_txt_p     ,'w')
    file_to_write_rossby = open(name_txt_rossby,'w')

    nidx = len(max_gas_p_ndg);
    for idx in range(nidx):
        delta_x = np.abs(max_gas_p_ndg[idx,2]-max_part_rho_ndg[idx,2])
        delta_y = np.abs(max_gas_p_ndg[idx,3]-max_part_rho_ndg[idx,3])
        file_to_write_p.write("%d %.18f %.18f\n"%(idx + 1, delta_x, delta_y))    

        delta_x = np.abs(min_gas_rossby[idx,2]-max_part_rho_ndg[idx,2])
        delta_y = np.abs(min_gas_rossby[idx,3]-max_part_rho_ndg[idx,3])
        file_to_write_rossby.write("%d %.18f %.18f\n"%(idx + 1, delta_x, delta_y))    

    file_to_write_p.close()
    file_to_write_rossby.close()

    return name_txt_p, name_txt_rossby

if __name__ == "__main__":
    args                          = standalone_args()
    prog_name                     = os.path.basename(main.__file__)
    start_time                    = parallel_info(prog_name, sequential=True)

    input_variables_str           = read_input_variables_file(args.input_dir)

    if 'ONEPHASE' not in input_variables_str:
        name_file_p, name_file_rossby = calc(**vars(args))

        # Distance of particles max pressure to gas max density
        plot(input=name_file_p, output="analysis/distances/gas_particle/distance_x_p.png",      cols=[0,1],ylogscale=True,ymin=1e-03)
        plot(input=name_file_p, output="analysis/distances/gas_particle/distance_y_p.png",      cols=[0,2],ylogscale=True,ymin=1e-03)

        # Distance of particles max density to gas min vorticity
        plot(input=name_file_rossby, output="analysis/distances/gas_particle/distance_x_rossby.png", cols=[0,1],ylogscale=True,ymin=1e-03)
        plot(input=name_file_rossby, output="analysis/distances/gas_particle/distance_y_rossby.png", cols=[0,2],ylogscale=True,ymin=1e-03)
    else:
        print('If you run an only gas simulation please ignore following message. Otherwise please check your simulation folder.')
        print('\n')
        print('This simulation was run without particles')
        print('Thus it is not possible to evaluate the distances between the extremums of')
        print('gas physical variables (p and rossby) and the the particles density maximum')

    print_time_execution(start_time, prog_name)


