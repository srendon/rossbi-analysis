#!/usr/bin/env python3
import argparse
import plotall_1d_txt
import maxpoint_analysis1_post2
import itertools
import os

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("sims",        help="Simulations", nargs='+')
    parser.add_argument("-titles",     help="Short Titles for simulations", nargs='+')    
    return parser.parse_args()

def plot(sims, titles, vals, output, xlabel, ylabel = None):    
    input = []
    legends = []
    for i in range(len(sims)):
        for v in vals:
            input.append(sims[i] + "/analysis/maxpoint1/" + v + "/profile");
            legends.append(titles[i] + " # " + v)
    print (input)
    plotall_1d_txt.plot(input, output, legends=legends, xlabel = xlabel, ylabel = ylabel)

def plot_xy(sims, titles, vals, output):
    vals_x   = [s + "_x" for s in vals]
    output_x = output + "_x/profile"
    vals_y   = [s + "_y" for s in vals]
    output_y = output + "_y/profile"
    os.system("mkdir -p " + output + "_x")
    os.system("mkdir -p " + output + "_y")
    plot(sims, titles, vals_x, output_x, xlabel = "R - R0")
    plot(sims, titles, vals_y, output_y, xlabel = "phi - phi0")

    
    
def plot_all(sims, titles):
    plot_xy(sims, titles, ["gas_rho_ndg", "part_rho_ndg"], "gpc_rho")
    plot_xy(sims, titles, ["gas_rho_ndg"], "gas_rho_ndg")
    plot_xy(sims, titles, ["part_rho_ndg"], "part_rho_ndg")
    plot_xy(sims, titles, ["gas_p_ndg"],  "gas_p_ndg")
    plot_xy(sims, titles, ["log_part_rho_ndg"], "log_part_rho_ndg")
    plot_xy(sims, titles, ["gas_vort", "part_vort"], "gpc_vort")
    plot_xy(sims, titles, ["gas_div", "part_div"], "gpc_div")
    plot_xy(sims, titles, ["gas_vx", "part_vx"], "gpc_vx")
    plot_xy(sims, titles, ["gas_vy_nmg", "part_vy_nmp"], "gpc_vy")
    
    maxpoint_analysis1_post2.plot(".");
    
if __name__ == "__main__":
    args = standalone_args()
    plot_all(**vars(args))
        
