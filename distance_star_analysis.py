#!/usr/bin/env python3
# we take maxpoint1/gas_vx_y/profile*.txt and calculate distance between min and max  (delta_y)
# we take maxpoint1/gas_vy_nmg_x/profile*.txt and calculate distance between min and max  (delta_x)
# we read radius of maximum of "gas_p   / gas_p0" from analysis/maxpoint1/max_gas_p_ndg.txt
#  (we've used this value as a reference one, for trace profiles in maxpoint_analysis1.py)


import numpy as np
import argparse
import os
import sys
from plot_1d_txt import *
from auxiliary_functions import parse_idx_range, create_file, create_dir
from auxiliary_functions import parallel_info, print_time_execution
from auxiliary_functions import generate_error_message, read_input_variables_file
import __main__ as main
from mc_meshdata import meshdata, init_meshdata4sim
import cv2

def standalone_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-input_dir",  help="Input directory", default='.')
    parser.add_argument("-output_dir", help="output_dir", default = "./analysis/distances/")
    parser.add_argument("-idx_range",  help="Snapshot index (idx), index range (idx1-idx2) or all files (None)", default=None)
    return parser.parse_args()


def calc(list_phase_type, input_dir, output_dir, idx_range):
    ErrorMessage = generate_error_message(sys._getframe().f_code.co_name, main.__file__)

    idx1, idx2 = parse_idx_range(idx_range, input_dir)

    for phase_type in list_phase_type:
        dict_batch = {}
        create_dir(output_dir + "{phase_type}/".format(phase_type=phase_type) )

        # Copy all min and max files in the output_dir
        os.system("cp {input_dir}/analysis/maxpoint1/max_{phase_type}*.txt {output_dir}{phase_type}/"
                .format(input_dir=input_dir, phase_type=phase_type,output_dir=output_dir))
        os.system("cp {input_dir}/analysis/maxpoint1/min_{phase_type}*.txt {output_dir}{phase_type}/"
                .format(input_dir=input_dir, phase_type=phase_type,output_dir=output_dir))

        # Here we load the file containing the positions of the pressure, density and vorticy extremums 
        pressure_to_load   = input_dir + "/analysis/maxpoint1/max_{phase_type}_p_ndg.txt".format(phase_type=phase_type)
        rho_to_load        = input_dir + "/analysis/maxpoint1/max_{phase_type}_rho_ndg.txt".format(phase_type=phase_type)
        rossby_to_load     = input_dir + "/analysis/maxpoint1/min_{phase_type}_rossby.txt".format(phase_type=phase_type)
     
        dict_batch['rho_max']    = rho_to_load
        dict_batch['rossby min'] = rossby_to_load
        if phase_type=='gas':
            dict_batch['p_max']  = pressure_to_load

        for batch in dict_batch.keys():
            file_to_plot =dict_batch[batch]
            output_file  = output_dir + "{phase_type}/dist_star_{batch}.png".format(batch=batch, phase_type=phase_type)
            plot(input=file_to_plot, output=output_file, cols=[0,2], xlabel='idx', ylabel=r'$d_{star}$(AU)',
                title='Vortex center position with respect to time (computed thanks to {var})'.format(var=batch))



if __name__ == "__main__":
    args = standalone_args()
    prog_name           = os.path.basename(main.__file__)
    start_time          = parallel_info(prog_name, sequential=True)
    input_variables_str = read_input_variables_file(args.input_dir)

    ## Check the number of phases
    list_phase_type=['gas']
    #if 'ONEPHASE' not in input_variables_str:
    #    list_phase_type.append('particles')

    calc(list_phase_type, **vars(args) )

    print_time_execution(start_time, prog_name)
    
	
