#!/usr/bin/env python3

# Permits to study the evolution of the dust-to-gas ratio


import h5py
import numpy as np
from sys import stdout
import sys
import os
import multiprocessing
from functools import partial



def read_input_variables_file(input_dir=None):
    if input_dir==None:
        input_variables_file = open('./Input_variables.data')
    else:
        input_variables_file = open(input_dir + '/Input_variables.data')
    input_variables_str = input_variables_file.read()
    input_variables_file.close()
    return input_variables_str

def find_parameter(file_list, string_to_find):
    for i in file_list.split('\n'):
        if string_to_find in i:
            string_line=i
    parameter=float(string_line.split()[-1])
    return parameter

def write_max_rho(i, path, filename, rho0_gas, period_r0):
    print(i)
    f    = h5py.File("{path}{time:03}.data.h5".format(path=path,time=i),"r")
    rho_part  = f["particles"]["rho"][()]
    time      = f['vars'].attrs["time"][0]
    Z = rho_part/rho0_gas
    Z_max = np.amax(Z)
    time_write=time/2.0/np.pi/period_r0
    os.system("echo '{orbit:.2f}      {txt_to_save:20}' >> {filename}".format(orbit=time_write, txt_to_save=Z_max, filename=filename))
    f.close()

if __name__ == "__main__":

    # Multiprocessing
    ncpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
    pool = multiprocessing.Pool(processes=ncpus)

    path     = os.path.dirname(os.path.realpath(__file__))
    path     = path + '/'
    filename = path+'lambda0.txt'
    os.system("touch {filename}".format(filename=filename))
    os.system("echo '{orbit}      {txt_to_save}' >> {filename}".format(orbit='Time (period)', txt_to_save='Max(rho_dust/rho_gas)', filename=filename))
    fstat      = h5py.File("{path}Stationnary.h5".format(path=path),"r")
    rho0_gas   = fstat["gas"]["rho"][()]
    
    input_variables_str = read_input_variables_file(path)
    period_r0           = find_parameter(input_variables_str, "period_r0 (years)")
    
    partial_write_max_rho = partial(write_max_rho, 
                                    path=path,
                                    filename=filename,
                                    rho0_gas=rho0_gas,
                                    period_r0=period_r0)

    pool.map(partial_write_max_rho, list(range(2,201,1)))

    pool.close()
    fstat.close()
    

